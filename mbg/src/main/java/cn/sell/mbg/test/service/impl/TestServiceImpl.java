package cn.sell.mbg.test.service.impl;

import cn.sell.comm.base.MySqlCrudServiceImpl;
import lombok.extern.log4j.Log4j2;
import cn.sell.mbg.test.po.Testa;
import cn.sell.mbg.test.service.TestService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Log4j2
@Service
@Transactional
public class TestServiceImpl extends MySqlCrudServiceImpl<Testa,String> implements TestService {
}
