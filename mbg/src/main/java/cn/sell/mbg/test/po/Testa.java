package cn.sell.mbg.test.po;

import cn.sell.comm.model.po.BasePO;
import lombok.Data;

import javax.persistence.Id;

@Data
public class Testa extends BasePO<String> {

    @Id
    private String id;

    private String name;

    private String value;

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }
}
