package cn.sell.mbg.test.mapper;

import cn.sell.comm.base.mapper.MyBaseMapper;
import cn.sell.mbg.test.po.Testa;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface testMapper extends MyBaseMapper<Testa> {
}
