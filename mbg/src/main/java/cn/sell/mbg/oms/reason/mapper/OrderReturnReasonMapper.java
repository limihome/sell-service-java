package cn.sell.mbg.oms.reason.mapper;

import cn.sell.comm.base.mapper.MyBaseMapper;
import cn.sell.mbg.oms.reason.OrderReturnReason;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrderReturnReasonMapper extends MyBaseMapper<OrderReturnReason> {
}