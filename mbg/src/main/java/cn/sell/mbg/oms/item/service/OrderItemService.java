package cn.sell.mbg.oms.item.service;

import cn.sell.comm.base.service.MyBaseService;
import cn.sell.mbg.oms.item.OmsOrderItem;

public interface OrderItemService extends MyBaseService<OmsOrderItem,Long> {
}
