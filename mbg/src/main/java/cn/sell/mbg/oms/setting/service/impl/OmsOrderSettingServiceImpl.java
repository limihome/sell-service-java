package cn.sell.mbg.oms.setting.service.impl;

import cn.sell.comm.base.MySqlCrudServiceImpl;
import cn.sell.mbg.oms.setting.OmsOrderSetting;
import cn.sell.mbg.oms.setting.service.OmsOrderSettingService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Log4j2
@Service
@Transactional
public class OmsOrderSettingServiceImpl
        extends MySqlCrudServiceImpl<OmsOrderSetting, Long>
        implements OmsOrderSettingService {
}
