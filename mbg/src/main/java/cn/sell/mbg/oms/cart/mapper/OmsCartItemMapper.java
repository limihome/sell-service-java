package cn.sell.mbg.oms.cart.mapper;

import cn.sell.comm.base.mapper.MyBaseMapper;
import cn.sell.mbg.oms.cart.OmsCartItem;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OmsCartItemMapper extends MyBaseMapper<OmsCartItem> {
}