package cn.sell.mbg.oms.setting.service;

import cn.sell.comm.base.service.MyBaseService;
import cn.sell.mbg.oms.setting.OmsOrderSetting;

public interface OmsOrderSettingService extends MyBaseService<OmsOrderSetting,Long> {
}
