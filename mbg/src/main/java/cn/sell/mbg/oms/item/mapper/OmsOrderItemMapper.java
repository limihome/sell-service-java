package cn.sell.mbg.oms.item.mapper;

import cn.sell.comm.base.mapper.MyBaseMapper;
import cn.sell.mbg.oms.item.OmsOrderItem;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OmsOrderItemMapper extends MyBaseMapper<OmsOrderItem> {
}