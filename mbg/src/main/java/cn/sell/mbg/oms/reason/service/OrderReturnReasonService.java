package cn.sell.mbg.oms.reason.service;

import cn.sell.comm.base.service.MyBaseService;
import cn.sell.mbg.oms.reason.OrderReturnReason;

public interface OrderReturnReasonService extends MyBaseService<OrderReturnReason,Long> {
}
