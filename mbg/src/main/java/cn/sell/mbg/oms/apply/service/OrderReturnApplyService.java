package cn.sell.mbg.oms.apply.service;

import cn.sell.comm.base.service.MyBaseService;
import cn.sell.mbg.oms.apply.OrderReturnApply;

public interface OrderReturnApplyService extends MyBaseService<OrderReturnApply,Long> {
}
