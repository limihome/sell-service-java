package cn.sell.mbg.oms.order.service;

import cn.sell.comm.base.service.MyBaseService;
import cn.sell.mbg.oms.order.OmsOrder;
import cn.sell.mbg.oms.order.view.OmsOrderDetail;

public interface OmsOrderService extends MyBaseService<OmsOrder, Long> {
    OmsOrderDetail getOrderById(Long orderId);

    int close(OmsOrder order);
}
