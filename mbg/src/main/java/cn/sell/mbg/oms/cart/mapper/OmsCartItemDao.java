package cn.sell.mbg.oms.cart.mapper;

import cn.sell.mbg.oms.cart.OmsCartItem;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OmsCartItemDao {
    OmsCartItem selectByPrimaryKey(Long id);
}