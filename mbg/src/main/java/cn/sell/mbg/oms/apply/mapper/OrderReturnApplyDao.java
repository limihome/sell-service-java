package cn.sell.mbg.oms.apply.mapper;

import cn.sell.mbg.oms.apply.OrderReturnApply;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrderReturnApplyDao {
    int deleteByPrimaryKey(Long id);

    int insert(OrderReturnApply record);

    int insertSelective(OrderReturnApply record);

    OrderReturnApply selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(OrderReturnApply record);

    int updateByPrimaryKey(OrderReturnApply record);
}