package cn.sell.mbg.oms.apply.service.impl;

import cn.sell.comm.base.MySqlCrudServiceImpl;
import cn.sell.mbg.oms.apply.OrderReturnApply;
import cn.sell.mbg.oms.apply.service.OrderReturnApplyService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Log4j2
@Service
@Transactional
public class OrderReturnApplyServiceImpl
        extends MySqlCrudServiceImpl<OrderReturnApply, Long>
        implements OrderReturnApplyService {
}
