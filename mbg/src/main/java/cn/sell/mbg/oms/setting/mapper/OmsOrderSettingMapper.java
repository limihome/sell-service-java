package cn.sell.mbg.oms.setting.mapper;

import cn.sell.comm.base.mapper.MyBaseMapper;
import cn.sell.mbg.oms.setting.OmsOrderSetting;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OmsOrderSettingMapper extends MyBaseMapper<OmsOrderSetting> {
}