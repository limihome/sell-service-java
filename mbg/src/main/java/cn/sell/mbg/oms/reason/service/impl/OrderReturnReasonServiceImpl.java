package cn.sell.mbg.oms.reason.service.impl;

import cn.sell.comm.base.MySqlCrudServiceImpl;
import cn.sell.mbg.oms.reason.OrderReturnReason;
import cn.sell.mbg.oms.reason.service.OrderReturnReasonService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Log4j2
@Service
@Transactional
public class OrderReturnReasonServiceImpl
        extends MySqlCrudServiceImpl<OrderReturnReason, Long>
        implements OrderReturnReasonService {
}
