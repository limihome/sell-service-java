package cn.sell.mbg.oms.order.mapper;

import cn.sell.comm.base.mapper.MyBaseMapper;
import cn.sell.mbg.oms.order.OmsOrder;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OmsOrderMapper extends MyBaseMapper<OmsOrder> {
}