package cn.sell.mbg.oms.order.mapper;

import cn.sell.mbg.oms.order.OmsOrder;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OmsOrderDao {
    int deleteByPrimaryKey(Long id);

    int insert(OmsOrder record);

    int insertSelective(OmsOrder record);

    OmsOrder selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(OmsOrder record);

    int updateByPrimaryKey(OmsOrder record);
}