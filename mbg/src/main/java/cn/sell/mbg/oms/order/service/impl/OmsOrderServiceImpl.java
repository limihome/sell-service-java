package cn.sell.mbg.oms.order.service.impl;

import cn.sell.comm.base.MySqlCrudServiceImpl;
import cn.sell.mbg.oms.history.OmsOrderOperateHistory;
import cn.sell.mbg.oms.history.service.OrderOperateHistoryService;
import cn.sell.mbg.oms.item.OmsOrderItem;
import cn.sell.mbg.oms.item.service.OrderItemService;
import cn.sell.mbg.oms.order.OmsOrder;
import cn.sell.mbg.oms.order.service.OmsOrderService;
import cn.sell.mbg.oms.order.view.OmsOrderDetail;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Log4j2
@Service
@Transactional
public class OmsOrderServiceImpl
        extends MySqlCrudServiceImpl<OmsOrder, Long>
        implements OmsOrderService {

    @Autowired
    private OrderItemService orderItemServiceImpl;

    @Autowired
    private OrderOperateHistoryService orderOperateHistoryServiceImpl;

    @Override
    public OmsOrderDetail getOrderById(Long orderId) {
        OmsOrderDetail omsOrderDetail = new OmsOrderDetail();
        OmsOrder omsOrder = super.selectByPk(orderId);
        BeanUtils.copyProperties(omsOrder, omsOrderDetail);


        OmsOrderItem omsOrderItem = new OmsOrderItem();
        omsOrderItem.setOrderId(orderId);
        List<OmsOrderItem> omsOrderItems = orderItemServiceImpl.selectByEntity(omsOrderItem);
        omsOrderDetail.setOrderItemList(omsOrderItems);

        OmsOrderOperateHistory omsOrderOperateHistory = new OmsOrderOperateHistory();
        omsOrderOperateHistory.setOrderId(orderId);
        List<OmsOrderOperateHistory> omsOrderOperateHistories = orderOperateHistoryServiceImpl.selectByEntity(omsOrderOperateHistory);
        omsOrderDetail.setHistoryList(omsOrderOperateHistories);
        return omsOrderDetail;
    }

    @Override
    public int close(OmsOrder order) {
        order.setStatus(4);
        return super.updateByPkSelective(order.getId(), order);
    }
}
