package cn.sell.mbg.oms.history.mapper;

import cn.sell.comm.base.mapper.MyBaseMapper;
import cn.sell.mbg.oms.history.OmsOrderOperateHistory;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OmsOrderOperateHistoryMapper extends MyBaseMapper<OmsOrderOperateHistory> {
}