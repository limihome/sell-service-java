package cn.sell.mbg.oms.reason;

import cn.sell.comm.model.po.BasePO;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "oms_order_return_reason")
public class OrderReturnReason extends BasePO<Long> {
    /**
     * 
     */
    @Id
    @GeneratedValue(generator = "JDBC")
    private Long id;

    /**
     * 退货类型
     */
    @ApiModelProperty(value = "退货类型")
    private String name;

    /**
     * 
     */
    private Integer sort;

    /**
     * 状态：0->不启用；1->启用
     */
    @ApiModelProperty(value = "状态：0->不启用；1->启用")
    private Integer status;

    /**
     * 添加时间
     */
    @ApiModelProperty(value = "添加时间")
    private String createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}