package cn.sell.mbg.oms.history.mapper;

import cn.sell.mbg.oms.history.OmsOrderOperateHistory;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OmsOrderOperateHistoryDao {
    int deleteByPrimaryKey(Long id);

    int insert(OmsOrderOperateHistory record);

    int insertSelective(OmsOrderOperateHistory record);

    OmsOrderOperateHistory selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(OmsOrderOperateHistory record);

    int updateByPrimaryKey(OmsOrderOperateHistory record);
}