package cn.sell.mbg.oms.apply.mapper;

import cn.sell.comm.base.mapper.MyBaseMapper;
import cn.sell.mbg.oms.apply.OrderReturnApply;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrderReturnApplyMapper extends MyBaseMapper<OrderReturnApply> {
}