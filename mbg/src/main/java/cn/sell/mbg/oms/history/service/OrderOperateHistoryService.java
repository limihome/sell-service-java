package cn.sell.mbg.oms.history.service;

import cn.sell.comm.base.service.MyBaseService;
import cn.sell.mbg.oms.history.OmsOrderOperateHistory;

public interface OrderOperateHistoryService extends MyBaseService<OmsOrderOperateHistory,Long> {
}
