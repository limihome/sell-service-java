package cn.sell.mbg.oms.setting.mapper;

import cn.sell.mbg.oms.setting.OmsOrderSetting;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OmsOrderSettingDao {
    int deleteByPrimaryKey(Long id);

    int insert(OmsOrderSetting record);

    int insertSelective(OmsOrderSetting record);

    OmsOrderSetting selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(OmsOrderSetting record);

    int updateByPrimaryKey(OmsOrderSetting record);
}