package cn.sell.mbg.oms.reason.mapper;

import cn.sell.mbg.oms.reason.OrderReturnReason;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrderReturnReasonDao {
    int deleteByPrimaryKey(Long id);

    int insert(OrderReturnReason record);

    int insertSelective(OrderReturnReason record);

    OrderReturnReason selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(OrderReturnReason record);

    int updateByPrimaryKey(OrderReturnReason record);
}