package cn.sell.mbg.oms.order.view;

import cn.sell.mbg.oms.history.OmsOrderOperateHistory;
import cn.sell.mbg.oms.item.OmsOrderItem;
import cn.sell.mbg.oms.order.OmsOrder;
import lombok.Data;

import java.util.List;

/**
 * 订单详情信息
 * Created by macro on 2018/10/11.
 */
@Data
public class OmsOrderDetail extends OmsOrder {

    private List<OmsOrderItem> orderItemList;

    private List<OmsOrderOperateHistory> historyList;
}
