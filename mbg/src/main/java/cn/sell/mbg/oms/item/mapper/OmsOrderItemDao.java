package cn.sell.mbg.oms.item.mapper;

import cn.sell.mbg.oms.item.OmsOrderItem;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OmsOrderItemDao {
    int deleteByPrimaryKey(Long id);

    int insert(OmsOrderItem record);

    int insertSelective(OmsOrderItem record);

    OmsOrderItem selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(OmsOrderItem record);

    int updateByPrimaryKey(OmsOrderItem record);
}