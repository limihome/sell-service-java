package cn.sell.mbg.oms.item.service.impl;

import cn.sell.comm.base.MySqlCrudServiceImpl;
import cn.sell.mbg.oms.item.OmsOrderItem;
import cn.sell.mbg.oms.item.service.OrderItemService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Log4j2
@Service
@Transactional
public class OrderItemServiceImpl
        extends MySqlCrudServiceImpl<OmsOrderItem, Long>
        implements OrderItemService {
}
