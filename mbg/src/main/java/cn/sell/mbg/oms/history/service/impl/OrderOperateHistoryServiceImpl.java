package cn.sell.mbg.oms.history.service.impl;

import cn.sell.comm.base.MySqlCrudServiceImpl;
import cn.sell.mbg.oms.history.OmsOrderOperateHistory;
import cn.sell.mbg.oms.history.service.OrderOperateHistoryService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Log4j2
@Service
@Transactional
public class OrderOperateHistoryServiceImpl
        extends MySqlCrudServiceImpl<OmsOrderOperateHistory, Long>
        implements OrderOperateHistoryService {
}
