package cn.sell.mbg.cms.content.service.impl;

import cn.sell.comm.base.MySqlCrudServiceImpl;
import cn.sell.mbg.cms.article.ArtArticle;
import cn.sell.mbg.cms.article.service.ArtArticleService;
import cn.sell.mbg.cms.content.ArtArticleContent;
import cn.sell.mbg.cms.content.service.ArtArticleContentService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Log4j2
@Service
@Transactional
public class ArtArticleContentServiceImpl
        extends MySqlCrudServiceImpl<ArtArticleContent, Long>
        implements ArtArticleContentService {
}
