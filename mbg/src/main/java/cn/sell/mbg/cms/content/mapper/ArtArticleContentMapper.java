package cn.sell.mbg.cms.content.mapper;

import cn.sell.comm.base.mapper.MyBaseMapper;
import cn.sell.mbg.cms.content.ArtArticleContent;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ArtArticleContentMapper extends MyBaseMapper<ArtArticleContent> {
}