package cn.sell.mbg.cms.article.mapper;

import cn.sell.comm.base.mapper.MyBaseMapper;
import cn.sell.mbg.cms.article.ArtArticle;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ArtArticleMapper extends MyBaseMapper<ArtArticle> {
}