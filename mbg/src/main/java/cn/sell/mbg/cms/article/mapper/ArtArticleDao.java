package cn.sell.mbg.cms.article.mapper;

import cn.sell.mbg.cms.article.ArtArticle;

public interface ArtArticleDao {
    int deleteByPrimaryKey(Long id);

    int insert(ArtArticle record);

    int insertSelective(ArtArticle record);

    ArtArticle selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ArtArticle record);

    int updateByPrimaryKey(ArtArticle record);
}