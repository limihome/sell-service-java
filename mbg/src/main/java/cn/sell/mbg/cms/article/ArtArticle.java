package cn.sell.mbg.cms.article;

import cn.sell.comm.model.po.BasePO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
public class ArtArticle extends BasePO<Long> {
    /**
     * 
     */
    @Id
    @GeneratedValue(generator = "JDBC")
    private Long id;

    /**
     * 文章标题
     */
    @ApiModelProperty(value = "文章标题")
    private String title;

    /**
     * 作者名称
     */
    @ApiModelProperty(value = "作者名称")
    private String authorName;

    /**
     * 文章图片
     */
    @ApiModelProperty(value = "文章图片")
    private String cover;

    /**
     * 文章状态：0-等待审核，1-审核通过，2-拒绝
     */
    @ApiModelProperty(value = "文章状态：0-等待审核，1-审核通过，2-拒绝")
    private String status;

    /**
     * 文章类型
     */
    @ApiModelProperty(value = "文章类型")
    private String type;

    /**
     * 创建日期
     */
    @ApiModelProperty(value = "创建日期")
    private String createTime;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人")
    private String creator;

    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    private String modifyTime;

    /**
     * 修改人
     */
    @ApiModelProperty(value = "修改人")
    private String modifier;

    /**
     * 删除标志
     */
    @ApiModelProperty(value = "删除标志")
    private String isDel;

    /**
     * 版本号
     */
    @ApiModelProperty(value = "版本号")
    private Integer migrationVersion;
}