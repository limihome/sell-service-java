package cn.sell.mbg.cms.article.service;

import cn.sell.comm.base.service.MyBaseService;
import cn.sell.mbg.cms.article.ArtArticle;
import cn.sell.mbg.sys.file.SysFile;

public interface ArtArticleService
        extends MyBaseService<ArtArticle,Long> {
}
