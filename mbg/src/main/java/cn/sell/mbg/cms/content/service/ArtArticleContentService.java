package cn.sell.mbg.cms.content.service;

import cn.sell.comm.base.service.MyBaseService;
import cn.sell.mbg.cms.content.ArtArticleContent;

public interface ArtArticleContentService extends MyBaseService<ArtArticleContent,Long> {
}
