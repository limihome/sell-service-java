package cn.sell.mbg.cms.article.service.impl;

import cn.sell.comm.base.MySqlCrudServiceImpl;
import cn.sell.mbg.cms.article.ArtArticle;
import cn.sell.mbg.cms.article.service.ArtArticleService;
import cn.sell.mbg.sys.file.SysFile;
import cn.sell.mbg.sys.file.service.SysFileService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Log4j2
@Service
@Transactional
public class ArtArticleServiceImpl
        extends MySqlCrudServiceImpl<ArtArticle, Long>
        implements ArtArticleService {
}
