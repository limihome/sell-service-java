package cn.sell.mbg;

import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.api.ProgressCallback;
import org.mybatis.generator.api.VerboseProgressCallback;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.internal.DefaultShellCallback;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 */
public class Generator {
    public static void main(String[] args) {
        run(); //pow generator
    }

    public static void run() {
        try {
            List<String> warnings = new ArrayList<String>();
            try {
                //hkt config
                File configFile = new File(Generator.class.getClassLoader().getResource("mybatis_hkt_tables.xml").getPath());
                ConfigurationParser cp = new ConfigurationParser(warnings);
                Configuration config = cp.parseConfiguration(configFile);
                DefaultShellCallback callback = new DefaultShellCallback(true);//当生成的代码重复时，覆盖原代码
                //创建 MBG
                MyBatisGenerator myBatisGenerator = new MyBatisGenerator(config, callback, warnings);
                ProgressCallback progressCallback = new VerboseProgressCallback();
                //执行生成代码
                myBatisGenerator.generate(progressCallback);
            } catch (Exception e) {
                e.printStackTrace();
            }
            for (String warning : warnings) {
                System.out.println(warning);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
        }
    }
}
