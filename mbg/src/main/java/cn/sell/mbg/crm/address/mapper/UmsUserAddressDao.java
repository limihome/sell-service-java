package cn.sell.mbg.crm.address.mapper;

import cn.sell.mbg.crm.address.UmsUserAddress;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UmsUserAddressDao {

    int deleteByPrimaryKey(Long id);

    int insert(UmsUserAddress record);

    int insertSelective(UmsUserAddress record);

    UmsUserAddress selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UmsUserAddress record);

    int updateByPrimaryKey(UmsUserAddress record);

}