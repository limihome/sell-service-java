package cn.sell.mbg.crm.level.service.impl;

import cn.sell.comm.base.MySqlCrudServiceImpl;
import cn.sell.mbg.crm.level.UmsUserLevel;
import cn.sell.mbg.crm.level.service.UmsUserLevelService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Log4j2
@Service
@Transactional
public class UmsUserLevelServiceImpl
        extends MySqlCrudServiceImpl<UmsUserLevel, Long>
        implements UmsUserLevelService {
}
