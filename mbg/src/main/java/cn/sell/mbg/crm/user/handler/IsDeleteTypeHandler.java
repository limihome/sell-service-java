package cn.sell.mbg.crm.user.handler;

import cn.sell.comm.enums.ENUM_IS_DELETE;
import cn.sell.comm.enums.base.BaseEnumTypeHandler;
import cn.sell.comm.enums.base.DisplayedEnum;
import lombok.extern.log4j.Log4j2;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Log4j2
public class IsDeleteTypeHandler extends BaseEnumTypeHandler<ENUM_IS_DELETE> {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, ENUM_IS_DELETE parameter, JdbcType jdbcType) throws SQLException {
        log.info("parameter:{}",parameter);
        ps.setString(i, parameter.getValue());
    }

    @Override
    public ENUM_IS_DELETE getNullableResult(ResultSet rs, String columnName) throws SQLException {
        log.info("columnName:{}",columnName);
        return DisplayedEnum.valueOfEnum(ENUM_IS_DELETE.class,rs.getString(columnName));
    }

    @Override
    public ENUM_IS_DELETE getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return DisplayedEnum.valueOfEnum(ENUM_IS_DELETE.class,rs.getString(columnIndex));
    }

    @Override
    public ENUM_IS_DELETE getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return DisplayedEnum.valueOfEnum(ENUM_IS_DELETE.class,cs.getString(columnIndex));
    }
}
