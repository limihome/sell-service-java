package cn.sell.mbg.crm.address.service.impl;

import cn.sell.comm.base.MySqlCrudServiceImpl;
import cn.sell.mbg.crm.address.UmsUserAddress;
import cn.sell.mbg.crm.address.service.UmsUserAddressService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Log4j2
@Service
@Transactional
public class UmsUserAddressServiceImpl
        extends MySqlCrudServiceImpl<UmsUserAddress, Long>
        implements UmsUserAddressService {
}
