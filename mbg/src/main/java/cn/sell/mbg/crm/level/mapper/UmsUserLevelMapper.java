package cn.sell.mbg.crm.level.mapper;

import cn.sell.comm.base.mapper.MyBaseMapper;
import cn.sell.mbg.crm.level.UmsUserLevel;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UmsUserLevelMapper  extends MyBaseMapper<UmsUserLevel> {
}