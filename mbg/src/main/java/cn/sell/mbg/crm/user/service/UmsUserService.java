package cn.sell.mbg.crm.user.service;

import cn.sell.comm.base.service.MyBaseService;
import cn.sell.mbg.crm.user.UmsUser;
import cn.sell.mbg.crm.user.view.UserInfo;

public interface UmsUserService extends MyBaseService<UmsUser, Long> {
    UmsUser login(UmsUser umsUser) throws Exception;

    UmsUser register(UmsUser umsUser) throws Exception;

    UmsUser updateByPhone(UmsUser umsUser) throws Exception;

    UserInfo userInfo(Long userId);
}
