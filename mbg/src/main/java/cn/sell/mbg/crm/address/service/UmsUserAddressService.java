package cn.sell.mbg.crm.address.service;

import cn.sell.comm.base.service.MyBaseService;
import cn.sell.mbg.crm.address.UmsUserAddress;

public interface UmsUserAddressService extends MyBaseService<UmsUserAddress,Long> {
}
