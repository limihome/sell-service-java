package cn.sell.mbg.crm.level.mapper;

import cn.sell.mbg.crm.level.UmsUserLevel;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UmsUserLevelDao {
    int insertSelective(UmsUserLevel record);
}