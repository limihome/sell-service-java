package cn.sell.mbg.crm.address.mapper;

import cn.sell.comm.base.mapper.MyBaseMapper;
import cn.sell.mbg.crm.address.UmsUserAddress;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UmsUserAddressMapper extends MyBaseMapper<UmsUserAddress> {
}