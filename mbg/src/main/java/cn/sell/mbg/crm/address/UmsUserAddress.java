package cn.sell.mbg.crm.address;

import cn.sell.comm.model.po.BasePO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@ApiModel("用户地址")
@Table(name = "ums_user_receive_address")
public class UmsUserAddress extends BasePO<Long> {

    @Id
    @GeneratedValue(generator = "JDBC")
    private Long id;

    /**
     * 会员编号
     */
    @ApiModelProperty(value = "会员编号")
    private Long userId;

    /**
     * 收货人名称
     */
    @ApiModelProperty(value = "收货人名称")
    private String name;

    /**
     * 收货人手机号
     */
    @ApiModelProperty(value = "收货人手机号")
    private String phoneNumber;

    /**
     * 是否为默认
     */
    @ApiModelProperty(value = "是否为默认")
    private Integer defaultStatus;

    /**
     * 邮政编码
     */
    @ApiModelProperty(value = "邮政编码")
    private String postCode;

    /**
     * 省份/直辖市
     */
    @ApiModelProperty(value = "省份/直辖市")
    private String province;

    /**
     * 城市
     */
    @ApiModelProperty(value = "城市")
    private String city;

    /**
     * 区
     */
    @ApiModelProperty(value = "区")
    private String region;

    /**
     * 详细地址(街道)
     */
    @ApiModelProperty(value = "详细地址(街道)")
    private String detailAddress;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber == null ? null : phoneNumber.trim();
    }

    public Integer getDefaultStatus() {
        return defaultStatus;
    }

    public void setDefaultStatus(Integer defaultStatus) {
        this.defaultStatus = defaultStatus;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode == null ? null : postCode.trim();
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region == null ? null : region.trim();
    }

    public String getDetailAddress() {
        return detailAddress;
    }

    public void setDetailAddress(String detailAddress) {
        this.detailAddress = detailAddress == null ? null : detailAddress.trim();
    }
}