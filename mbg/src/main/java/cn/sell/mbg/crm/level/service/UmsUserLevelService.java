package cn.sell.mbg.crm.level.service;

import cn.sell.comm.base.service.MyBaseService;
import cn.sell.mbg.crm.level.UmsUserLevel;

public interface UmsUserLevelService extends MyBaseService<UmsUserLevel,Long> {
}
