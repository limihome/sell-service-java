package cn.sell.mbg.crm.user.mapper;

import cn.sell.comm.base.mapper.MyBaseMapper;
import cn.sell.mbg.crm.user.UmsUser;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UmsUserMapper extends MyBaseMapper<UmsUser> {
}
