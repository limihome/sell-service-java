package cn.sell.mbg.crm.user.view;


import cn.sell.mbg.crm.user.UmsUser;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UserInfo {
    @ApiModelProperty(value = "用户基本信息")
    private UmsUser umsUser;
}
