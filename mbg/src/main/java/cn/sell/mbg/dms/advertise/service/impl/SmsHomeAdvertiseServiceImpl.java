package cn.sell.mbg.dms.advertise.service.impl;

import cn.sell.comm.base.MySqlCrudServiceImpl;
import cn.sell.comm.model.qo.PageQO;
import cn.sell.comm.model.vo.PageVO;
import cn.sell.comm.util.DateUtil;
import cn.sell.mbg.dms.advertise.SmsHomeAdvertise;
import cn.sell.mbg.dms.advertise.service.SmsHomeAdvertiseService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Condition;

@Log4j2
@Service
@Transactional
public class SmsHomeAdvertiseServiceImpl extends MySqlCrudServiceImpl<SmsHomeAdvertise, Long> implements SmsHomeAdvertiseService {
    @Override
    public PageVO<SmsHomeAdvertise> homeByList(PageQO pageQO) {
        String dateStr = DateUtil.getDateStr(DateUtil.getNowDate());
        Condition condition = new Condition(SmsHomeAdvertise.class);
        condition.createCriteria()
                .andEqualTo("status", '1')
                .andGreaterThan("endTime", dateStr)
                .andLessThan("startTime", dateStr);
        condition.orderBy("sort");
        pageQO.setCondition(pageQO);
        return super.selectPage(pageQO);
    }
}
