package cn.sell.mbg.dms.reduction.mapper;

import cn.sell.comm.base.mapper.MyBaseMapper;
import cn.sell.mbg.dms.reduction.PmsProductFullReduction;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PmsProductFullReductionMapper extends MyBaseMapper<PmsProductFullReduction> {
}
