package cn.sell.mbg.dms.ladder.mapper;

import cn.sell.comm.base.mapper.MyBaseMapper;
import cn.sell.mbg.dms.ladder.PmsProductLadder;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PmsProductLadderMapper extends MyBaseMapper<PmsProductLadder> {

}