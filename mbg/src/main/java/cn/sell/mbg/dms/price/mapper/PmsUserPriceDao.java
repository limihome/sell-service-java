package cn.sell.mbg.dms.price.mapper;

import cn.sell.mbg.dms.price.PmsUserPrice;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PmsUserPriceDao {
    int deleteByPrimaryKey(Long id);

    int insert(PmsUserPrice record);

    int insertSelective(PmsUserPrice record);

    PmsUserPrice selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(PmsUserPrice record);

    int updateByPrimaryKey(PmsUserPrice record);
}