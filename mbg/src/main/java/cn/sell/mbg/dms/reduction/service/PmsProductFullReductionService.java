package cn.sell.mbg.dms.reduction.service;

import cn.sell.comm.base.service.MyBaseService;
import cn.sell.mbg.dms.reduction.PmsProductFullReduction;

public interface PmsProductFullReductionService extends MyBaseService<PmsProductFullReduction,Long> {

}
