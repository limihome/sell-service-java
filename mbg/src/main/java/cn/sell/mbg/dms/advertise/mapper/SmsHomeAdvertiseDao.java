package cn.sell.mbg.dms.advertise.mapper;

import cn.sell.mbg.dms.advertise.SmsHomeAdvertise;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SmsHomeAdvertiseDao {
    int deleteByPrimaryKey(Long id);

    int insert(SmsHomeAdvertise record);

    int insertSelective(SmsHomeAdvertise record);

    SmsHomeAdvertise selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SmsHomeAdvertise record);

    int updateByPrimaryKey(SmsHomeAdvertise record);
}