package cn.sell.mbg.dms.price;

import cn.sell.comm.model.po.BasePO;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

public class PmsUserPrice extends BasePO<Long> {
    /**
     * 
     */
    @Id
    @GeneratedValue(generator = "JDBC")
    private Long id;

    /**
     * 
     */
    private Long productId;

    /**
     * 
     */
    private Long userLevelId;

    /**
     * 会员价格
     */
    @ApiModelProperty(value = "会员价格")
    private BigDecimal userPrice;

    /**
     * 
     */
    private String userLevelName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getUserLevelId() {
        return userLevelId;
    }

    public void setUserLevelId(Long userLevelId) {
        this.userLevelId = userLevelId;
    }

    public BigDecimal getUserPrice() {
        return userPrice;
    }

    public void setUserPrice(BigDecimal userPrice) {
        this.userPrice = userPrice;
    }

    public String getUserLevelName() {
        return userLevelName;
    }

    public void setUserLevelName(String userLevelName) {
        this.userLevelName = userLevelName == null ? null : userLevelName.trim();
    }
}