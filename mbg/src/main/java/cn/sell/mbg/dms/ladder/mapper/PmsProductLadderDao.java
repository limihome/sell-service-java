package cn.sell.mbg.dms.ladder.mapper;

import cn.sell.mbg.dms.ladder.PmsProductLadder;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PmsProductLadderDao {

    int deleteByPrimaryKey(Long id);

    int insert(PmsProductLadder record);

    int insertSelective(PmsProductLadder record);

    PmsProductLadder selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(PmsProductLadder record);

    int updateByPrimaryKey(PmsProductLadder record);


}