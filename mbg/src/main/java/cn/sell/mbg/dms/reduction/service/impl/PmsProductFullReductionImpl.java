package cn.sell.mbg.dms.reduction.service.impl;

import cn.sell.comm.base.MySqlCrudServiceImpl;
import cn.sell.mbg.dms.reduction.PmsProductFullReduction;
import cn.sell.mbg.dms.reduction.service.PmsProductFullReductionService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Log4j2
@Service
@Transactional
public class PmsProductFullReductionImpl
        extends MySqlCrudServiceImpl<PmsProductFullReduction, Long>
        implements PmsProductFullReductionService {
}
