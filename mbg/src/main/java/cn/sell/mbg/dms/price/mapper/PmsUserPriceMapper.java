package cn.sell.mbg.dms.price.mapper;

import cn.sell.comm.base.mapper.MyBaseMapper;
import cn.sell.mbg.dms.price.PmsUserPrice;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PmsUserPriceMapper extends MyBaseMapper<PmsUserPrice> {

}