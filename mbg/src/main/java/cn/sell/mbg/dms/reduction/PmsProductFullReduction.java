package cn.sell.mbg.dms.reduction;

import cn.sell.comm.model.po.BasePO;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;

public class PmsProductFullReduction extends BasePO<Long> {
    /**
     * id
     */
    @Id
    @GeneratedValue(generator = "JDBC")
    private Long id;
    /**
     * 商品id
     */
    private Long productId;
    /**
     * 商品满足金额
     */
    @ApiModelProperty(value = "商品满足金额")
    private BigDecimal fullPrice;
    /**
     * 商品减少金额
     */
    @ApiModelProperty(value = "商品减少金额")
    private BigDecimal reducePrice;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public BigDecimal getFullPrice() {
        return fullPrice;
    }

    public void setFullPrice(BigDecimal fullPrice) {
        this.fullPrice = fullPrice;
    }

    public BigDecimal getReducePrice() {
        return reducePrice;
    }

    public void setReducePrice(BigDecimal reducePrice) {
        this.reducePrice = reducePrice;
    }
}
