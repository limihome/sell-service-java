package cn.sell.mbg.dms.advertise.service;

import cn.sell.comm.base.service.MyBaseService;
import cn.sell.comm.model.qo.PageQO;
import cn.sell.comm.model.vo.PageVO;
import cn.sell.mbg.dms.advertise.SmsHomeAdvertise;

public interface SmsHomeAdvertiseService extends MyBaseService<SmsHomeAdvertise,Long> {
    PageVO<SmsHomeAdvertise> homeByList(PageQO pageQO);
}
