package cn.sell.mbg.dms.ladder;

import cn.sell.comm.model.po.BasePO;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

public class PmsProductLadder extends BasePO<Long> {
    /**
     * 
     */
    @Id
    @GeneratedValue(generator = "JDBC")
    private Long id;

    /**
     * 商品id
     */
    @ApiModelProperty(value = "商品id")
    private Long productId;

    /**
     * 满足的商品数量
     */
    @ApiModelProperty(value = "满足的商品数量")
    private Integer count;

    /**
     * 折扣
     */
    @ApiModelProperty(value = "折扣")
    private BigDecimal discount;

    /**
     * 折后价格
     */
    @ApiModelProperty(value = "折后价格")
    private BigDecimal price;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}