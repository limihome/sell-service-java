package cn.sell.mbg.dms.advertise;

import cn.sell.comm.model.po.BasePO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@ApiModel("轮播图对象")
public class SmsHomeAdvertise extends BasePO<Long> {
    /**
     * 
     */
    @Id
    @GeneratedValue(generator = "JDBC")
    private Long id;

    /**
     * 轮播名称
     */
    @ApiModelProperty(value = "轮播名称")
    private String name;

    /**
     * 轮播位置：0->PC首页轮播；1->app首页轮播
     */
    @ApiModelProperty(value = "轮播位置：0->PC首页轮播；1->app首页轮播")
    private Integer type;

    /**
     * 轮播图，图片地址
     */
    @ApiModelProperty(value = "轮播图，图片地址")
    private String pic;

    /**
     * 轮播开始时间
     */
    @ApiModelProperty(value = "轮播开始时间")
    private Date startTime;

    /**
     * 轮播结束时间
     */
    @ApiModelProperty(value = "轮播结束时间")
    private Date endTime;

    /**
     * 上下线状态：0->下线；1->上线
     */
    @ApiModelProperty(value = "上下线状态：0->下线；1->上线")
    private Integer status;

    /**
     * 点击数
     */
    @ApiModelProperty(value = "点击数")
    private Integer clickCount;

    /**
     * 下单数
     */
    @ApiModelProperty(value = "下单数")
    private Integer orderCount;

    /**
     * 链接地址
     */
    @ApiModelProperty(value = "链接地址")
    private String url;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String note;

    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    private Integer sort;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic == null ? null : pic.trim();
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getClickCount() {
        return clickCount;
    }

    public void setClickCount(Integer clickCount) {
        this.clickCount = clickCount;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}