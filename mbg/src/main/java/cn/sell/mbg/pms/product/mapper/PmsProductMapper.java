package cn.sell.mbg.pms.product.mapper;

import cn.sell.comm.base.mapper.MyBaseMapper;
import cn.sell.mbg.pms.product.PmsProduct;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PmsProductMapper extends MyBaseMapper<PmsProduct> {
}