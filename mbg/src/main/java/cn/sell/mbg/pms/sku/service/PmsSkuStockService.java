package cn.sell.mbg.pms.sku.service;

import cn.sell.comm.base.service.MyBaseService;
import cn.sell.mbg.pms.sku.PmsSkuStock;

import java.util.List;

public interface PmsSkuStockService extends MyBaseService<PmsSkuStock,Long> {
    int replaceList(Long id, List<PmsSkuStock> lists);
}
