package cn.sell.mbg.pms.attribute.mapper;

import cn.sell.comm.base.mapper.MyBaseMapper;
import cn.sell.mbg.pms.attribute.ProductAttributeParams;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ProductAttributeParamsMapper  extends MyBaseMapper<ProductAttributeParams> {
//    int deleteByPrimaryKey(Long id);
//
//    int insert(ProductAttributeValue record);
//
//    int insertSelective(ProductAttributeValue record);
//
//    ProductAttributeValue selectByPrimaryKey(Long id);
//
//    int updateByPrimaryKeySelective(ProductAttributeValue record);
//
//    int updateByPrimaryKey(ProductAttributeValue record);
}