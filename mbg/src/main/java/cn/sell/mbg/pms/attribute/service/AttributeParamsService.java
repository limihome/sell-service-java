package cn.sell.mbg.pms.attribute.service;

import cn.sell.comm.base.service.MyBaseService;
import cn.sell.mbg.pms.attribute.ProductAttributeParams;

public interface AttributeParamsService extends MyBaseService<ProductAttributeParams,Long> {
}
