package cn.sell.mbg.pms.attribute.service;

import cn.sell.comm.base.service.MyBaseService;
import cn.sell.mbg.pms.attribute.ProductAttributeType;

public interface AttributeTypeService extends MyBaseService<ProductAttributeType,Long> {
}
