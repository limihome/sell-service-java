package cn.sell.mbg.pms.product.service;

import cn.sell.comm.base.service.MyBaseService;
import cn.sell.mbg.pms.product.PmsProduct;
import cn.sell.mbg.pms.product.view.PmsProductParam;

public interface PmsProductService extends MyBaseService<PmsProduct, Long> {

    /**
     * 创建商品信息
     *
     * @param productParam
     * @return
     */
    PmsProduct create(PmsProductParam productParam);

    /**
     * 修改商品，连同商品的其他信息一起修改
     *
     * @param productParam
     * @return
     */
    boolean updateProduct(PmsProductParam productParam);

    /**
     * 获取商品的详细信息
     *
     * @param id
     * @return
     */
    PmsProductParam getProductById(Long id);

    /**
     * app
     * 获取商品信息
     *
     * @return
     */
    public PmsProductParam getProductDetailByIdResultByApp(int id);

    /**
     * 级联删除商品信息
     *
     * @param id
     * @return
     */
    boolean deleteProduct(Long id);
}
