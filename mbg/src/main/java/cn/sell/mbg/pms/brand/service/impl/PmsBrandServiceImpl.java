package cn.sell.mbg.pms.brand.service.impl;

import cn.sell.comm.base.MySqlCrudServiceImpl;
import cn.sell.mbg.pms.brand.PmsBrand;
import cn.sell.mbg.pms.brand.service.PmsBrandService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Log4j2
@Service
@Transactional
public class PmsBrandServiceImpl
        extends MySqlCrudServiceImpl<PmsBrand, Long>
        implements PmsBrandService {
}
