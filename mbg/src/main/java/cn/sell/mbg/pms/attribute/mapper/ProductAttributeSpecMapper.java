package cn.sell.mbg.pms.attribute.mapper;

import cn.sell.comm.base.mapper.MyBaseMapper;
import cn.sell.mbg.pms.attribute.ProductAttributeSpec;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ProductAttributeSpecMapper extends MyBaseMapper<ProductAttributeSpec> {
}