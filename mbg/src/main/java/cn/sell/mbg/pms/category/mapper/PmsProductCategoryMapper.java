package cn.sell.mbg.pms.category.mapper;

import cn.sell.comm.base.mapper.MyBaseMapper;
import cn.sell.mbg.pms.category.PmsProductCategory;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PmsProductCategoryMapper extends MyBaseMapper<PmsProductCategory> {
}