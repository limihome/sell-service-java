package cn.sell.mbg.pms.category.view;

import cn.sell.mbg.pms.category.PmsProductCategory;
import lombok.Data;

import java.util.List;

/**
 * Created by macro on 2018/5/25.
 */
@Data
public class PmsProductCategoryWithChildrenItem extends PmsProductCategory {
    private List<PmsProductCategory> children;
}
