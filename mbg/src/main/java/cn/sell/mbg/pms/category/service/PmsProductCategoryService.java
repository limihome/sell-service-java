package cn.sell.mbg.pms.category.service;

import cn.sell.comm.base.service.MyBaseService;
import cn.sell.mbg.pms.category.PmsProductCategory;

import java.util.List;

public interface PmsProductCategoryService extends MyBaseService<PmsProductCategory,Long> {
    List<PmsProductCategory> toTree();

    Long create(PmsProductCategory productCategory);
}
