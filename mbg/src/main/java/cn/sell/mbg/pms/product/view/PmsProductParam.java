package cn.sell.mbg.pms.product.view;

/**
 * @ClassName:PmsProductParam
 * @Description: TODO:
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Date: 2020/7/27 22:11
 * @Version: 1.0
 **/

import cn.sell.mbg.dms.reduction.PmsProductFullReduction;
import cn.sell.mbg.pms.attribute.ProductAttributeParams;
import cn.sell.mbg.dms.ladder.PmsProductLadder;
import cn.sell.mbg.pms.product.PmsProduct;
import cn.sell.mbg.pms.sku.PmsSkuStock;
import cn.sell.mbg.dms.price.PmsUserPrice;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 创建和修改商品时使用的参数
 * Created by macro on 2018/4/26.
 *
 */
@Data
public class PmsProductParam extends PmsProduct {

    //商品所选分类的父id
    private Long cateParentId;

    @ApiModelProperty("商品阶梯价格设置")
    private List<PmsProductLadder> productLadderList;

    @ApiModelProperty("商品满减价格设置")
    private List<PmsProductFullReduction> productFullReductionList;

    @ApiModelProperty("商品会员价格设置")
    private List<PmsUserPrice> userPriceList;

    @ApiModelProperty("商品的sku库存信息")
    private List<PmsSkuStock> skuStockList;

    @ApiModelProperty("商品参数及自定义规格属性")
    private List<ProductAttributeParams> productAttributeValueList;

//    @ApiModelProperty("专题和商品关系")
//    private List<CmsSubjectProductRelation> subjectProductRelationList;
//    @ApiModelProperty("优选专区和商品的关系")
//    private List<CmsPrefrenceAreaProductRelation> prefrenceAreaProductRelationList;
}
