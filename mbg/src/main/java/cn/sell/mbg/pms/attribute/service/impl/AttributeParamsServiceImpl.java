package cn.sell.mbg.pms.attribute.service.impl;

import cn.sell.comm.base.MySqlCrudServiceImpl;
import cn.sell.mbg.pms.attribute.ProductAttributeParams;
import cn.sell.mbg.pms.attribute.service.AttributeParamsService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Log4j2
@Service
@Transactional
public class AttributeParamsServiceImpl extends MySqlCrudServiceImpl<ProductAttributeParams, Long> implements AttributeParamsService {
}
