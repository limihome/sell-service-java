package cn.sell.mbg.pms.sku.service.impl;

import cn.sell.comm.base.MySqlCrudServiceImpl;
import cn.sell.mbg.pms.sku.PmsSkuStock;
import cn.sell.mbg.pms.sku.mapper.PmsSkuStockDao;
import cn.sell.mbg.pms.sku.service.PmsSkuStockService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Log4j2
@Service
@Transactional
public class PmsSkuStockServiceImpl
        extends MySqlCrudServiceImpl<PmsSkuStock, Long>
        implements PmsSkuStockService {

    @Autowired
    private PmsSkuStockDao skuStockDao;

    @Override
    public int replaceList(Long id, List<PmsSkuStock> lists) {
        return skuStockDao.replaceList(lists);
    }
}
