package cn.sell.mbg.pms.attribute.mapper;

import cn.sell.mbg.pms.attribute.ProductAttributeType;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ProductAttributeTypeDao {
    int deleteByPrimaryKey(Long id);

    int insert(ProductAttributeType record);

    int insertSelective(ProductAttributeType record);

    ProductAttributeType selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ProductAttributeType record);

    int updateByPrimaryKey(ProductAttributeType record);
}