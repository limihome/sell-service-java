package cn.sell.mbg.pms.brand.mapper;

import cn.sell.comm.base.mapper.MyBaseMapper;
import cn.sell.mbg.pms.brand.PmsBrand;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PmsBrandMapper extends MyBaseMapper<PmsBrand> {
}