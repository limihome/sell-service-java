package cn.sell.mbg.pms.sku.mapper;

import cn.sell.mbg.pms.sku.PmsSkuStock;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PmsSkuStockDao {
    /**
     * 批量刷新
     * @param skuStockList
     * @return
     */
    int replaceList(@Param("list") List<PmsSkuStock> skuStockList);
}