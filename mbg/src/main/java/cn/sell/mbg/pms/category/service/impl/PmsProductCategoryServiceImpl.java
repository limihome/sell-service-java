package cn.sell.mbg.pms.category.service.impl;

import cn.sell.comm.base.MySqlCrudServiceImpl;
import cn.sell.mbg.pms.category.PmsProductCategory;
import cn.sell.mbg.pms.category.mapper.PmsProductCategoryDao;
import cn.sell.mbg.pms.category.mapper.PmsProductCategoryMapper;
import cn.sell.mbg.pms.category.service.PmsProductCategoryService;
import com.alibaba.fastjson.JSONArray;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Log4j2
@Service
@Transactional
public class PmsProductCategoryServiceImpl
        extends MySqlCrudServiceImpl<PmsProductCategory, Long>
        implements PmsProductCategoryService {

    @Autowired
    PmsProductCategoryDao pmsProductCategoryDao;
    @Autowired
    PmsProductCategoryMapper pmsProductCategoryMapper;

    @Override
    public List<PmsProductCategory> toTree() {
        List<PmsProductCategory> pmsProductCategories = pmsProductCategoryDao.listWithChildren();
        log.info(JSONArray.toJSON(pmsProductCategories));
        return pmsProductCategoryDao.listWithChildren();
    }

    @Override
    public Long create(PmsProductCategory productCategory) {
        if (0 == productCategory.getParentId()) {
            productCategory.setLevel(0);
        } else {
            productCategory.setLevel(1);
        }
        return super.insert(productCategory);
    }

}
