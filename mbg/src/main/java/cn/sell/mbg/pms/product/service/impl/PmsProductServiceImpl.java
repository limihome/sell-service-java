package cn.sell.mbg.pms.product.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.sell.comm.base.MySqlCrudServiceImpl;
import cn.sell.mbg.pms.attribute.ProductAttributeParams;
import cn.sell.mbg.pms.attribute.mapper.ProductAttributeParamsMapper;
import cn.sell.mbg.dms.ladder.PmsProductLadder;
import cn.sell.mbg.dms.ladder.mapper.PmsProductLadderMapper;
import cn.sell.mbg.pms.category.PmsProductCategory;
import cn.sell.mbg.pms.category.service.PmsProductCategoryService;
import cn.sell.mbg.pms.product.PmsProduct;
import cn.sell.mbg.pms.product.mapper.PmsProductDao;
import cn.sell.mbg.pms.product.mapper.PmsProductMapper;
import cn.sell.mbg.pms.product.service.PmsProductService;
import cn.sell.mbg.pms.product.view.PmsProductParam;
import cn.sell.mbg.dms.reduction.PmsProductFullReduction;
import cn.sell.mbg.dms.reduction.mapper.PmsProductFullReductionMapper;
import cn.sell.mbg.pms.sku.PmsSkuStock;
import cn.sell.mbg.pms.sku.mapper.PmsSkuStockMapper;
import cn.sell.mbg.dms.price.PmsUserPrice;
import cn.sell.mbg.dms.price.mapper.PmsUserPriceMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Log4j2
@Service
@Transactional
public class PmsProductServiceImpl
        extends MySqlCrudServiceImpl<PmsProduct, Long>
        implements PmsProductService {

    @Autowired
    private PmsProductDao pmsProductDao;

    @Autowired
    private PmsProductMapper pmsProductMapper;

    @Autowired
    private PmsSkuStockMapper pmsSkuStockMapper;

    @Autowired
    private PmsUserPriceMapper pmsUserPriceMapper;

    @Autowired
    private PmsProductLadderMapper pmsProductLadderMapper;

    @Autowired
    private PmsProductCategoryService pmsProductCategoryService;

    @Autowired
    private ProductAttributeParamsMapper productAttributeParamsMapper;

    @Autowired
    private PmsProductFullReductionMapper pmsProductFullReductionMapper;

    @Override
    public PmsProduct create(PmsProductParam productParam) {
        PmsProduct pmsProduct = new PmsProduct();
        //对象拷贝
        BeanUtils.copyProperties(productParam, pmsProduct);
        int rowCount = pmsProductMapper.insert(pmsProduct);

        List<PmsProductLadder> productLadderList = productParam.getProductLadderList();
        if (!CollUtil.isEmpty(productLadderList)) {
            pmsProductLadderMapper.insertList(productLadderList);
        }

        List<PmsProductFullReduction> productFullReductionList = productParam.getProductFullReductionList();
        if (!CollUtil.isEmpty(productFullReductionList)) {
            pmsProductFullReductionMapper.insertList(productFullReductionList);
        }

        List<ProductAttributeParams> productAttributeValueList = productParam.getProductAttributeValueList();
        if (!CollUtil.isEmpty(productAttributeValueList)) {
            productAttributeParamsMapper.insertList(productAttributeValueList);
        }

        List<PmsUserPrice> userPriceList = productParam.getUserPriceList();
        if (!CollUtil.isEmpty(userPriceList)) {
            pmsUserPriceMapper.insertList(userPriceList);
        }

        List<PmsSkuStock> skuStockList = productParam.getSkuStockList();
        if (!CollUtil.isEmpty(skuStockList)) {
            pmsSkuStockMapper.insertList(skuStockList);
        }

        if (rowCount > 0) {
            return pmsProduct;
        }
        return null;
    }

    @Override
    public PmsProductParam getProductById(Long id) {
        PmsProductParam pmsProductParam = new PmsProductParam();

        PmsProduct pmsProduct = pmsProductMapper.selectByPrimaryKey(id);
        //对象拷贝
        BeanUtils.copyProperties(pmsProduct, pmsProductParam);

        Long productCategoryId = pmsProduct.getProductCategoryId();
        PmsProductCategory pmsProductCategory = pmsProductCategoryService.selectByPk(productCategoryId);
        pmsProductParam.setCateParentId(pmsProductCategory.getParentId());

        PmsProductLadder productLadder = new PmsProductLadder();
        productLadder.setProductId(id);
        List<PmsProductLadder> ladders = pmsProductLadderMapper.select(productLadder);
        pmsProductParam.setProductLadderList(ladders);

        PmsProductFullReduction pmsProductFullReduction = new PmsProductFullReduction();
        pmsProductFullReduction.setProductId(id);
        List<PmsProductFullReduction> pmsProductFullReductions = pmsProductFullReductionMapper.select(pmsProductFullReduction);
        pmsProductParam.setProductFullReductionList(pmsProductFullReductions);

        ProductAttributeParams productAttributeParams = new ProductAttributeParams();
        productAttributeParams.setProductId(id);
        List<ProductAttributeParams> select = productAttributeParamsMapper.select(productAttributeParams);
        pmsProductParam.setProductAttributeValueList(select);

        PmsUserPrice pmsUserPrice = new PmsUserPrice();
        pmsUserPrice.setProductId(id);
        List<PmsUserPrice> pmsUserPrices = pmsUserPriceMapper.select(pmsUserPrice);
        pmsProductParam.setUserPriceList(pmsUserPrices);

        PmsSkuStock pmsSkuStock = new PmsSkuStock();
        pmsSkuStock.setProductId(id);
        List<PmsSkuStock> pmsSkuStocks = pmsSkuStockMapper.select(pmsSkuStock);
        pmsProductParam.setSkuStockList(pmsSkuStocks);

        return pmsProductParam;
    }

    @Override
    public PmsProductParam getProductDetailByIdResultByApp(int id) {
        return pmsProductDao.getProductDetailByIdResultByApp(id);
    }

    @Override
    public boolean updateProduct(PmsProductParam productParam) {
        PmsProduct pmsProduct = new PmsProduct();
        //对象拷贝
        BeanUtils.copyProperties(productParam, pmsProduct);
        int rowCount = pmsProductMapper.updateByPrimaryKeySelective(pmsProduct);

        List<PmsProductLadder> productLadderList = productParam.getProductLadderList();
        for (PmsProductLadder pmsProductLadder : productLadderList) {
            rowCount = rowCount + pmsProductLadderMapper.updateByPrimaryKeySelective(pmsProductLadder);
        }

        List<PmsProductFullReduction> productFullReductionList = productParam.getProductFullReductionList();
        for (PmsProductFullReduction pmsProductFullReduction : productFullReductionList) {
            rowCount = rowCount + pmsProductFullReductionMapper.updateByPrimaryKeySelective(pmsProductFullReduction);
        }

        List<ProductAttributeParams> productAttributeValueList = productParam.getProductAttributeValueList();
        for (ProductAttributeParams attributeParams : productAttributeValueList) {
            rowCount = rowCount + productAttributeParamsMapper.updateByPrimaryKeySelective(attributeParams);
        }

        List<PmsUserPrice> memberPriceList = productParam.getUserPriceList();
        for (PmsUserPrice pmsUserPrice : memberPriceList) {
            rowCount = rowCount + pmsUserPriceMapper.updateByPrimaryKeySelective(pmsUserPrice);

        }

        List<PmsSkuStock> skuStockList = productParam.getSkuStockList();
        for (PmsSkuStock pmsSkuStock : skuStockList) {
            rowCount = rowCount + pmsSkuStockMapper.updateByPrimaryKeySelective(pmsSkuStock);
        }

        if (rowCount > 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteProduct(Long id) {
        // TODO: 商品如果有正在进行中的订单怎么处理。
        PmsProduct pmsProduct = pmsProductMapper.selectByPrimaryKey(id);
        pmsProductMapper.deleteByPrimaryKey(pmsProduct.getId());

        PmsProductLadder productLadder = new PmsProductLadder();
        productLadder.setProductId(id);
        pmsProductLadderMapper.delete(productLadder);

        PmsProductFullReduction pmsProductFullReduction = new PmsProductFullReduction();
        pmsProductFullReduction.setProductId(id);
        pmsProductFullReductionMapper.delete(pmsProductFullReduction);

        ProductAttributeParams productAttributeParams = new ProductAttributeParams();
        productAttributeParams.setProductId(id);
        productAttributeParamsMapper.delete(productAttributeParams);

        PmsUserPrice pmsUserPrice = new PmsUserPrice();
        pmsUserPrice.setProductId(id);
        pmsUserPriceMapper.delete(pmsUserPrice);

        PmsSkuStock pmsSkuStock = new PmsSkuStock();
        pmsSkuStock.setProductId(id);
        pmsSkuStockMapper.delete(pmsSkuStock);

        return false;
    }
}