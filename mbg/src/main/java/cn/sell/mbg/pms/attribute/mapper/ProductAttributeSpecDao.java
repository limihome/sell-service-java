package cn.sell.mbg.pms.attribute.mapper;

import cn.sell.mbg.pms.attribute.ProductAttributeSpec;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ProductAttributeSpecDao {
    int deleteByPrimaryKey(Long id);

    int insert(ProductAttributeSpec record);

    int insertSelective(ProductAttributeSpec record);

    ProductAttributeSpec selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ProductAttributeSpec record);

    int updateByPrimaryKey(ProductAttributeSpec record);
}