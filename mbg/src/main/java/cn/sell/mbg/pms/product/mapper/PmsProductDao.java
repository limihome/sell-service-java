package cn.sell.mbg.pms.product.mapper;

import cn.sell.mbg.pms.product.view.PmsProductParam;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


@Mapper
public interface PmsProductDao {

    PmsProductParam getProductDetailByIdResultByApp(@Param("id") Integer pageSize);

    int deleteByPrimaryKey(Long id);
}