package cn.sell.mbg.pms.brand.mapper;

import cn.sell.mbg.pms.brand.PmsBrand;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PmsBrandDao {
    int deleteByPrimaryKey(Long id);

    int insert(PmsBrand record);

    int insertSelective(PmsBrand record);

    PmsBrand selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(PmsBrand record);

    int updateByPrimaryKeyWithBLOBs(PmsBrand record);

    int updateByPrimaryKey(PmsBrand record);
}