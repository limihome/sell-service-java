package cn.sell.mbg.pms.attribute.service.impl;

import cn.sell.comm.base.MySqlCrudServiceImpl;
import cn.sell.mbg.pms.attribute.ProductAttributeSpec;
import cn.sell.mbg.pms.attribute.ProductAttributeType;
import cn.sell.mbg.pms.attribute.service.AttributeSpecService;
import cn.sell.mbg.pms.attribute.service.AttributeTypeService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Log4j2
@Service
@Transactional
public class AttributeSpecServiceImpl extends MySqlCrudServiceImpl<ProductAttributeSpec, Long> implements AttributeSpecService {

    @Autowired
    private AttributeTypeService attributeTypeServiceImpl;

    @Override
    public Long create(ProductAttributeSpec attributeSpec) {
        Long insert = super.insert(attributeSpec);
        ProductAttributeType productAttributeType = attributeTypeServiceImpl.selectByPk(attributeSpec.getProductAttributeCategoryId());
        if (null != productAttributeType) {
            if (0 == attributeSpec.getType()) {
                productAttributeType.setAttributeCount(productAttributeType.getAttributeCount() + 1);
            } else {
                productAttributeType.setParamCount(productAttributeType.getParamCount() + 1);
            }
        }
        return insert;
    }
}
