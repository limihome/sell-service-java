package cn.sell.mbg.pms.attribute.mapper;

import cn.sell.comm.base.mapper.MyBaseMapper;
import cn.sell.mbg.pms.attribute.ProductAttributeType;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ProductAttributeTypeMapper extends MyBaseMapper<ProductAttributeType> {
}