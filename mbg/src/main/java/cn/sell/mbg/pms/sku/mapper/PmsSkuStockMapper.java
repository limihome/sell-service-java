package cn.sell.mbg.pms.sku.mapper;

import cn.sell.comm.base.mapper.MyBaseMapper;
import cn.sell.mbg.pms.sku.PmsSkuStock;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PmsSkuStockMapper extends MyBaseMapper<PmsSkuStock> {
}