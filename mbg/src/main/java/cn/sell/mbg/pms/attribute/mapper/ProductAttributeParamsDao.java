package cn.sell.mbg.pms.attribute.mapper;

import cn.sell.mbg.pms.attribute.ProductAttributeParams;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ProductAttributeParamsDao {
    int deleteByPrimaryKey(Long id);

    int insert(ProductAttributeParams record);

    int insertSelective(ProductAttributeParams record);

    ProductAttributeParams selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ProductAttributeParams record);

    int updateByPrimaryKey(ProductAttributeParams record);
}