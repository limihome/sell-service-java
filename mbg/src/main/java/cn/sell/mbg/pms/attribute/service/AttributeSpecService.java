package cn.sell.mbg.pms.attribute.service;

import cn.sell.comm.base.service.MyBaseService;
import cn.sell.mbg.pms.attribute.ProductAttributeSpec;

public interface AttributeSpecService extends MyBaseService<ProductAttributeSpec,Long> {
    Long create(ProductAttributeSpec attributeSpec);
}
