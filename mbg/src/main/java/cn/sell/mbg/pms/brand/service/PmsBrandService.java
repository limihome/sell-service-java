package cn.sell.mbg.pms.brand.service;

import cn.sell.comm.base.service.MyBaseService;
import cn.sell.mbg.pms.brand.PmsBrand;

public interface PmsBrandService extends MyBaseService<PmsBrand,Long> {
}
