package cn.sell.mbg.pms.attribute.service.impl;

import cn.sell.comm.base.MySqlCrudServiceImpl;
import cn.sell.mbg.pms.attribute.ProductAttributeType;
import cn.sell.mbg.pms.attribute.service.AttributeTypeService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Log4j2
@Service
@Transactional
public class AttributeTypeServiceImpl extends MySqlCrudServiceImpl<ProductAttributeType, Long> implements AttributeTypeService {
}
