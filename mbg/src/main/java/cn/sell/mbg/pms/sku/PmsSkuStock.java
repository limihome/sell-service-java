package cn.sell.mbg.pms.sku;

import cn.sell.comm.model.po.BasePO;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

public class PmsSkuStock extends BasePO<Long> {
    /**
     *
     */
    @Id
    @GeneratedValue(generator = "JDBC")
    private Long id;

    /**
     *
     */
    private Long productId;

    /**
     * sku编码
     */
    @ApiModelProperty(value = "sku编码")
    private String skuCode;

    /**
     * 金额
     */
    @ApiModelProperty(value = "金额")
    private BigDecimal price;

    /**
     * 库存
     */
    @ApiModelProperty(value = "库存")
    private Integer stock;

    /**
     * 预警库存
     */
    @ApiModelProperty(value = "预警库存")
    private Integer lowStock;

    /**
     * 展示图片
     */
    @ApiModelProperty(value = "展示图片")
    private String pic;

    /**
     * 销量
     */
    @ApiModelProperty(value = "销量")
    private Integer sale;

    /**
     * 单品促销价格
     */
    @ApiModelProperty(value = "单品促销价格")
    private BigDecimal promotionPrice;

    /**
     * 锁定库存
     */
    @ApiModelProperty(value = "锁定库存")
    private Integer lockStock;

    /**
     * 商品销售属性，json格式
     */
    @ApiModelProperty(value = "商品销售属性，json格式")
    private String spData;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode == null ? null : skuCode.trim();
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Integer getLowStock() {
        return lowStock;
    }

    public void setLowStock(Integer lowStock) {
        this.lowStock = lowStock;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic == null ? null : pic.trim();
    }

    public Integer getSale() {
        return sale;
    }

    public void setSale(Integer sale) {
        this.sale = sale;
    }

    public BigDecimal getPromotionPrice() {
        return promotionPrice;
    }

    public void setPromotionPrice(BigDecimal promotionPrice) {
        this.promotionPrice = promotionPrice;
    }

    public Integer getLockStock() {
        return lockStock;
    }

    public void setLockStock(Integer lockStock) {
        this.lockStock = lockStock;
    }

    public String getSpData() {
        return spData;
    }

    public void setSpData(String spData) {
        this.spData = spData == null ? null : spData.trim();
    }
}