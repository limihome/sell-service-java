package cn.sell.mbg.pms.category.mapper;

import cn.sell.mbg.pms.category.PmsProductCategory;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PmsProductCategoryDao {
    List<PmsProductCategory> listWithChildren();
}
