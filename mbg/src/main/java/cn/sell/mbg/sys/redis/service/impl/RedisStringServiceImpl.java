package cn.sell.mbg.sys.redis.service.impl;

import cn.sell.mbg.sys.redis.service.RedisStringService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName:RedisServiceImpl
 * @Description: StringRedisTemplate与RedisTemplate的区别?
 * 两者的关系是StringRedisTemplate继承RedisTemplate。
 * 两者的数据是不共通的；也就是说StringRedisTemplate只能管理StringRedisTemplate里面的数据
 * RedisTemplate只能管RedisTemplate中的数据。
 * SDR默认采用的序列化策略有两种，一种是String的序列化策略，一种是JDK的序列化策略。
 * StringRedisTemplate默认采用的是String的序列化策略，保存的key和value都是采用此策略序列化保存的。
 * RedisTemplate默认采用的是JDK的序列化策略，保存的key和value都是采用此策略序列化保存的。
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Date: 2020/8/20 16:39
 * @Version: 1.0
 **/
@Service
public class RedisStringServiceImpl implements RedisStringService {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 存入永久数据
     *
     * @param key   键
     * @param value 值
     */
    @Override
    public void set(String key, String value) {
//        this.set(key, value, 0, TimeUnit.SECONDS);
        // 存入永久数据
         stringRedisTemplate.opsForValue().set(key, value);
    }

    /**
     * 新增一个字符串类型的值,key是键，value是值。
     *
     * @param key     键
     * @param value   值
     * @param timeout 超时时间
     * @param unit    计数单位
     */
    @Override
    public void set(String key, String value, long timeout, TimeUnit unit) {
        // 也可以向redis里存入数据和设置缓存时间
        stringRedisTemplate.opsForValue().set(key, value, timeout, unit);
    }

    /**
     * 如果不存在则插入
     *
     * @param key   键
     * @param value 值
     * @return true为插入成功, false失败
     */
    @Override
    public Boolean setIfAbsent(String key, String value) {
        return stringRedisTemplate.opsForValue().setIfAbsent(key, value);
    }

    /**
     * 批量插入，key值存在会覆盖原值
     *
     * @param map
     */
    @Override
    public void multiSet(Map<String, String> map) {
        stringRedisTemplate.opsForValue().multiSet(map);
    }

    /**
     * 批量插入，如果里面的所有key都不存在，则全部插入，返回true，如果其中一个在redis中已存在，全不插入，返回false
     *
     * @param map
     * @return
     */
    @Override
    public Boolean multiSetIfAbsent(Map<String, String> map) {
        return stringRedisTemplate.opsForValue().multiSetIfAbsent(map);
    }


    /**
     * 获取值,key不存在返回null
     *
     * @param key
     * @return
     */
    @Override
    public String get(String key) {
        String s = stringRedisTemplate.opsForValue().get(key);
        return stringRedisTemplate.opsForValue().get(key);
    }

    /**
     * 在原有的值基础上新增字符串到末尾。
     * <p>
     *
     * @param key
     * @param value
     * @return
     */
    @Override
    public Integer append(String key, String value) {
        return stringRedisTemplate.opsForValue().append(key, value);
    }


    /**
     * 获取原来key键对应的值并重新赋新值
     * <p>
     *
     * @param key
     * @param value
     * @return
     */
    @Override
    public String getAndSet(String key, String value) {
        return stringRedisTemplate.opsForValue().getAndSet(key, value);
    }

    /**
     * 获取指定key的值进行减1，如果value不是integer类型，会抛异常，如果key不存在会创建一个，默认value为0
     * <p>
     * decrement(k key)
     *
     * @return
     */
    @Override
    public Long decrement(String key) {
        return stringRedisTemplate.opsForValue().decrement(key);
    }

    /**
     * 获取指定key的值进行加1，如果value不是integer类型，会抛异常，如果key不存在会创建一个，默认value为0
     * <p>
     * increment(k key)
     *
     * @return
     */
    @Override
    public Long increment(String key) {
        return stringRedisTemplate.opsForValue().increment(key);
    }

    /**
     * 删除指定key,成功返回true，否则false
     * <p>
     * delete(k key)
     *
     * @return
     */
    @Override
    public Boolean delete(String key) {
        return stringRedisTemplate.opsForValue().getOperations().delete(key);
    }

    /**
     * 删除多个key，返回删除key的个数
     * <p>
     * delete(k ...keys)
     *
     * @return
     */
    @Override
    public Long deleteMulti(List<String> keys) {
        return stringRedisTemplate.opsForValue().getOperations().delete(keys);
    }
}
