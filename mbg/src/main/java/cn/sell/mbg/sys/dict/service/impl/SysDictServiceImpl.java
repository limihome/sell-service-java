package cn.sell.mbg.sys.dict.service.impl;

import cn.sell.comm.base.MySqlCrudServiceImpl;
import cn.sell.mbg.sys.dict.SysDict;
import cn.sell.mbg.sys.dict.service.SysDictService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Log4j2
@Service
@Transactional
public class SysDictServiceImpl
        extends MySqlCrudServiceImpl<SysDict, Long>
        implements SysDictService {

//    @Override
//    public List<SysDict> getListByCode(String code) {
//        Condition condition = new Condition(SysDict.class);
//        condition.createCriteria().andEqualTo("dict_code", code);
//        return super.selectByCondition(condition);
//    }
}
