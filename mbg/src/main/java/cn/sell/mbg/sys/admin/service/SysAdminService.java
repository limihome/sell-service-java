package cn.sell.mbg.sys.admin.service;

import cn.sell.comm.base.service.MyBaseService;
import cn.sell.mbg.sys.admin.SysAdmin;

import javax.servlet.http.HttpServletRequest;

public interface SysAdminService extends MyBaseService<SysAdmin,Long> {
    SysAdmin getAdminByUsername(String username);

    SysAdmin login(SysAdmin sysAdmin, HttpServletRequest request);
}
