package cn.sell.mbg.sys.oss.service;

import cn.sell.mbg.sys.oss.dto.OssCallbackResult;
import cn.sell.mbg.sys.oss.vo.OssPolicyVO;

import javax.servlet.http.HttpServletRequest;

/**
 * oss上传管理Service
 * Created by macro on 2018/5/17.
 */
public interface OssService {
    /**
     * oss上传策略生成
     */
    OssPolicyVO policy();

    /**
     * oss上传成功回调
     */
    OssCallbackResult callback(HttpServletRequest request);
}
