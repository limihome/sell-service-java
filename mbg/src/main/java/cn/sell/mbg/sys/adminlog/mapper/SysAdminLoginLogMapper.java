package cn.sell.mbg.sys.adminlog.mapper;

import cn.sell.comm.base.mapper.MyBaseMapper;
import cn.sell.mbg.sys.adminlog.SysAdminLoginLog;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysAdminLoginLogMapper extends MyBaseMapper<SysAdminLoginLog> {
}