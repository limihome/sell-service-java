package cn.sell.mbg.sys.sequence;

import lombok.Data;

import java.util.concurrent.atomic.AtomicLong;

/**
 * <p></p>
 *
 * @author coderzl
 * @Title SequenceRange
 * @Description 序列区间，用于缓存序列
 * @date 2017/6/6 22:58
 */
@Data
public class SequenceRange {
    private final long min;
    private final long max;
    private SysSequence sysSequence;

    /**
     *
     */
    private final AtomicLong value;
    /**
     * 是否超限
     */
    private volatile boolean over = false;

    /**
     * 构造.
     *
     * @param min
     * @param max
     */
    public SequenceRange(long min, long max,SysSequence sysSequence) {
        this.min = min;
        this.max = max;
        this.sysSequence = sysSequence;
        this.value = new AtomicLong(min);
    }

    /**
     * <p>Gets and increment</p>
     *
     * @return
     */
    public String getAndIncrement() {
        long currentValue = value.getAndIncrement();
        if (currentValue > max) {
            over = true;
            return "-1";
        }
        long num = sysSequence.getSize();
        if(sysSequence.getPrefix()!=null){
            num =  sysSequence.getSize() - sysSequence.getPrefix().length();
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(sysSequence.getPrefix());
        stringBuffer.append(String.format("%0" + num + "d", currentValue));
        return stringBuffer.toString();
    }

    public static void main(String[] args) {
        String result = "";
        String code = "1122";
        String num = "20";
        result = String.format("%0" + num + "d", Integer.parseInt(code) + 1);
        System.out.println(result);
        System.out.println(result.length());
    }

}
