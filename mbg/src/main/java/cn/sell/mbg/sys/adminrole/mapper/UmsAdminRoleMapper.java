package cn.sell.mbg.sys.adminrole.mapper;

import cn.sell.comm.base.mapper.MyBaseMapper;
import cn.sell.mbg.sys.adminrole.UmsAdminRoleRelation;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UmsAdminRoleMapper extends MyBaseMapper<UmsAdminRoleRelation> {
}
