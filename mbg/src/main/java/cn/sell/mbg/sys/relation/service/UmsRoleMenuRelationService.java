package cn.sell.mbg.sys.relation.service;

import cn.sell.comm.base.service.MyBaseService;
import cn.sell.mbg.sys.menu.UmsMenu;
import cn.sell.mbg.sys.relation.UmsRoleMenuRelation;

import java.util.List;

public interface UmsRoleMenuRelationService extends MyBaseService<UmsRoleMenuRelation, Long> {

    List<UmsMenu> getCheckedMenu(Long roleId);

    Long saveOrUpdateChecked(Long roleId, String menuIds);

    int deleteByRoleId(Long roleId);
}
