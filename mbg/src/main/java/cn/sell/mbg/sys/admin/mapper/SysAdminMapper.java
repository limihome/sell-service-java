package cn.sell.mbg.sys.admin.mapper;

import cn.sell.comm.base.mapper.MyBaseMapper;
import cn.sell.mbg.sys.admin.SysAdmin;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysAdminMapper extends MyBaseMapper<SysAdmin> {
}