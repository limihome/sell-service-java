package cn.sell.mbg.sys.file;

import cn.sell.comm.model.po.BasePO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
public class SysFile extends BasePO<Long> {
    /**
     * 
     */
    @Id
    @GeneratedValue(generator = "JDBC")
    private Long id;

    /**
     * 名称
     */
    @ApiModelProperty(value = "名称")
    private String name;

    /**
     * 路径
     */
    @ApiModelProperty(value = "路径")
    private String path;

    /**
     * 大小
     */
    @ApiModelProperty(value = "大小")
    private String size;

    /**
     * 前端文件标记
     */
    @ApiModelProperty(value = "前端文件标记")
    private String mark;

    /**
     * 关联id
     */
    @ApiModelProperty(value = "关联id")
    private Long relevanceId;

    /**
     * 创建日期
     */
    @ApiModelProperty(value = "创建日期")
    private String createTime;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人")
    private String creator;

    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    private String modifyTime;

    /**
     * 修改人
     */
    @ApiModelProperty(value = "修改人")
    private String modifier;

    /**
     * 删除标志
     */
    @ApiModelProperty(value = "删除标志")
    private String isDel;

    /**
     * 版本号
     */
    @ApiModelProperty(value = "版本号")
    private Integer migrationVersion;

}