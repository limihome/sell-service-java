package cn.sell.mbg.sys.admin.mapper;

import cn.sell.mbg.sys.admin.SysAdmin;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysAdminDao {
    int deleteByPrimaryKey(Long id);

    int insert(SysAdmin record);

    int insertSelective(SysAdmin record);

    SysAdmin selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysAdmin record);

    int updateByPrimaryKey(SysAdmin record);
}