package cn.sell.mbg.sys.relation.mapper;

import cn.sell.mbg.sys.relation.UmsRoleMenuRelation;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UmsRoleMenuRelationDao {
    int deleteByPrimaryKey(Long id);

    int insert(UmsRoleMenuRelation record);

    int insertSelective(UmsRoleMenuRelation record);

    UmsRoleMenuRelation selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UmsRoleMenuRelation record);

    int updateByPrimaryKey(UmsRoleMenuRelation record);
}