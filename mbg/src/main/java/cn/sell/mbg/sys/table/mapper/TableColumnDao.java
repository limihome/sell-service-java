package cn.sell.mbg.sys.table.mapper;

import cn.sell.mbg.sys.table.TableColumn;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface TableColumnDao {

	
	/**
	* 查询指定数据库和表的表结构
	* @author liusonglin
	* @date 2018年7月25日
	* @param dataSourceName
	* @param tableName
	* @return
	*/
		
	List<TableColumn> getTableColumn(@Param("dataSourceName") String dataSourceName,
                                     @Param("tableName") String tableName);

	
	/**
	* 查询数据所有表的表结构
	* @author liusonglin
	* @date 2018年7月25日
	* @param dataSourceName
	* @return
	*/
		
	List<TableColumn> getAllTableColumn(@Param("dataSourceName") String dataSourceName);
}