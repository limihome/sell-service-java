package cn.sell.mbg.sys.menu.view;

import cn.sell.mbg.sys.menu.UmsMenu;
import lombok.Data;

import java.util.List;

@Data
public class UmsMenuWithChildrenItem extends UmsMenu {
    private List<UmsMenu> children;
}
