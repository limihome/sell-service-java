package cn.sell.mbg.sys.adminrole.mapper;

import cn.sell.mbg.sys.adminrole.UmsAdminRoleRelation;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UmsAdminRoleRelationDao {
    int deleteByPrimaryKey(Long id);

    int insert(UmsAdminRoleRelation record);

    int insertSelective(UmsAdminRoleRelation record);

    UmsAdminRoleRelation selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UmsAdminRoleRelation record);

    int updateByPrimaryKey(UmsAdminRoleRelation record);
}