package cn.sell.mbg.sys.sequence.mapper;

import cn.sell.comm.base.mapper.MyBaseMapper;
import cn.sell.mbg.sys.sequence.SysSequence;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SequenceMapper extends MyBaseMapper<SysSequence> {
}
