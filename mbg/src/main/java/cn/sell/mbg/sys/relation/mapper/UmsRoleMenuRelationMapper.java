package cn.sell.mbg.sys.relation.mapper;


import cn.sell.comm.base.mapper.MyBaseMapper;
import cn.sell.mbg.sys.relation.UmsRoleMenuRelation;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UmsRoleMenuRelationMapper extends MyBaseMapper<UmsRoleMenuRelation> {
}