package cn.sell.mbg.sys.sms.service;


public interface IDySmsService {

    /**
     * 短信发送API
     *
     * @param phone
     * @param smsType
     * @return
     */
    public String sendSms(String phone, String smsType) throws Exception;

    /**
     * 验证短信验证码接口
     *
     * @param phone
     * @param smsType
     * @param verifyCode
     * @return
     */
    public boolean verifySendSms(String phone, String smsType, String verifyCode);

    /**
     * 短信查询API
     *
     * @param phone
     * @param smsBizId
     * @param sendDate
     * @param pages
     * @param rows
     * @return
     */
    public Object sendDetails(String phone, String smsBizId, String sendDate, Integer pages, Integer rows);

    /**
     * 短信批量发送API
     *
     * @param phone
     * @param signName
     * @param smsType
     * @param smsUpExtendCode
     * @return
     */
    public boolean sendBatchSms(String phone, String signName, String smsType, String smsUpExtendCode);

}
