package cn.sell.mbg.sys.file.mapper;

import cn.sell.comm.base.mapper.MyBaseMapper;
import cn.sell.mbg.sys.file.SysFile;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysFileMapper extends MyBaseMapper<SysFile> {
}