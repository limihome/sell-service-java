package cn.sell.mbg.sys.role.service.impl;

import cn.sell.comm.base.MySqlCrudServiceImpl;
import cn.sell.mbg.sys.role.UmsRole;
import cn.sell.mbg.sys.role.service.UmsRoleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UmsRoleServiceImpl extends MySqlCrudServiceImpl<UmsRole ,Long>implements UmsRoleService {
}
