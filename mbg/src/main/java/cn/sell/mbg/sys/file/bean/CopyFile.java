package cn.sell.mbg.sys.file.bean;

import lombok.Data;

@Data
public class CopyFile {

    /**
     * 旧文件id
     */
    private Long oldFileId;

    /**
     * 新文件的名称
     * afas.pdf
     */
    private String newName;
    /**
     * 新文件的路径
     * /adfas/adsfsaf 最后不要加斜杠
     */
    private String newPath;
}
