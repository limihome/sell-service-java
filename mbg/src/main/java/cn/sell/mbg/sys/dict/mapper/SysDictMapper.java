package cn.sell.mbg.sys.dict.mapper;

import cn.sell.comm.base.mapper.MyBaseMapper;
import cn.sell.mbg.sys.dict.SysDict;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysDictMapper extends MyBaseMapper<SysDict> {
}