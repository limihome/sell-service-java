package cn.sell.mbg.sys.menu.service;

import cn.sell.comm.base.service.MyBaseService;
import cn.sell.mbg.sys.menu.UmsMenu;
import cn.sell.mbg.sys.menu.view.UmsMenuWithChildrenItem;

import java.util.List;


public interface UmsMenuService extends MyBaseService<UmsMenu, Long> {
    List<UmsMenuWithChildrenItem> getMenu();
}
