package cn.sell.mbg.sys.role.mapper;
import cn.sell.comm.base.mapper.MyBaseMapper;
import cn.sell.mbg.sys.role.UmsRole;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UmsRoleMapper extends MyBaseMapper<UmsRole> {

}
