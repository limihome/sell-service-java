package cn.sell.mbg.sys.adminlog.service;

import cn.sell.comm.base.service.MyBaseService;
import cn.sell.mbg.sys.adminlog.SysAdminLoginLog;

public interface SysAdminLoginLogService extends MyBaseService<SysAdminLoginLog,Long> {
}
