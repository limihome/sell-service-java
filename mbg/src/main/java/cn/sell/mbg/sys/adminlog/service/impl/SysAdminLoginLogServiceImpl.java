package cn.sell.mbg.sys.adminlog.service.impl;

import cn.sell.comm.base.MySqlCrudServiceImpl;
import cn.sell.mbg.sys.adminlog.SysAdminLoginLog;
import cn.sell.mbg.sys.adminlog.service.SysAdminLoginLogService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Log4j2
@Service
@Transactional
public class SysAdminLoginLogServiceImpl extends MySqlCrudServiceImpl<SysAdminLoginLog, Long> implements SysAdminLoginLogService {
}
