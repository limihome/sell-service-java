package cn.sell.mbg.sys.menu.service.impl;

import cn.sell.comm.base.MySqlCrudServiceImpl;
import cn.sell.mbg.sys.menu.UmsMenu;
import cn.sell.mbg.sys.menu.mapper.UmsMenuDao;
import cn.sell.mbg.sys.menu.service.UmsMenuService;
import cn.sell.mbg.sys.menu.view.UmsMenuWithChildrenItem;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Log4j2
@Service
@Transactional
public class UmsMenuServiceImpl extends MySqlCrudServiceImpl<UmsMenu, Long> implements UmsMenuService {

    @Autowired
    private UmsMenuDao umsMenuDaoImpl;

    public List<UmsMenuWithChildrenItem> getMenu() {
        return umsMenuDaoImpl.listWithChildren();
    }
}
