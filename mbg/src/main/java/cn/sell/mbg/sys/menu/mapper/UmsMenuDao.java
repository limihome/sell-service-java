package cn.sell.mbg.sys.menu.mapper;

import cn.sell.mbg.sys.menu.view.UmsMenuWithChildrenItem;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UmsMenuDao {
    List<UmsMenuWithChildrenItem> listWithChildren();
}