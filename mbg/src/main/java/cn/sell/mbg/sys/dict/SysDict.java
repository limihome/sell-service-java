package cn.sell.mbg.sys.dict;

import cn.sell.comm.model.po.BasePO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
public class SysDict extends BasePO<Long>  {
    /**
     * id
     */
    @Id
    @GeneratedValue(generator = "JDBC")
    private Long id;

    /**
     * 字典类型
     */
    @ApiModelProperty(value = "字典类型")
    private String dictCode;

    /**
     * 字典名称
     */
    @ApiModelProperty(value = "字典名称")
    private String dictName;

    /**
     * 字典值
     */
    @ApiModelProperty(value = "字典值")
    private String dictValue;

    /**
     * 字典描述
     */
    @ApiModelProperty(value = "字典描述")
    private String dictDesc;

    /**
     * 是否删除
     */
    @ApiModelProperty(value = "是否删除")
    private String isDelete;

}
