package cn.sell.mbg.sys.redis.service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName:RedisService
 * @Description: TODO:
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Date: 2020/8/20 15:52
 * @Version: 1.0
 **/
public interface RedisStringService {

    /**
     * 存入永久数据
     *
     * @param key   键
     * @param value 值
     */
    public void set(String key, String value);

    /**
     * 新增一个字符串类型的值,key是键，value是值。
     *
     * @param key     键
     * @param value   值
     * @param timeout 超时时间
     * @param unit    计数单位
     */
    public void set(String key, String value, long timeout, TimeUnit unit);

    /**
     * 如果不存在则插入
     *
     * @param key   键
     * @param value 值
     * @return true为插入成功, false失败
     */
    public Boolean setIfAbsent(String key, String value);

    /**
     * 批量插入，key值存在会覆盖原值
     *
     * @param map
     */
    public void multiSet(Map<String, String> map);

    /**
     * 批量插入，如果里面的所有key都不存在，则全部插入，返回true，如果其中一个在redis中已存在，全不插入，返回false
     *
     * @param map
     * @return
     */
    public Boolean multiSetIfAbsent(Map<String, String> map);

    /**
     * 获取值,key不存在返回null
     *
     * @param key
     * @return
     */
    public String get(String key);

    /**
     * 在原有的值基础上新增字符串到末尾。
     * <p>
     *
     * @param key
     * @param value
     * @return
     */
    public Integer append(String key, String value);

    /**
     * 获取原来key键对应的值并重新赋新值
     * <p>
     *
     * @param key
     * @param value
     * @return
     */
    public String getAndSet(String key, String value);

    /**
     * 获取指定key的值进行减1，如果value不是integer类型，会抛异常，如果key不存在会创建一个，默认value为0
     * <p>
     * decrement(k key)
     *
     * @return
     */
    public Long decrement(String key);

    /**
     * 获取指定key的值进行加1，如果value不是integer类型，会抛异常，如果key不存在会创建一个，默认value为0
     * <p>
     * increment(k key)
     *
     * @return
     */
    public Long increment(String key);

    /**
     * 删除指定key,成功返回true，否则false
     * <p>
     * delete(k key)
     *
     * @return
     */
    public Boolean delete(String key);

    /**
     * 删除多个key，返回删除key的个数
     * <p>
     * delete(k ...keys)
     *
     * @return
     */
    public Long deleteMulti(List<String> keys);
}
