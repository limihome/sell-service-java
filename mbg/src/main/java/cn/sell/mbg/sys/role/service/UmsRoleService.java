package cn.sell.mbg.sys.role.service;

import cn.sell.comm.base.service.MyBaseService;
import cn.sell.mbg.sys.role.UmsRole;

public interface UmsRoleService extends MyBaseService<UmsRole,Long> {
}
