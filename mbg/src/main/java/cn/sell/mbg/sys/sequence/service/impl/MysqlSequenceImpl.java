package cn.sell.mbg.sys.sequence.service.impl;

import cn.sell.mbg.sys.sequence.factory.MysqlSequenceFactory;
import cn.sell.mbg.sys.sequence.service.MysqlSequence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

@Service
@Transactional
public class MysqlSequenceImpl implements MysqlSequence {

    @Autowired
    private MysqlSequenceFactory mysqlSequenceFactory;

    /**
     * <p>
     * 获取指定sequence的序列号
     * </p>
     *
     * @param seqName sequence名
     * @return String 序列号
     * @author coderzl
     */
    @Override
    public String nextVal(String seqName) {
        return Objects.toString(mysqlSequenceFactory.getNextVal(seqName));
    }
}