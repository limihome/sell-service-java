package cn.sell.mbg.sys.sms.constants;

/**
 * 阿里云短信常量类
 */
public class SmsConstants {

    /**
     * 手机号正则检验
     */
    public static final String PHONE_REGEX_MOBILE = "(134[0-8]\\d{7})" + "|(" + "((13([0-3]|[5-9]))" + "|149" + "|15([0-3]|[5-9])" + "|166" + "|17(3|[5-8])" + "|18[0-9]" + "|19[8-9]" + ")" + "\\d{8}" + ")";

    /**
     * 短信redis存储～前缀
     */
    public static final String MB_SMS_CODE_PHONE = "REDIS_SMS_";

    /**
     * 批量短信redis存储～前缀
     */
    public static final String MB_SMS_BATCH_CODE_PHONE = "REDIS_SMS_BATCH_";

    /**
     * 短信redis存储～过期时间
     */
    public static final int MB_SMS_CODE_TIME = 600;


    /**
     * 阿里短信-身份验证验证码
     */
    public static final String ALI_SMS_ID_CARD_CODE = "SMS_150*******";

    /**
     * 阿里短信-登录确认验证码
     */
    public static final String ALI_SMS_LOGIN_YES_CODE = "SMS_199773173";

    /**
     * 阿里短信-登录异常验证码
     */
    public static final String ALI_SMS_LOGIN_ERROR_CODE = "SMS_150*******";

    /**
     * 阿里短信-用户注册验证码
     */
    public static final String ALI_SMS_USER_REGISTER_CODE = "SMS_150*******";

    /**
     * 阿里短信-修改密码验证码
     */
    public static final String ALI_SMS_UPDATE_PASSWORD_CODE = "SMS_150*******";

    /**
     * 阿里短信-信息变更验证码
     */
    public static final String ALI_SMS_INFO_UPDATE_CODE = "SMS_150*******";


    /**
     * 内部编码-阿里短信-身份验证验证码
     */
    public static final String MB_SMS_ID_CARD_CODE = "1200**";

    /**
     * 内部编码-阿里短信-登录确认验证码
     */
    public static final String MB_SMS_LOGIN_YES_CODE = "20100199773173";

    /**
     * 内部编码-阿里短信-登录异常验证码
     */
    public static final String MB_SMS_LOGIN_ERROR_CODE = "1200**";

    /**
     * 内部编码-阿里短信-用户注册验证码
     */
    public static final String MB_SMS_USER_REGISTER_CODE = "1200**";

    /**
     * 内部编码-阿里短信-修改密码验证码
     */
    public static final String MB_SMS_UPDATE_PASSWORD_CODE = "1200**";

    /**
     * 内部编码-阿里短信-信息变更验证码
     */
    public static final String MB_SMS_INFO_UPDATE_CODE = "1200**";

    /**
     * 阿里短信类型-内部短信类型编码
     */
    public static final String[] SMS_TYPE_ARRAY = {
            MB_SMS_ID_CARD_CODE,
            MB_SMS_LOGIN_YES_CODE,
            MB_SMS_LOGIN_ERROR_CODE,
            MB_SMS_USER_REGISTER_CODE,
            MB_SMS_UPDATE_PASSWORD_CODE,
            MB_SMS_INFO_UPDATE_CODE
    };


}
