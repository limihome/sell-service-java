package cn.sell.mbg.sys.file.service;

import cn.sell.comm.base.service.MyBaseService;
import cn.sell.mbg.sys.file.SysFile;
import cn.sell.mbg.sys.file.bean.CopyFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

public interface SysFileService
        extends MyBaseService<SysFile, Long> {
    List<SysFile> uploadFile(HttpServletRequest request, String filePath) throws Exception;

    List<SysFile> copy(List<CopyFile> files) throws IOException;

    List<SysFile> move(List<CopyFile> files) throws IOException;

    List<SysFile> delete(List<Long> ids) throws IOException;
}
