package cn.sell.mbg.sys.oss.config;

import com.aliyun.oss.OSSClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName:OssConfig
 * @Description: TODO:
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Date: 2020/8/1 14:38
 * @Version: 1.0
 **/
@Configuration
public class OssConfig {
    @Value("${oss.endpoint}")
    private String ALIYUN_OSS_ENDPOINT;
    @Value("${oss.accessKeyId}")
    private String ALIYUN_OSS_ACCESSKEYID;
    @Value("${oss.accessKeySecret}")
    private String ALIYUN_OSS_ACCESSKEYSECRET;

    @Bean
    public OSSClient ossClient() {
        return new OSSClient(ALIYUN_OSS_ENDPOINT, ALIYUN_OSS_ACCESSKEYID, ALIYUN_OSS_ACCESSKEYSECRET);
    }
}
