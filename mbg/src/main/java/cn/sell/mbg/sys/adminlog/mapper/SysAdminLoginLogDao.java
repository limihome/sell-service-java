package cn.sell.mbg.sys.adminlog.mapper;

import cn.sell.mbg.sys.adminlog.SysAdminLoginLog;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysAdminLoginLogDao {
    int deleteByPrimaryKey(Long id);

    int insert(SysAdminLoginLog record);

    int insertSelective(SysAdminLoginLog record);

    SysAdminLoginLog selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysAdminLoginLog record);

    int updateByPrimaryKey(SysAdminLoginLog record);
}