package cn.sell.mbg.sys.sequence;

import jodd.util.StringUtil;
import lombok.Data;

import javax.persistence.Id;

@Data
public class SysSequence {
    /**
     * id
     */
    @Id
    private Long id;
    /**
     * seq名
     */
    private String name;
    /**
     * 当前值
     */
    private Long current;
    /**
     * 最小值
     */
    private Long min;
    /**
     * 最大值
     */
    private Long max;
    /**
     * 前缀
     */
    private String prefix;
    /**
     * 生成流水号的长度
     */
    private Long size;

    /**
     * 每次取值的数量(默认为100)
     */
    private Long step;
    /**
     *
     */
    private String createTime;
    /**
     *
     */
    private String updateTime;

    public boolean validate() {
        //一些简单的校验。如当前值必须在最大最小值之间。step值不能大于max与min的差
        if (StringUtil.isBlank(name)
                || min < 0
                || max <= 0
                || step <= 0
                || min >= max
                || max - min <= step
                || current < min
                || current > max) {
            return false;
        }
        return true;
    }
}
