package cn.sell.mbg.sys.menu.mapper;

import cn.sell.comm.base.mapper.MyBaseMapper;
import cn.sell.mbg.sys.menu.UmsMenu;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UmsMenuMapper extends MyBaseMapper<UmsMenu> {
}
