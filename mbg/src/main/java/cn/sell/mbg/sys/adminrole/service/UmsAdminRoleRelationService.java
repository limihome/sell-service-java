package cn.sell.mbg.sys.adminrole.service;

import cn.sell.comm.base.service.MyBaseService;
import cn.sell.mbg.sys.adminrole.UmsAdminRoleRelation;
import cn.sell.mbg.sys.role.UmsRole;

import java.util.List;

public interface UmsAdminRoleRelationService extends MyBaseService<UmsAdminRoleRelation, Long> {

    List<UmsRole> getCheckedRole(Long adminId);

    Long saveOrUpdateChecked(Long adminId, String roleIds);

    int deleteByAdminId(Long adminId);
}
