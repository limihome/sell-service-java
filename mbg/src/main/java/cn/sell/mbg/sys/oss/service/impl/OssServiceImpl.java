package cn.sell.mbg.sys.oss.service.impl;

import cn.hutool.json.JSONUtil;
import cn.sell.mbg.sys.oss.dto.OssCallbackDTO;
import cn.sell.mbg.sys.oss.dto.OssCallbackResult;
import cn.sell.mbg.sys.oss.service.OssService;
import cn.sell.mbg.sys.oss.vo.OssPolicyVO;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.common.utils.BinaryUtil;
import com.aliyun.oss.model.MatchMode;
import com.aliyun.oss.model.PolicyConditions;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * oss上传管理Service实现类
 * Created by macro on 2018/5/17.
 */
@Log4j2
@Service
public class OssServiceImpl implements OssService {

    @Value("${oss.policy.expire}")
    private int ALIYUN_OSS_EXPIRE;

    @Value("${oss.policy.maxSize}")
    private int ALIYUN_OSS_MAX_SIZE;

    @Value("${oss.callback}")
    private String ALIYUN_OSS_CALLBACK;

    @Value("${oss.bucketName}")
    private String ALIYUN_OSS_BUCKET_NAME;

    @Value("${oss.endpoint}")
    private String ALIYUN_OSS_ENDPOINT;

    @Value("${oss.dir.prefix}")
    private String ALIYUN_OSS_DIR_PREFIX;

    @Autowired
    private OSSClient ossClient;

    public void testOss() {

    }

    /**
     * 签名生成
     */
    @Override
    public OssPolicyVO policy() {
        OssPolicyVO resultVO = new OssPolicyVO();
        // 存储目录
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String dir = ALIYUN_OSS_DIR_PREFIX + sdf.format(new Date());
        // 签名有效期
        long expireEndTime = System.currentTimeMillis() + ALIYUN_OSS_EXPIRE * 1000;
        Date expiration = new Date(expireEndTime);
        // 文件大小
        long maxSize = ALIYUN_OSS_MAX_SIZE * 1024 * 1024;
        // 回调
        OssCallbackDTO callback = new OssCallbackDTO();
        callback.setCallbackUrl(ALIYUN_OSS_CALLBACK);
        callback.setCallbackBody("filename=${object}&size=${size}&mimeType=${mimeType}&height=${imageInfo.height}&width=${imageInfo.width}");
        callback.setCallbackBodyType("application/x-www-form-urlencoded");
        // 提交节点
        String action = "http://" + ALIYUN_OSS_BUCKET_NAME + "." + ALIYUN_OSS_ENDPOINT;
        try {
            PolicyConditions policyConds = new PolicyConditions();
            policyConds.addConditionItem(PolicyConditions.COND_CONTENT_LENGTH_RANGE, 0, maxSize);
            policyConds.addConditionItem(MatchMode.StartWith, PolicyConditions.COND_KEY, dir);
            String postPolicy = ossClient.generatePostPolicy(expiration, policyConds);
            byte[] binaryData = postPolicy.getBytes("utf-8");
            String policy = BinaryUtil.toBase64String(binaryData);
            String signature = ossClient.calculatePostSignature(postPolicy);
            String callbackData = BinaryUtil.toBase64String(JSONUtil.parse(callback).toString().getBytes("utf-8"));
            // 返回结果
            resultVO.setAccessKeyId(ossClient.getCredentialsProvider().getCredentials().getAccessKeyId());
            resultVO.setPolicy(policy);
            resultVO.setSignature(signature);
            resultVO.setDir(dir);
            resultVO.setCallback(callbackData);
            resultVO.setHost(action);
        } catch (Exception e) {
            log.error("签名生成失败", e);
        }
        return resultVO;
    }

    @Override
    public OssCallbackResult callback(HttpServletRequest request) {
        OssCallbackResult result = new OssCallbackResult();
        String filename = request.getParameter("filename");
        filename = "http://".concat(ALIYUN_OSS_BUCKET_NAME).concat(".").concat(ALIYUN_OSS_ENDPOINT).concat("/").concat(filename);
        result.setFilename(filename);
        result.setSize(request.getParameter("size"));
        result.setMimeType(request.getParameter("mimeType"));
        result.setWidth(request.getParameter("width"));
        result.setHeight(request.getParameter("height"));
        return result;
    }

}
