package cn.sell.admin.cms;

import cn.sell.mbg.cms.article.ArtArticle;
import cn.sell.mbg.cms.article.service.ArtArticleService;
import cn.sell.mbg.sys.file.SysFile;
import cn.sell.mbg.sys.file.service.SysFileService;
import io.swagger.annotations.*;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * the system for content management
 *
 * @ClassName:ContentController
 * @Description: TODO:
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Date: 2020/4/1 14:05
 * @Version: 1.0
 **/

@Log4j2
@RestController
@RequestMapping("/rest/content")
@Api(tags = "ContentController", description = "文件上传")
@ApiResponses({
        @ApiResponse(code = 200, message = "请求正常"),
        @ApiResponse(code = 4001, message = "系统异常")
})
public class ContentController {

    @Value("${file.base.path}")
    String filePath;

    @Autowired
    private SysFileService sysFileServiceImpl;

    @Autowired
    private ArtArticleService artArticleService;

    @ApiOperation("商品图片批量上传")
    @RequestMapping(value = "/markdown", method = RequestMethod.POST)
    public List<SysFile> markdownUploadFile(HttpServletRequest request,
                                            @RequestParam(value = "articleId") Long articleId) throws Exception {
        List<SysFile> sysFiles = sysFileServiceImpl.uploadFile(request, filePath);
        for (SysFile file : sysFiles) {
            file.setRelevanceId(articleId);
            sysFileServiceImpl.saveOrUpdate(file);
        }
        return sysFiles;
    }

    @ApiOperation("创建新文章")
    @RequestMapping(value = "/article", method = RequestMethod.GET)
    public ArtArticle createArticle() throws Exception {
        ArtArticle artArticle = new ArtArticle();
        artArticleService.insert(artArticle);
        return artArticle;
    }

}
