package cn.sell.admin.home;

import cn.sell.comm.model.qo.PageQO;
import cn.sell.comm.model.vo.PageVO;
import cn.sell.comm.result.annotation.ResponseResult;
import cn.sell.mbg.dms.advertise.SmsHomeAdvertise;
import cn.sell.mbg.dms.advertise.service.SmsHomeAdvertiseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
  *@Author: Limi
  *@Contact : qq(2393839633),Email(13924223985@163.com)
  *@Description: TODO: 地址信息控制台
  *@Date: 2020/3/21 20:40
  *@Version: 1.0
  **/

@Log4j2
@RestController
@ResponseResult // 正常情况下统一返回格式需要增加这个注解，错误情况架构会自动捕捉异常
@RequestMapping("/rest/home")
@Api(tags = "HomeController - 首页信息查询")
@ApiResponses({
        @ApiResponse(code = 200, message = "请求正常"),
        @ApiResponse(code = 4001, message = "系统异常")
})
public class HomeController {

    @Autowired
    private SmsHomeAdvertiseService smsHomeAdvertiseServiceImpl;

    /**
     * @api {GET} /rest/home/data 查询
     * @apiVersion 1.0.0
     * @apiGroup 首页信息查询
     * @apiName list
     * @apiDescription 首页信息查询
     * @apiSuccess (响应结果) {Object} response
     * @apiSuccessExample 响应结果示例
     * {}
     */
    @ApiOperation("查询字典表数据，支持条件、分页查询，不支持模糊查询")
    @GetMapping("/data")
    public Map<String, Object> list() {
        Map<String, Object> dataMap = new HashMap<>();
        PageQO pageQO = new PageQO(1, 3);

        PageVO<SmsHomeAdvertise> smsHomeAdvertisePageVO = smsHomeAdvertiseServiceImpl.homeByList(pageQO);
        dataMap.put("advertiseList",smsHomeAdvertisePageVO.getList());


        return dataMap;
    }
}
