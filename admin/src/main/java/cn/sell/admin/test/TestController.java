package cn.sell.admin.test;

import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;
import cn.sell.comm.model.qo.PageQO;
import cn.sell.comm.model.vo.PageVO;
import cn.sell.mbg.sys.sequence.service.MysqlSequence;
import cn.sell.mbg.test.po.Testa;
import cn.sell.mbg.test.service.TestService;
import cn.sell.comm.result.annotation.ResponseResult;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
  *@Author: Limi
  *@Contact : qq(2393839633),Email(13924223985@163.com)
  *@Description: TODO: 例子
  *@Date: 2020/3/23 9:40
  *@Version: 1.0
  **/

@Log4j2
@Transactional
@RestController
@ResponseResult // 正常情况下统一返回格式需要增加这个注解，错误情况架构会自动捕捉异常
@RequestMapping("/rest/test")
public class TestController {


    @Autowired
    private MysqlSequence mysqlSequence;

    @GetMapping("/excel")
    public void asdf() throws Exception {
        for(int i =0;i<10;i++){
            log.info(mysqlSequence.nextVal("test"));
        }
    }

    @GetMapping("/plug")
    public PageVO<Testa> pagePlug() throws Exception {
        return null;
    }

    @GetMapping("/test")
    public PageInfo<Object> page() throws Exception {
        return null;
    }

    @GetMapping("/insert")
    public void insert() throws Exception {
    }
}
