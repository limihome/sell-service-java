package cn.sell.admin.dms.reduction;

import cn.sell.comm.model.qo.PageQO;
import cn.sell.comm.model.vo.PageVO;
import cn.sell.comm.result.annotation.ResponseResult;
import cn.sell.mbg.dms.reduction.PmsProductFullReduction;
import cn.sell.mbg.dms.reduction.service.PmsProductFullReductionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;


@Log4j2
@RestController
@ResponseResult // 正常情况下统一返回格式需要增加这个注解，错误情况架构会自动捕捉异常
@RequestMapping("/rest/reduction")
@Api(tags = "ProductFullReductionController - 商品减免")
@ApiResponses({
        @ApiResponse(code = 200, message = "请求正常"),
        @ApiResponse(code = 4001, message = "系统异常")
})
public class ReductionController {

    @Autowired
    private PmsProductFullReductionService pmsProductFullReductionService;

    /**
     *
     * @param productFullReduction
     * @return
     * @throws Exception
     */
    @ApiOperation("商品减免管理：新增")
    @PostMapping("/add")
    public Long add(@RequestBody PmsProductFullReduction productFullReduction) throws Exception {
        Assert.notNull(productFullReduction.getProductId(),"product_id is not null [商品id不能为空]");
        Assert.notNull(productFullReduction.getFullPrice(),"full_price is not null [商品满足金额不能为空]");
        Assert.notNull(productFullReduction.getReducePrice(),"reduce_price is not null [商品减免金额不能为空]");
      return  pmsProductFullReductionService.insert(productFullReduction);
    }

    /**
     *
     * @param id
     * @return
     * @throws Exception
     */
    @ApiOperation("商品减免管理：删除")
    @PostMapping("/delete/{id}")
    public int delete(@PathVariable("id") Long id) throws Exception {
        Assert.notNull(id,"id is not null [ 主键不能为空 ]");
        return pmsProductFullReductionService.deleteByPk(id);
    }

    @ApiOperation("商品减免管理：修改")
    @PostMapping("/update")
    public int update(@RequestBody PmsProductFullReduction productFullReduction){
        Assert.notNull(productFullReduction.getId(),"id is not null [ 主键不能为空 ]");
        return  pmsProductFullReductionService.updateByPkSelective(productFullReduction.getId(),productFullReduction);
    }

    @ApiOperation("查询商品减免表数据，根据商品id查询")
    @PostMapping("/list/{productId}")
    public PageVO<PmsProductFullReduction> list(@PathVariable("productId") Long productId,
                                                @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                                @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        PmsProductFullReduction pmsProductFullReduction=new PmsProductFullReduction();
        pmsProductFullReduction.setProductId(productId);
        PageQO pageQO = new PageQO(pageNum, pageSize);
        pageQO.setCondition(pmsProductFullReduction);
        return pmsProductFullReductionService.selectPage(pageQO);
    }
}
