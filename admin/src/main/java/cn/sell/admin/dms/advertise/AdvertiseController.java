package cn.sell.admin.dms.advertise;

import cn.sell.comm.model.qo.PageQO;
import cn.sell.comm.model.vo.PageVO;
import cn.sell.comm.result.annotation.ResponseResult;
import cn.sell.mbg.dms.advertise.SmsHomeAdvertise;
import cn.sell.mbg.dms.advertise.service.SmsHomeAdvertiseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

/**
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Description: 轮播图管理控制台
 * @Date: 2020/3/21 20:40
 * @Version: 1.0
 **/

@Log4j2
@RestController
@ResponseResult // 正常情况下统一返回格式需要增加这个注解，错误情况架构会自动捕捉异常
@RequestMapping("/rest/advertise")
@Api(tags = "AdvertiseController - 轮播图管理")
public class AdvertiseController {

    @Autowired
    private SmsHomeAdvertiseService smsHomeAdvertiseServiceImpl;

    @ApiOperation("轮播图管理：支持条件、分页、排序查询，不支持模糊查询")
    @PostMapping("/list")
    public PageVO<SmsHomeAdvertise> list(@RequestBody SmsHomeAdvertise homeAdvertise,
                                         @RequestParam(value = "pageSize", defaultValue = "3") Integer pageSize,
                                         @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        PageQO pageQO = new PageQO(pageNum, pageSize);
        pageQO.setCondition(homeAdvertise);
        return smsHomeAdvertiseServiceImpl.selectPage(pageQO);
    }

    @ApiOperation("轮播图管理：新增")
    @PostMapping("/add")
    public Long add(@RequestBody SmsHomeAdvertise homeAdvertise) throws Exception {
        Assert.notNull(homeAdvertise.getName(), "name is not null [ 收货人名称不能为空 ]");
        return smsHomeAdvertiseServiceImpl.insert(homeAdvertise);
    }

    /**
     * @api {POST} /rest/advertise/update update
     * @apiVersion 1.0.0
     * @apiGroup 轮播图管理
     * @apiName update
     * @apiDescription 地址信息
     * @apiParam (请求体) {Number} id
     * @apiParam (请求体) {String} name 轮播名称
     * @apiParam (请求体) {Number} type 轮播位置：0->PC首页轮播；1->app首页轮播
     * @apiParam (请求体) {String} pic 轮播图，图片地址
     * @apiParam (请求体) {Number} startTime 轮播开始时间
     * @apiParam (请求体) {Number} endTime 轮播结束时间
     * @apiParam (请求体) {Number} status 上下线状态：0->下线；1->上线
     * @apiParam (请求体) {Number} clickCount 点击数
     * @apiParam (请求体) {Number} orderCount 下单数
     * @apiParam (请求体) {String} url 链接地址
     * @apiParam (请求体) {String} note 备注
     * @apiParam (请求体) {Number} sort 排序
     * @apiParam (请求体) {String} version 版本号
     * @apiParam (请求体) {String} createUser 创建人
     * @apiParam (请求体) {String} createTime 创建时间
     * @apiParam (请求体) {String} updateUser 修改人
     * @apiParam (请求体) {String} updateTime 修改时间
     * @apiParamExample 请求体示例
     * {"note":"WD","clickCount":1061,"orderCount":6961,"updateUser":"ksBsOv","updateTime":"zx","pic":"1tlTBTT","sort":7865,"type":6662,"version":"G","url":"0AQSi9O","createTime":"5ZsxPZ","name":"NsQ7gEQ","startTime":3201660947483,"createUser":"MxhkHxC","id":5958,"endTime":1865649399668,"status":4006}
     * @apiSuccess (响应结果) {Number} response
     * @apiSuccessExample 响应结果示例
     * 4139
     */
    @ApiOperation("轮播图管理：修改")
    @PostMapping("/update")
    public int update(@RequestBody SmsHomeAdvertise homeAdvertise) {
        Assert.notNull(homeAdvertise.getId(), "id is not null [ 主键不能为空 ]");
        return smsHomeAdvertiseServiceImpl.updateByPk(homeAdvertise.getId(), homeAdvertise);
    }

    /**
     * @api {GET} /rest/advertise/delete/{id} delete
     * @apiVersion 1.0.0
     * @apiGroup 轮播图管理
     * @apiName delete
     * @apiDescription 地址信息
     * @apiParam (请求参数) {Number} id
     * @apiParamExample 请求参数示例
     * id=2281
     * @apiSuccess (响应结果) {Number} response
     * @apiSuccessExample 响应结果示例
     * 40011
     */
    @ApiOperation("轮播图管理：删除")
    @GetMapping("/delete/{id}")
    public int delete(@PathVariable("id") Long id) {
        Assert.notNull(id, "id is not null [ 主键不能为空 ]");
        return smsHomeAdvertiseServiceImpl.deleteByPk(id);
    }

}
