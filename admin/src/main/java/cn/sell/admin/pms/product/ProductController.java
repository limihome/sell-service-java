package cn.sell.admin.pms.product;

import cn.sell.comm.model.qo.PageQO;
import cn.sell.comm.model.vo.PageVO;
import cn.sell.comm.result.annotation.ResponseResult;
import cn.sell.mbg.pms.product.PmsProduct;
import cn.sell.mbg.pms.product.service.PmsProductService;
import cn.sell.mbg.pms.product.view.PmsProductParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

/**
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Description: TODO: 商品管理
 * @Date: 2020/3/23 9:39
 * @Version: 1.0
 **/

@Log4j2
@RestController
@ResponseResult // 正常情况下统一返回格式需要增加这个注解，错误情况架构会自动捕捉异常
@RequestMapping("/rest/product")
@Api(tags = "ProductController - 商品管理")
@ApiResponses({
        @ApiResponse(code = 200, message = "请求正常"),
        @ApiResponse(code = 4001, message = "系统异常")
})
public class ProductController {
    @Autowired
    private PmsProductService productService;

    @ApiOperation("查询商品表数据，支持条件、分页查询，不支持模糊查询")
    @PostMapping("/list")
    public PageVO<PmsProduct> list(@RequestBody PmsProduct product,
                                   @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                   @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) throws Exception {
        product.getDeleteStatus();
        PageQO pageQO = new PageQO(pageNum, pageSize);
        pageQO.setCondition(product);
        pageQO.setOrderBy("create_time DESC, sort ASC");
        return productService.selectPage(pageQO);
    }

    @ApiOperation("商品表数据:获取商品")
    @GetMapping("/get/{id}")
    public PmsProductParam getProductById(@PathVariable("id") Long id) throws Exception {
        return productService.getProductById(id);
    }

    @ApiOperation("商品表数据:添加商品")
    @PostMapping("/create")
    public PmsProduct create(@RequestBody PmsProductParam pmsProductParam) throws Exception {
        return productService.create(pmsProductParam);
    }


    @ApiOperation("商品表数据：只修改商品表数据")
    @PostMapping("/update")
    public int update(@RequestBody PmsProduct product) {
        Assert.notNull(product.getId(), "id is not null [ 主键不能为空 ]");
        return productService.updateByPkSelective(product.getId(),product);
    }


    @ApiOperation("商品表数据：修改所有关联数据")
    @PostMapping("/update/{id}")
    public boolean update(@PathVariable("id") Long id,
                          @RequestBody PmsProductParam pmsProductParam) {
        Assert.notNull(id, "id is not null [ 主键不能为空 ]");
        return productService.updateProduct(pmsProductParam);
    }

    @ApiOperation("商品表数据：根据商品id删除商品")
    @PostMapping("/delete/{id}")
    public boolean delete(@PathVariable("id") Long id) {
        Assert.notNull(id, "id is not null [ 主键不能为空 ]");
        return productService.deleteProduct(id);
    }

    @ApiOperation("商品表数据：逻辑删除")
    @PostMapping("/deleteStatus")
    public int deleteStatus(Long id,Integer deleteStatus)  {
        Assert.notNull(id, "id is not null [ 主键不能为空 ]");
        PmsProduct pmsProduct = new PmsProduct();
        pmsProduct.setId(id);
        pmsProduct.setDeleteStatus(deleteStatus);
        return productService.updateByPkSelective(id,pmsProduct);
    }

}
