package cn.sell.admin.pms.brand;

import cn.sell.comm.model.qo.PageQO;
import cn.sell.comm.model.vo.PageVO;
import cn.sell.comm.result.annotation.ResponseResult;
import cn.sell.mbg.pms.brand.PmsBrand;
import cn.sell.mbg.pms.brand.service.PmsBrandService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

/**
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Description: TODO: 字典管理
 * @Date: 2020/3/23 9:39
 * @Version: 1.0
 **/

@Log4j2
@RestController
@ResponseResult // 正常情况下统一返回格式需要增加这个注解，错误情况架构会自动捕捉异常
@RequestMapping("/rest/brand")
@Api(tags = "BrandController - 品牌管理")
@ApiResponses({
        @ApiResponse(code = 200, message = "请求正常"),
        @ApiResponse(code = 4001, message = "系统异常")
})
public class BrandController {

    @Autowired
    private PmsBrandService brandServiceImpl;

    @ApiOperation("品牌管理：查询")
    @PostMapping("/list")
    public PageVO<PmsBrand> list(@RequestBody PmsBrand brand,
                                 @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                 @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) throws Exception {
        PageQO pageQO = new PageQO(pageNum, pageSize);
        pageQO.setCondition(brand);
        return brandServiceImpl.selectPage(pageQO);
    }

    @ApiOperation("品牌管理：新增")
    @PostMapping("/add")
    public Long add(@RequestBody PmsBrand brand) throws Exception {
        Assert.notNull(brand.getName(), "name is not null [ 品牌名称不能为空 ]");
        return brandServiceImpl.insert(brand);
    }

    @ApiOperation("品牌管理：修改")
    @PostMapping("/update")
    public int update(@RequestBody PmsBrand brand) throws Exception {
        Assert.notNull(brand.getId(), "id is not null [ 主键不能为空 ]");
        Assert.notNull(brand.getName(), "name is not null [ 品牌名称不能为空 ]");
        return brandServiceImpl.updateByPkSelective(brand.getId(), brand);
    }

    @ApiOperation("品牌管理：删除（物理删除）")
    @GetMapping("/delete/{id}")
    public int delete(@PathVariable("id") Long id) throws Exception {
        return brandServiceImpl.deleteByPk(id);
    }
}
