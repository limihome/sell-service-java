package cn.sell.admin.pms.sku;

import cn.sell.comm.model.qo.PageQO;
import cn.sell.comm.model.vo.PageVO;
import cn.sell.comm.result.annotation.ResponseResult;
import cn.sell.mbg.pms.sku.PmsSkuStock;
import cn.sell.mbg.pms.sku.service.PmsSkuStockService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Description: TODO: 字典管理
 * @Date: 2020/3/23 9:39
 * @Version: 1.0
 **/

@Log4j2
@RestController
@ResponseResult // 正常情况下统一返回格式需要增加这个注解，错误情况架构会自动捕捉异常
@RequestMapping("/rest/sku")
@Api(tags = "SkuStockController - 商品规格参数")
@ApiResponses({
        @ApiResponse(code = 200, message = "请求正常"),
        @ApiResponse(code = 4001, message = "系统异常")
})
public class SkuStockController {

    @Autowired
    private PmsSkuStockService skuStockServiceImpl;


    /**
     * @api {POST} /rest/sku/list list
     * @apiVersion 1.0.0
     * @apiGroup Sku管理
     * @apiName list
     * @apiParam (请求参数) {Number} pageSize
     * @apiParam (请求参数) {Number} pageNum
     * @apiParamExample 请求参数示例
     * pageSize=4267&pageNum=4715
     * @apiParam (请求体) {Number} id
     * @apiParam (请求体) {Number} productId
     * @apiParam (请求体) {String} skuCode sku编码
     * @apiParam (请求体) {Number} price
     * @apiParam (请求体) {Number} stock 库存
     * @apiParam (请求体) {Number} lowStock 预警库存
     * @apiParam (请求体) {String} pic 展示图片
     * @apiParam (请求体) {Number} sale 销量
     * @apiParam (请求体) {Number} promotionPrice 单品促销价格
     * @apiParam (请求体) {Number} lockStock 锁定库存
     * @apiParam (请求体) {String} spData 商品销售属性，json格式
     * @apiParam (请求体) {String} version 版本号
     * @apiParam (请求体) {String} createUser 创建人
     * @apiParam (请求体) {String} createTime 创建时间
     * @apiParam (请求体) {String} updateUser 修改人
     * @apiParam (请求体) {String} updateTime 修改时间
     * @apiParamExample 请求体示例
     * {"spData":"Q5OifwbN","lockStock":1495,"productId":4402,"promotionPrice":184.66904554693576,"lowStock":712,"updateUser":"OdOH0Ll","updateTime":"YH7P","pic":"Kwz06bdxTE","version":"0YEsBB","sale":420,"createTime":"n3G","price":2876.8151161875553,"createUser":"rNV2WC","id":271,"stock":3311,"skuCode":"9d"}
     * @apiSuccess (响应结果) {Number} pageNum
     * @apiSuccess (响应结果) {Number} pageSize
     * @apiSuccess (响应结果) {Number} total
     * @apiSuccess (响应结果) {Number} pages
     * @apiSuccess (响应结果) {Array} list
     * @apiSuccess (响应结果) {Number} list.id
     * @apiSuccess (响应结果) {Number} list.productId
     * @apiSuccess (响应结果) {String} list.skuCode sku编码
     * @apiSuccess (响应结果) {Number} list.price
     * @apiSuccess (响应结果) {Number} list.stock 库存
     * @apiSuccess (响应结果) {Number} list.lowStock 预警库存
     * @apiSuccess (响应结果) {String} list.pic 展示图片
     * @apiSuccess (响应结果) {Number} list.sale 销量
     * @apiSuccess (响应结果) {Number} list.promotionPrice 单品促销价格
     * @apiSuccess (响应结果) {Number} list.lockStock 锁定库存
     * @apiSuccess (响应结果) {String} list.spData 商品销售属性，json格式
     * @apiSuccess (响应结果) {String} list.version 版本号
     * @apiSuccess (响应结果) {String} list.createUser 创建人
     * @apiSuccess (响应结果) {String} list.createTime 创建时间
     * @apiSuccess (响应结果) {String} list.updateUser 修改人
     * @apiSuccess (响应结果) {String} list.updateTime 修改时间
     * @apiSuccessExample 响应结果示例
     * {"total":8392,"pages":4060,"pageSize":7576,"list":[{"spData":"fE6","lockStock":732,"productId":283,"promotionPrice":3395.129594055911,"lowStock":1423,"updateUser":"hVceW","updateTime":"tXyBZNsb","pic":"CWNB","version":"F2G","sale":360,"createTime":"aLVCY","price":5070.752567943089,"createUser":"u6rmvu","id":3525,"stock":3661,"skuCode":"x"}],"pageNum":4216}
     */
    @ApiOperation("商品规格参数，支持条件、分页查询，不支持模糊查询")
    @PostMapping("/list")
    public PageVO<PmsSkuStock> list(@RequestBody PmsSkuStock skuStock,
                                    @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                    @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) throws Exception {
        PageQO pageQO = new PageQO(pageNum, pageSize);
        pageQO.setCondition(skuStock);
        return skuStockServiceImpl.selectPage(pageQO);
    }


    /**
     * @api {POST} /rest/sku/add add
     * @apiVersion 1.0.0
     * @apiGroup Sku管理
     * @apiName add
     * @apiParam (请求体) {Number} id
     * @apiParam (请求体) {Number} productId
     * @apiParam (请求体) {String} skuCode sku编码
     * @apiParam (请求体) {Number} price
     * @apiParam (请求体) {Number} stock 库存
     * @apiParam (请求体) {Number} lowStock 预警库存
     * @apiParam (请求体) {String} pic 展示图片
     * @apiParam (请求体) {Number} sale 销量
     * @apiParam (请求体) {Number} promotionPrice 单品促销价格
     * @apiParam (请求体) {Number} lockStock 锁定库存
     * @apiParam (请求体) {String} spData 商品销售属性，json格式
     * @apiParam (请求体) {String} version 版本号
     * @apiParam (请求体) {String} createUser 创建人
     * @apiParam (请求体) {String} createTime 创建时间
     * @apiParam (请求体) {String} updateUser 修改人
     * @apiParam (请求体) {String} updateTime 修改时间
     * @apiParamExample 请求体示例
     * {"spData":"c","lockStock":3877,"productId":4844,"promotionPrice":6583.064807498218,"lowStock":5484,"updateUser":"TngNgM","updateTime":"7e2LdZS","pic":"dK","version":"JqJ","sale":5829,"createTime":"N","price":5847.159205428188,"createUser":"VFnwpIyy","id":8503,"stock":6848,"skuCode":"EKpRPAQDlN"}
     * @apiSuccess (响应结果) {Number} response
     * @apiSuccessExample 响应结果示例
     * 7236
     */
    @ApiOperation("商品规格参数：新增")
    @PostMapping("/add")
    public Long add(@RequestBody PmsSkuStock skuStock) throws Exception {
//        Assert.notNull(skuStock.get(), "code is not null [ 字典编码不能为空 ]");
        return skuStockServiceImpl.insert(skuStock);
    }


    /**
     * @api {POST} /rest/sku/update update
     * @apiVersion 1.0.0
     * @apiGroup Sku管理
     * @apiName update
     * @apiParam (请求体) {Number} id
     * @apiParam (请求体) {Number} productId
     * @apiParam (请求体) {String} skuCode sku编码
     * @apiParam (请求体) {Number} price
     * @apiParam (请求体) {Number} stock 库存
     * @apiParam (请求体) {Number} lowStock 预警库存
     * @apiParam (请求体) {String} pic 展示图片
     * @apiParam (请求体) {Number} sale 销量
     * @apiParam (请求体) {Number} promotionPrice 单品促销价格
     * @apiParam (请求体) {Number} lockStock 锁定库存
     * @apiParam (请求体) {String} spData 商品销售属性，json格式
     * @apiParam (请求体) {String} version 版本号
     * @apiParam (请求体) {String} createUser 创建人
     * @apiParam (请求体) {String} createTime 创建时间
     * @apiParam (请求体) {String} updateUser 修改人
     * @apiParam (请求体) {String} updateTime 修改时间
     * @apiParamExample 请求体示例
     * {"spData":"D3Wsy","lockStock":1797,"productId":6754,"promotionPrice":1848.3681415585017,"lowStock":9890,"updateUser":"IY","updateTime":"CqX","pic":"1O0tvoP","version":"6","sale":6356,"createTime":"Lg769Vxa","price":7345.9597508933875,"createUser":"QAwzk2","id":5721,"stock":1240,"skuCode":"XVvDakho"}
     * @apiSuccess (响应结果) {Number} response
     * @apiSuccessExample 响应结果示例
     * 2910
     */
    @ApiOperation("商品规格参数：修改")
    @PostMapping("/update")
    public int update(@RequestBody PmsSkuStock skuStock) throws Exception {
//        Assert.notNull(sysDict.getId(), "id is not null [ 主键不能为空 ]");
        return skuStockServiceImpl.updateByPkSelective(skuStock.getId(), skuStock);
    }

    @ApiOperation("sku：批量刷新")
    @PostMapping("/replaceList/{productId}")
    public int replaceList(@PathVariable("productId") Long id,
                           @RequestBody List<PmsSkuStock> lists) throws Exception {
        return skuStockServiceImpl.replaceList(id, lists);
    }
}
