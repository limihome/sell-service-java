package cn.sell.admin.pms.attribute;

import cn.sell.comm.model.qo.PageQO;
import cn.sell.comm.model.vo.PageVO;
import cn.sell.comm.result.annotation.ResponseResult;
import cn.sell.mbg.pms.attribute.ProductAttributeParams;
import cn.sell.mbg.pms.attribute.service.AttributeParamsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClassName:AttributeCategory
 * @Description: 商品属性值
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Date: 2020/3/23 15:14
 * @Version: 1.0
 **/

@Log4j2
@RestController
@ResponseResult // 正常情况下统一返回格式需要增加这个注解，错误情况架构会自动捕捉异常
@RequestMapping("/rest/attribute/params")
@Api(tags = "AttributeController - 商品参数信息")
@ApiResponses({
        @ApiResponse(code = 200, message = "请求正常"),
        @ApiResponse(code = 4001, message = "系统异常")
})
public class AttributeParamsController {

    @Autowired
    private AttributeParamsService attributeValueServiceImpl;

    @PostMapping("/list")
    @ApiOperation("商品参数：支持条件、分页、排序查询，不支持模糊查询")
    public PageVO<ProductAttributeParams> list(@RequestBody ProductAttributeParams attributeValue,
                                              @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                              @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        PageQO pageQO = new PageQO(pageNum, pageSize);
        pageQO.setCondition(attributeValue);
        return attributeValueServiceImpl.selectPage(pageQO);
    }

    @PostMapping("/list/tree")
    @ApiOperation("查询属性分类value，返回tree")
    public List<ProductAttributeParams> listToTree() {
        List<ProductAttributeParams> productAttributeCategories = attributeValueServiceImpl.selectAll();
        return productAttributeCategories;
    }

    @PostMapping("/add")
    @ApiOperation("商品参数信息：新增")
    public Long add(@RequestBody ProductAttributeParams attributeValue) throws Exception {
        Assert.notNull(attributeValue.getProductAttributeId(), "productAttributeId is not null [ 分类名称不能为空 ]");
        return attributeValueServiceImpl.insert(attributeValue);
    }

    @PostMapping("/update")
    @ApiOperation("商品参数信息：修改")
    public int update(@RequestBody ProductAttributeParams attributeValue) {
        Assert.notNull(attributeValue.getId(), "id is not null [ 主键不能为空 ]");
        return attributeValueServiceImpl.updateByPkSelective(attributeValue.getId(), attributeValue);
    }

    @GetMapping("/delete/{id}")
    @ApiOperation("商品参数信息：删除")
    public int delete(@PathVariable("id") Long id) {
        Assert.notNull(id, "id is not null [ 主键不能为空 ]");
        return attributeValueServiceImpl.deleteByPk(id);
    }
}
