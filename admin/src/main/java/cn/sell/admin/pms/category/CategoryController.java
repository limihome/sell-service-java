package cn.sell.admin.pms.category;

import cn.sell.comm.model.qo.PageQO;
import cn.sell.comm.model.vo.PageVO;
import cn.sell.comm.result.annotation.ResponseResult;
import cn.sell.mbg.pms.category.PmsProductCategory;
import cn.sell.mbg.pms.category.service.PmsProductCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClassName:CategoryController
 * @Description: TODO: 商品分类，
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Date: 2020/3/23 11:52
 * @Version: 1.0
 **/
@Log4j2
@RestController
@ResponseResult // 正常情况下统一返回格式需要增加这个注解，错误情况架构会自动捕捉异常
@RequestMapping("/rest/product/category")
@Api(tags = "CategoryController - 商品分类管理")
@ApiResponses({
        @ApiResponse(code = 200, message = "请求正常"),
        @ApiResponse(code = 4001, message = "系统异常")
})
public class CategoryController {

    @Autowired
    private PmsProductCategoryService productCategoryServiceImpl;

    @ApiOperation("查询商品分类数据，支持条件、分页查询，不支持模糊查询")
    @PostMapping("/list")
    public PageVO<PmsProductCategory> list(@RequestBody PmsProductCategory productCategory,
                                @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) throws Exception {
        PageQO pageQO = new PageQO(pageNum, pageSize);
        pageQO.setCondition(productCategory);
        return productCategoryServiceImpl.selectPage(pageQO);
    }

    @ApiOperation("查询商品分类数据：返回tree")
    @PostMapping("/tree")
    public List<PmsProductCategory> toTree() throws Exception {
        return productCategoryServiceImpl.toTree();
    }

    @ApiOperation("查询商品分类数据：根据id查询")
    @GetMapping("/get/{categoryId}")
    public PmsProductCategory getCategoryById(@PathVariable Long categoryId) throws Exception {
        return productCategoryServiceImpl.selectByPk(categoryId);
    }

    /**
     * @api {POST} /rest/category/add 新增
     * @apiVersion 1.0.0
     * @apiGroup 商品分类管理
     * @apiName add
     * @apiParam (请求体) {Number} id id
     * @apiParam (请求体) {Number} parentId 上级分类的编号：0表示一级分类
     * @apiParam (请求体) {String} name 名称
     * @apiParam (请求体) {Number} level 分类级别：0->1级；1->2级
     * @apiParam (请求体) {Number} productCount 商品数量
     * @apiParam (请求体) {String} productUnit 商品单位
     * @apiParam (请求体) {Number} navStatus 是否显示在导航栏：0->不显示；1->显示
     * @apiParam (请求体) {Number} showStatus 显示状态：0->不显示；1->显示
     * @apiParam (请求体) {Number} sort 排序
     * @apiParam (请求体) {String} icon 图标
     * @apiParam (请求体) {String} keywords 关键字
     * @apiParam (请求体) {String} description 描述
     * @apiParam (请求体) {String} version 版本号
     * @apiParam (请求体) {String} createUser 创建人
     * @apiParam (请求体) {String} createTime 创建时间
     * @apiParam (请求体) {String} updateUser 修改人
     * @apiParam (请求体) {String} updateTime 修改时间
     * @apiParamExample 请求体示例
     * {"keywords":"ZFTwq6","level":2198,"icon":"4","description":"MWDbg","updateUser":"J","showStatus":381,"updateTime":"u61axY7SZ","productUnit":"S15","sort":178,"productCount":8140,"version":"28NR","parentId":9813,"createTime":"SAAK2KE","name":"r4","createUser":"TLRMZdh","id":8513,"navStatus":8586}
     * @apiSuccess (响应结果) {Number} response
     * @apiSuccessExample 响应结果示例
     * 8665
     */
    @ApiOperation("商品分类数据：新增")
    @PostMapping("/add")
    public Long add(@RequestBody PmsProductCategory productCategory) throws Exception {
        Assert.notNull(productCategory.getName(), "name is not null [ 分类名称不能为空 ]");
        return productCategoryServiceImpl.create(productCategory);
    }

    /**
     * @api {POST} /rest/category/update 修改
     * @apiVersion 1.0.0
     * @apiGroup 商品分类管理
     * @apiName update
     * @apiParam (请求体) {Number} id id
     * @apiParam (请求体) {Number} parentId 上级分类的编号：0表示一级分类
     * @apiParam (请求体) {String} name 名称
     * @apiParam (请求体) {Number} level 分类级别：0->1级；1->2级
     * @apiParam (请求体) {Number} productCount 商品数量
     * @apiParam (请求体) {String} productUnit 商品单位
     * @apiParam (请求体) {Number} navStatus 是否显示在导航栏：0->不显示；1->显示
     * @apiParam (请求体) {Number} showStatus 显示状态：0->不显示；1->显示
     * @apiParam (请求体) {Number} sort 排序
     * @apiParam (请求体) {String} icon 图标
     * @apiParam (请求体) {String} keywords 关键字
     * @apiParam (请求体) {String} description 描述
     * @apiParam (请求体) {String} version 版本号
     * @apiParam (请求体) {String} createUser 创建人
     * @apiParam (请求体) {String} createTime 创建时间
     * @apiParam (请求体) {String} updateUser 修改人
     * @apiParam (请求体) {String} updateTime 修改时间
     * @apiParamExample 请求体示例
     * {"keywords":"FDrRoQ1sMx","level":7441,"icon":"Ak0guc","description":"rhKYY0","updateUser":"n0Oufe6l","showStatus":467,"updateTime":"7V6YrdHB","productUnit":"1y","sort":1146,"productCount":7250,"version":"p7jkrUHjHN","parentId":9929,"createTime":"CNuBZxr","name":"N0IXd","createUser":"A","id":2761,"navStatus":9403}
     * @apiSuccess (响应结果) {Number} response
     * @apiSuccessExample 响应结果示例
     * 1689
     */
    @ApiOperation("商品分类数据：修改")
    @PostMapping("/update")
    public int update(@RequestBody PmsProductCategory productCategory) throws Exception {
        Assert.notNull(productCategory.getId(), "id is not null [ 主键不能为空 ]");
        return productCategoryServiceImpl.updateByPkSelective(productCategory.getId(),productCategory);
    }

    /**
     * @api {POST} /rest/category/delete/{id} 删除
     * @apiVersion 1.0.0
     * @apiGroup 商品分类管理
     * @apiName delete
     * @apiParam (请求参数) {Number} id
     * @apiParamExample 请求参数示例
     * id=3218
     * @apiSuccess (响应结果) {Number} response
     * @apiSuccessExample 响应结果示例
     * 2643
     */
    @ApiOperation("商品分类数据：删除")
    @GetMapping("/delete/{id}")
    public int delete(@PathVariable("id") Long id) throws Exception {
        Assert.notNull(id, "id is not null [ 主键不能为空 ]");
        return productCategoryServiceImpl.deleteByPk(id);
    }
}
