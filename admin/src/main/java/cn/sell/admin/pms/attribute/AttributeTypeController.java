package cn.sell.admin.pms.attribute;

import cn.sell.comm.model.qo.PageQO;
import cn.sell.comm.model.vo.PageVO;
import cn.sell.comm.result.annotation.ResponseResult;
import cn.sell.mbg.pms.attribute.ProductAttributeType;
import cn.sell.mbg.pms.attribute.service.AttributeTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClassName:AttributeCategory
 * @Description: 商品属性分类
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Date: 2020/3/23 15:14
 * @Version: 1.0
 **/

@Log4j2
@RestController
@ResponseResult // 正常情况下统一返回格式需要增加这个注解，错误情况架构会自动捕捉异常
@RequestMapping("/rest/attribute/type")
@Api(tags = "AttributeController - 商品类型管理")
@ApiResponses({
        @ApiResponse(code = 200, message = "请求正常"),
        @ApiResponse(code = 4001, message = "系统异常")
})
public class AttributeTypeController {

    @Autowired
    private AttributeTypeService attributeTypeServiceImpl;

    @ApiOperation("类型管理，支持条件、分页查询，不支持模糊查询")
    @PostMapping("/list")
    public PageVO<ProductAttributeType> list(@RequestBody ProductAttributeType attributeType,
                                           @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                           @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) throws Exception {
        PageQO pageQO = new PageQO(pageNum, pageSize);
        pageQO.setCondition(attributeType);
        return attributeTypeServiceImpl.selectPage(pageQO);
    }


    @ApiOperation("类型管理：查询所有返回tree")
    @PostMapping("/list/tree")
    public List<ProductAttributeType> listToTree() {
        List<ProductAttributeType> productAttributeCategories = attributeTypeServiceImpl.selectAll();
        return productAttributeCategories;
    }


    @ApiOperation("类型管理：新增")
    @PostMapping("/add")
    public Long add(@RequestBody ProductAttributeType attributeCategory) throws Exception {
        Assert.notNull(attributeCategory.getName(), "name is not null [ 分类名称不能为空 ]");
        return attributeTypeServiceImpl.insert(attributeCategory);
    }

    @ApiOperation("类型管理：修改")
    @PostMapping("/update")
    public int update(@RequestBody ProductAttributeType attributeCategory) {
        Assert.notNull(attributeCategory.getId(), "id is not null [ 主键不能为空 ]");
        return attributeTypeServiceImpl.updateByPkSelective(attributeCategory.getId(),attributeCategory);
    }

    @ApiOperation("类型管理：删除")
    @GetMapping("/delete/{id}")
    public int delete(@PathVariable("id") Long id) {
        Assert.notNull(id, "id is not null [ 主键不能为空 ]");
        return attributeTypeServiceImpl.deleteByPk(id);
    }
}
