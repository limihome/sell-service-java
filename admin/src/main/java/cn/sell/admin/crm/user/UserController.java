package cn.sell.admin.crm.user;

import cn.sell.comm.model.qo.PageQO;
import cn.sell.comm.model.vo.PageVO;
import cn.sell.comm.result.annotation.ResponseResult;
import cn.sell.comm.result.exceptions.DataNotFoundException;
import cn.sell.mbg.crm.user.UmsUser;
import cn.sell.mbg.crm.user.service.UmsUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Description: TODO: 用户管理
 * @Date: 2020/3/23 9:40
 * @Version: 1.0
 **/

@Log4j2
@RestController
@ResponseResult // 正常情况下统一返回格式需要增加这个注解，错误情况架构会自动捕捉异常
@RequestMapping("/rest/user")
@Api(tags = "UserController - 用户管理")
@ApiResponses({
        @ApiResponse(code = 200, message = "请求正常"),
        @ApiResponse(code = 4001, message = "系统异常")
})
public class UserController {

    @Autowired
    private UmsUserService umsUserServiceImpl;

    @ApiOperation("会员信息查询:(分页查询，前端查询某个会员时也可以使用这个接口查询)")
    @PostMapping("/login")
    public UmsUser login(@RequestBody UmsUser umsUser) throws Exception {
        Assert.notNull(umsUser.getUsername(), "userName is not null [ 用户名不能为空 ]");
        Assert.notNull(umsUser.getPassword(), "password is not null [ 密码不能为空 ]");
        PageQO pageQO = new PageQO(1, 5);
        UmsUser user = new UmsUser();
        user.setPhone(umsUser.getPhone());
        user.setUsername(umsUser.getUsername());
        user.setPassword(umsUser.getPassword());
        pageQO.setCondition(user);
        PageVO<UmsUser> page = umsUserServiceImpl.selectPage(pageQO);
        List<UmsUser> list = page.getList();
        if (null != list && list.size() == 1) {
            return list.get(0);
        } else {
            throw new DataNotFoundException();
        }
    }

    @ApiOperation("会员信息查询:(分页查询，前端查询某个会员时也可以使用这个接口查询)")
    @PostMapping("/list")
    public PageVO<UmsUser> list(@RequestBody UmsUser umsUser,
                                @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) throws Exception {
        PageQO pageQO = new PageQO(pageNum, pageSize);
        pageQO.setCondition(umsUser);
        return umsUserServiceImpl.selectPage(pageQO);
    }

    @ApiOperation("会员注册:(发送短信时，将验证码返回给前端，由前端直接验证。不在后台接口中验证)")
    @PostMapping("/register")
    public UmsUser add(@RequestBody UmsUser umsUser) throws Exception {
        Assert.notNull(umsUser.getPhone(), "phone is not null [ 手机号不能为空 ]");
        Assert.notNull(umsUser.getNickname(), "Nickname is not null [ 真实名称不能为空 ]");
        Assert.notNull(umsUser.getPassword(), "Password is not null [ 密码不能为空 ]");
        return umsUserServiceImpl.register(umsUser);
    }

    @ApiOperation("会员信息修改：（会员信息修改，所有信息都可以修改）")
    @PostMapping("/update")
    public int update(@RequestBody UmsUser umsUser) {
        Assert.notNull(umsUser.getId(), "id is not null [ 主键不能为空 ]");
        return umsUserServiceImpl.updateByPk(umsUser.getId(), umsUser);
    }

    @ApiOperation("会员信息修改：根据手机号修改信息（手机号是唯一的）")
    @PostMapping("/updateByPhone")
    public UmsUser updateByPhone(@RequestBody UmsUser umsUser) throws Exception {
        Assert.notNull(umsUser.getPhone(), "phone is not null [ 手机号不能为空 ]");
        return umsUserServiceImpl.updateByPhone(umsUser);
    }
}
