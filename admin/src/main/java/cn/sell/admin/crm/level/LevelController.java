package cn.sell.admin.crm.level;

import cn.sell.comm.model.qo.PageQO;
import cn.sell.comm.model.vo.PageVO;
import cn.sell.comm.result.annotation.ResponseResult;
import cn.sell.mbg.crm.level.UmsUserLevel;
import cn.sell.mbg.crm.level.service.UmsUserLevelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName:LevelController
 * @Description: TODO:
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Date: 2020/7/21 15:13
 * @Version: 1.0
 **/
@Log4j2
@RestController
@ResponseResult // 正常情况下统一返回格式需要增加这个注解，错误情况架构会自动捕捉异常
@RequestMapping("/rest/user/level")
@Api(tags = "LevelController - 会员等级管理")
@ApiResponses({
        @ApiResponse(code = 200, message = "请求正常"),
        @ApiResponse(code = 4001, message = "系统异常")
})
public class LevelController {

    @Autowired
    private UmsUserLevelService levelServiceImpl;

    @ApiOperation("等级管理：查询")
    @PostMapping("/list")
    public PageVO<UmsUserLevel> list(@RequestBody UmsUserLevel userLevel,
                                     @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                     @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        PageQO pageQO = new PageQO(pageNum, pageSize);
        pageQO.setCondition(userLevel);
        return levelServiceImpl.selectPage(pageQO);
    }

    @ApiOperation("等级管理：新增")
    @PostMapping("/add")
    public Long add(@RequestBody UmsUserLevel userLevel) throws Exception {
        Assert.notNull(userLevel.getName(), "name is not null [ 收货人名称不能为空 ]");
        return levelServiceImpl.insert(userLevel);
    }

    @ApiOperation("等级管理：修改")
    @PostMapping("/update")
    public int update(@RequestBody UmsUserLevel userLevel) throws Exception {
        Assert.notNull(userLevel.getName(), "name is not null [ 收货人名称不能为空 ]");
        return levelServiceImpl.updateByPk(userLevel.getId(), userLevel);
    }

    @ApiOperation("等级管理：删除")
    @GetMapping("/delete/{id}")
    public int delete(@PathVariable("id") Long id) throws Exception {
        return levelServiceImpl.deleteByPk(id);
    }
}
