package cn.sell.admin.oms.reason;

import cn.sell.comm.model.qo.PageQO;
import cn.sell.comm.model.vo.PageVO;
import cn.sell.comm.result.annotation.ResponseResult;
import cn.sell.mbg.oms.reason.OrderReturnReason;
import cn.sell.mbg.oms.reason.service.OrderReturnReasonService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Description: TODO: 退货申请
 * @Date: 2020/3/23 9:39
 * @Version: 1.0
 **/

@Log4j2
@RestController
@ResponseResult // 正常情况下统一返回格式需要增加这个注解，错误情况架构会自动捕捉异常
@RequestMapping("/rest/reason")
@Api(tags = "ReasonController - 退货申请")
@ApiResponses({
        @ApiResponse(code = 200, message = "请求正常"),
        @ApiResponse(code = 4001, message = "系统异常")
})
public class ReasonController {

    @Autowired
    private OrderReturnReasonService returnReasonService;

    @ApiOperation("查询退货申请表数据，支持条件、分页查询，不支持模糊查询")
    @PostMapping("/list")
    public PageVO<OrderReturnReason> list(@RequestBody OrderReturnReason orderReturnReason,
                                          @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                          @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) throws Exception {
        PageQO pageQO = new PageQO(pageNum, pageSize);
        pageQO.setCondition(orderReturnReason);
        return returnReasonService.selectPage(pageQO);
    }

    @ApiOperation("退货申请：修改")
    @PostMapping("/add")
    public Long add(@RequestBody OrderReturnReason orderReturnReason) throws Exception {
        Assert.notNull(orderReturnReason.getId(), "id is not null [ 主键不能为空 ]");
        return returnReasonService.insert(orderReturnReason);
    }

    @ApiOperation("退货申请：修改")
    @PostMapping("/update")
    public int update(@RequestBody OrderReturnReason orderReturnReason) throws Exception {
        Assert.notNull(orderReturnReason.getId(), "id is not null [ 主键不能为空 ]");
        return returnReasonService.updateByPkSelective(orderReturnReason.getId(), orderReturnReason);
    }

    @ApiOperation("退货申请：删除（物理删除）")
    @PostMapping("/delete")
    public int delete(@RequestBody List<Long> ids) throws Exception {
        return returnReasonService.deleteByPks(ids);
    }
}
