package cn.sell.admin.oms.apply;

import cn.sell.comm.model.qo.PageQO;
import cn.sell.comm.model.vo.PageVO;
import cn.sell.comm.result.annotation.ResponseResult;
import cn.sell.mbg.oms.apply.OrderReturnApply;
import cn.sell.mbg.oms.apply.service.OrderReturnApplyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Description: TODO: 字典管理
 * @Date: 2020/3/23 9:39
 * @Version: 1.0
 **/

@Log4j2
@RestController
@ResponseResult // 正常情况下统一返回格式需要增加这个注解，错误情况架构会自动捕捉异常
@RequestMapping("/rest/apply")
@Api(tags = "ApplyController - 退货申请")
@ApiResponses({
        @ApiResponse(code = 200, message = "请求正常"),
        @ApiResponse(code = 4001, message = "系统异常")
})
public class ApplyController {
    @Autowired
    private OrderReturnApplyService returnApplyService;

    @ApiOperation("退货申请：支持条件、分页、排序查询，不支持模糊查询")
    @PostMapping("/list")
    public PageVO<OrderReturnApply> list(@RequestBody OrderReturnApply orderReturnApply,
                                         @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                         @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) throws Exception {
        PageQO pageQO = new PageQO(pageNum, pageSize);
        pageQO.setCondition(orderReturnApply);
        pageQO.setOrderBy("create_time desc");
        return returnApplyService.selectPage(pageQO);
    }

    @ApiOperation("字典表数据：修改")
    @GetMapping("/get/{id}")
    public OrderReturnApply add(@PathVariable("id") Long id) throws Exception {
        return returnApplyService.selectByPk(id);
    }

    @ApiOperation("字典表数据：修改")
    @PostMapping("/update")
    public int update(@RequestBody OrderReturnApply orderReturnApply) throws Exception {
        Assert.notNull(orderReturnApply.getId(), "id is not null [ 主键不能为空 ]");
        return returnApplyService.updateByPkSelective(orderReturnApply.getId(), orderReturnApply);
    }

    @ApiOperation("字典表数据：删除（物理删除）")
    @PostMapping("/delete")
    public int delete(@RequestBody List<Long> ids) throws Exception {
        return returnApplyService.deleteByPks(ids);
    }
}
