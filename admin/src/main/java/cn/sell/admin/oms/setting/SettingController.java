package cn.sell.admin.oms.setting;

import cn.sell.comm.result.annotation.ResponseResult;
import cn.sell.mbg.oms.setting.OmsOrderSetting;
import cn.sell.mbg.oms.setting.service.OmsOrderSettingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName:SettingController
 * @Description: TODO:
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Date: 2020/7/21 22:44
 * @Version: 1.0
 **/
@Log4j2
@RestController
@ResponseResult // 正常情况下统一返回格式需要增加这个注解，错误情况架构会自动捕捉异常
@RequestMapping("/rest/order/setting")
@Api(tags = "SettingController - 订单设置")
@ApiResponses({
        @ApiResponse(code = 200, message = "请求正常"),
        @ApiResponse(code = 4001, message = "系统异常")
})
public class SettingController {

    @Autowired
    private OmsOrderSettingService settingServiceImpl;

    @ApiOperation("订单设置：查询")
    @GetMapping("/get/{id}")
    public OmsOrderSetting list(@PathVariable("id") Long Id) {
        return settingServiceImpl.selectByPk(Id);
    }

    @ApiOperation("订单设置：查询")
    @PostMapping("/update")
    public int list(@RequestBody OmsOrderSetting orderSetting) {
        return settingServiceImpl.updateByPkSelective(orderSetting.getId(),orderSetting);
    }
}
