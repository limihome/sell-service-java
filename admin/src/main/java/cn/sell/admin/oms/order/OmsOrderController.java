package cn.sell.admin.oms.order;

import cn.sell.comm.model.qo.PageQO;
import cn.sell.comm.model.vo.PageVO;
import cn.sell.comm.result.annotation.ResponseResult;
import cn.sell.mbg.oms.order.OmsOrder;
import cn.sell.mbg.oms.order.service.OmsOrderService;
import cn.sell.mbg.oms.order.view.OmsOrderDetail;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName:OmsOrderController
 * @Description: TODO:
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Date: 2020/7/21 22:31
 * @Version: 1.0
 **/
@Log4j2
@RestController
@ResponseResult // 正常情况下统一返回格式需要增加这个注解，错误情况架构会自动捕捉异常
@RequestMapping("/rest/order")
@Api(tags = "OmsOrderController - 订单管理")
@ApiResponses({
        @ApiResponse(code = 200, message = "请求正常"),
        @ApiResponse(code = 4001, message = "系统异常")
})
public class OmsOrderController {
    @Autowired
    OmsOrderService orderService;

    @ApiOperation("订单管理：查询")
    @PostMapping("/list")
    public PageVO<OmsOrder> list(@RequestBody OmsOrder order,
                                 @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                 @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        PageQO pageQO = new PageQO(pageNum, pageSize);
        pageQO.setCondition(order);
        pageQO.setOrderBy("create_time DESC");
        return orderService.selectPage(pageQO);
    }

    @ApiOperation("订单管理：查询")
    @GetMapping("/get/{id}")
    public OmsOrderDetail getOrderById(@PathVariable("id") Long orderId) {
        return orderService.getOrderById(orderId);
    }

    @ApiOperation("订单管理：查询")
    @PostMapping("/update")
    public int update(@RequestBody OmsOrder order) {
        return orderService.updateByPkSelective(order.getId(),order);
    }

    @ApiOperation("订单管理：查询")
    @PostMapping("/close")
    public int close(@RequestBody OmsOrder order) {
        return orderService.close(order);
    }
}
