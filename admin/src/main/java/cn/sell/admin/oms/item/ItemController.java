package cn.sell.admin.oms.item;

import cn.sell.comm.model.qo.PageQO;
import cn.sell.comm.model.vo.PageVO;
import cn.sell.comm.result.annotation.ResponseResult;
import cn.sell.mbg.oms.item.OmsOrderItem;
import cn.sell.mbg.oms.item.service.OrderItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClassName:LevelController
 * @Description: TODO:
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Date: 2020/7/21 15:13
 * @Version: 1.0
 **/
@Log4j2
@RestController
@ResponseResult // 正常情况下统一返回格式需要增加这个注解，错误情况架构会自动捕捉异常
@RequestMapping("/rest/order/item")
@Api(tags = "ItemController - 订单商品信息")
@ApiResponses({
        @ApiResponse(code = 200, message = "请求正常"),
        @ApiResponse(code = 4001, message = "系统异常")
})
public class ItemController {

    @Autowired
    private OrderItemService orderItemServiceImpl;

    @ApiOperation("订单商品信息：查询")
    @PostMapping("/list")
    public PageVO<OmsOrderItem> list(@RequestBody OmsOrderItem orderItem,
                                     @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                     @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        PageQO pageQO = new PageQO(pageNum, pageSize);
        pageQO.setCondition(orderItem);
        return orderItemServiceImpl.selectPage(pageQO);
    }

    @ApiOperation("订单商品信息：查询")
    @PostMapping("/get/{orderId}")
    public List<OmsOrderItem> getItemByOrderId(
            @PathVariable("orderId") Long orderId) {
        OmsOrderItem omsOrderItem = new OmsOrderItem();
        omsOrderItem.setOrderId(orderId);
        return orderItemServiceImpl.selectByEntity(omsOrderItem);
    }
}
