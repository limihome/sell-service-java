package cn.sell.admin.sys.admin;

import cn.sell.comm.model.qo.PageQO;
import cn.sell.comm.model.vo.PageVO;
import cn.sell.comm.result.annotation.ResponseResult;
import cn.sell.mbg.sys.admin.SysAdmin;
import cn.sell.mbg.sys.admin.service.SysAdminService;
import cn.sell.mbg.sys.adminrole.service.UmsAdminRoleRelationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Map;

/**
 * @ClassName:AdminController
 * @Description: 管理员管理
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Date: 2020/6/28 9:49
 * @Version: 1.0
 **/
@Log4j2
@ResponseBody
@RestController
@ResponseResult // 正常情况下统一返回格式需要增加这个注解，错误情况架构会自动捕捉异常
@RequestMapping("/rest/admin")
@Api(tags = "AdminController - 管理员管理")
@ApiResponses({
        @ApiResponse(code = 200, message = "请求正常"),
        @ApiResponse(code = 4001, message = "系统异常")
})
public class AdminController {

    @Autowired
    private SysAdminService sysAdminServiceImpl;

    @Autowired
    private UmsAdminRoleRelationService umsAdminRoleRelationService;

    @ApiOperation("管理员管理：支持条件、分页、排序查询，不支持模糊查询")
    @PostMapping("/list")
    public PageVO<SysAdmin> list(@RequestBody SysAdmin sysAdmin,
                                 @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                 @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) throws Exception {
        PageQO pageQO = new PageQO(pageNum, pageSize);
        pageQO.setCondition(sysAdmin);
        return sysAdminServiceImpl.selectPage(pageQO);
    }

    @ApiOperation("后台用户登录")
    
    @PostMapping("/login")
    public SysAdmin login(@RequestBody SysAdmin sysAdmin, HttpServletRequest request) throws Exception {
        return sysAdminServiceImpl.login(sysAdmin, request);
    }


    @ApiOperation(value = "获取当前登录用户信息")
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public Map<String, Object> getAdminInfo(
            Principal principal) {
//        if (principal == null) {
//            throw new RuntimeException("值不能为空");
//        }
//        log.info("principal:", JSON.toJSONString(principal));
//        String username = principal.getName();
//        UmsAdmin umsAdmin = umsAdminServiceImpl.getAdminByUsername(username);
//        Map<String, Object> data = new HashMap<>();
//        data.put("username", umsAdmin.getUsername());
//        data.put("roles", new String[]{"TEST"});
//        data.put("menus", umsAdminRoleRelationService.getCheckedRole(umsAdmin.getId()));
//        data.put("icon", umsAdmin.getIcon());
        return null;
    }


    @ApiOperation("新增后台管理员用户")
    @PostMapping("/create")
    public Long create(@RequestBody SysAdmin sysAdmin) throws Exception {
        Assert.notNull(sysAdmin.getPassword(), "Password is not null [ 密码不能为空 ]");
        Assert.notNull(sysAdmin.getNickName(), "nickName is not null [ 昵称不能为空 ]");
        Assert.notNull(sysAdmin.getUsername(), "userName is not null [ 用户名称不能为空 ]");
        return sysAdminServiceImpl.insert(sysAdmin);
    }

    @ApiOperation("删除后台管理员用户")
    @GetMapping("/delete/{id}")
    public int delete(@PathVariable("id") Long id) throws Exception {
        Assert.notNull(id, "id is not null [ 管理员id不能为空 ]");
        umsAdminRoleRelationService.deleteByAdminId(id);
        return sysAdminServiceImpl.deleteByPk(id);
    }

    @ApiOperation("新增后台管理员用户")
    @PostMapping("/update")
    public int update(@RequestBody SysAdmin sysAdmin) throws Exception {
        Assert.notNull(sysAdmin.getId(), "id is not null [ 管理员id不能为空 ]");
        return sysAdminServiceImpl.updateByPkSelective(sysAdmin.getId(), sysAdmin);
    }

}
