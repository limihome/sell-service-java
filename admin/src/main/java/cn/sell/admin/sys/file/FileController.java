package cn.sell.admin.sys.file;

import cn.sell.mbg.sys.file.SysFile;
import cn.sell.mbg.sys.file.service.SysFileService;
import io.swagger.annotations.*;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @ClassName:FileController
 * @Description: TODO:
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Date: 2020/4/1 14:05
 * @Version: 1.0
 **/

@Log4j2
@RestController
//@ResponseResult // 正常情况下统一返回格式需要增加这个注解，错误情况架构会自动捕捉异常
@RequestMapping("/file")
@Api(tags = "FileController", description = "文件上传")
@ApiResponses({
        @ApiResponse(code = 200, message = "请求正常"),
        @ApiResponse(code = 4001, message = "系统异常")
})
public class FileController{

    @Value("${file.base.path}")
    String filePath;

    @Autowired
    private SysFileService sysFileServiceImpl;

    /**
     * @api {POST} /file/uploads 上传文件
     * @apiVersion 1.0.0
     * @apiGroup 文件上传
     * @apiName uploadFile
     * @apiDescription 可批量上传文件
     * @apiParam (请求参数) {String} type
     * @apiParamExample 请求参数示例
     * type=qMISoeg
     * @apiSuccess (响应结果) {Object} response
     * @apiSuccessExample 响应结果示例
     * null
     * @return
     */
    @ApiOperation("商品图片批量上传")
    @RequestMapping(value = "/uploads", method = RequestMethod.POST)
    @ApiImplicitParam(name = "type", value = "待定", required = false, paramType = "query", dataType = "String")
    public List<SysFile> uploadFile(HttpServletRequest request,
                                    @RequestParam(value = "type", required = false) String type) throws Exception {
        String filePath = "E:";
        return sysFileServiceImpl.uploadFile(request,filePath);
    }

    @ApiOperation("商品图片批量上传")
    @RequestMapping(value = "/markdown", method = RequestMethod.POST)
    public List<SysFile> markdownUploadFile(HttpServletRequest request) throws Exception {
        return sysFileServiceImpl.uploadFile(request,filePath);
    }

}
