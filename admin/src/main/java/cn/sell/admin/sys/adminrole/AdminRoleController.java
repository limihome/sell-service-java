package cn.sell.admin.sys.adminrole;

import cn.sell.comm.result.annotation.ResponseResult;
import cn.sell.mbg.sys.adminrole.service.UmsAdminRoleRelationService;
import cn.sell.mbg.sys.role.UmsRole;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Log4j2
@ResponseBody
@ResponseResult
@RestController
@RequestMapping("/rest/adminRole")
@Api(tags = "AdminRoleController - 管理员的角色分配管理")
@ApiResponses({
        @ApiResponse(code = 200, message = "请求正常"),
        @ApiResponse(code = 4001, message = "系统异常")
})
public class AdminRoleController {

    @Autowired
    private UmsAdminRoleRelationService umsAdminRoleRelationService;

    @ApiOperation("获取管理员角色")
    @GetMapping("/list/{adminId}")
    public List<UmsRole> list(@PathVariable("adminId") Long adminId) {
        Assert.notNull(adminId, "adminId is not null [ 管理员id不能为空 ]");
        return umsAdminRoleRelationService.getCheckedRole(adminId);
    }

    @ApiOperation("更新或创建管理员角色")
    @PostMapping("/saveOrUpdateChecked")
    public Long saveOrUpdateChecked(Long adminId, String roleIds) {
        return umsAdminRoleRelationService.saveOrUpdateChecked(adminId, roleIds);
    }

}


