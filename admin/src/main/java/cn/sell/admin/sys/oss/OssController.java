package cn.sell.admin.sys.oss;

import cn.sell.comm.result.annotation.ResponseResult;
import cn.sell.mbg.sys.oss.dto.OssCallbackResult;
import cn.sell.mbg.sys.oss.service.OssService;
import cn.sell.mbg.sys.oss.vo.OssPolicyVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * @ClassName:OssController
 * @Description: TODO:
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Date: 2020/7/26 20:55
 * @Version: 1.0
 **/
@Api(tags = "OssController - Oss管理")
@Controller
@ResponseBody
@ResponseResult // 正常情况下统一返回格式需要增加这个注解，错误情况架构会自动捕捉异常
@RequestMapping("/rest/oss")
@ApiResponses({
        @ApiResponse(code = 200, message = "请求正常"),
        @ApiResponse(code = 4001, message = "系统异常")
})
public class OssController {

    @Autowired
    OssService ossService;

    @ApiOperation(value = "oss上传签名生成")
    @RequestMapping(value = "/policy", method = RequestMethod.GET)
    public OssPolicyVO policy() {
        OssPolicyVO result = ossService.policy();
        return result;
    }

    @ApiOperation(value = "oss上传成功回调")
    @RequestMapping(value = "callback", method = RequestMethod.POST)
    public OssCallbackResult callback(HttpServletRequest request) {
        OssCallbackResult callback = ossService.callback(request);
        return callback;
    }

}
