package cn.sell.admin.sys.redis;

import cn.sell.comm.result.annotation.ResponseResult;
import cn.sell.mbg.sys.redis.service.RedisStringService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * @ClassName:RedisController
 * @Description: TODO:
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Date: 2020/8/20 18:01
 * @Version: 1.0
 **/
@Log4j2
@RestController
@ResponseResult // 正常情况下统一返回格式需要增加这个注解，错误情况架构会自动捕捉异常
@RequestMapping("/rest/redis")
@Api(tags = "RedisController - redis测试")
@ApiResponses({
        @ApiResponse(code = 200, message = "请求正常"),
        @ApiResponse(code = 4001, message = "系统异常")
})
public class RedisController {
    @Autowired
    private RedisStringService redisStringServiceImpl;

    @ApiOperation("redis设置值")
    @PostMapping("/set")
    public void set(String key, String value,Long timeOut) {
        redisStringServiceImpl.set(key, value, timeOut, TimeUnit.SECONDS);
    }
    @ApiOperation("redis设置值")
    @PostMapping("/get")
    public String get(String key) {
        return redisStringServiceImpl.get(key);
    }
}
