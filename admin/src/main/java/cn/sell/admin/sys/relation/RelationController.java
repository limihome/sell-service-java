package cn.sell.admin.sys.relation;

import cn.sell.comm.result.annotation.ResponseResult;
import cn.sell.mbg.sys.menu.UmsMenu;
import cn.sell.mbg.sys.relation.service.UmsRoleMenuRelationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Log4j2
@ResponseResult
@RestController
@RequestMapping("/rest/relation")
@Api(tags = "RelationController - 角色菜单关系")
@ApiResponses({
        @ApiResponse(code = 200, message = "请求正常"),
        @ApiResponse(code = 4001, message = "系统异常")
})
public class RelationController {

    @Autowired
    private UmsRoleMenuRelationService umsRoleMenuRelationService;

    @ApiOperation("根据角色id显示已选的菜单")
    @PostMapping("/list/{roleId}")
    public List<UmsMenu> list(@PathVariable Long roleId) {
        Assert.notNull(roleId, "RoleId is not null [ 角色id不能为空 ] ");
        return umsRoleMenuRelationService.getCheckedMenu(roleId);
    }

    @ApiOperation("创建或更新角色菜单")
    @PostMapping("/saveOrUpdate")
    public Long saveOrUpdateChecked( Long roleId, String menuIds) {
        Assert.notNull(roleId, "RoleId is not null [ 角色id不能为空 ] ");
        return umsRoleMenuRelationService.saveOrUpdateChecked(roleId, menuIds);
    }

    @ApiOperation("删除角色菜单联系")
    @PostMapping("/delete/{roleId}")
    public int delete(@PathVariable("roleId") Long roleId){
        Assert.notNull(roleId, "RoleId is not null [ 角色id不能为空 ] ");
        return  umsRoleMenuRelationService.deleteByRoleId(roleId);
    }
}
