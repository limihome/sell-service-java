package cn.sell.admin.sys.dict;

import cn.sell.comm.model.qo.PageQO;
import cn.sell.comm.model.vo.PageVO;
import cn.sell.comm.result.annotation.ResponseResult;
import cn.sell.mbg.sys.dict.SysDict;
import cn.sell.mbg.sys.dict.service.SysDictService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

/**
  *@Author: Limi
  *@Contact : qq(2393839633),Email(13924223985@163.com)
  *@Description: TODO: 字典管理
  *@Date: 2020/3/23 9:39
  *@Version: 1.0
  **/

@Log4j2
@RestController
@ResponseResult // 正常情况下统一返回格式需要增加这个注解，错误情况架构会自动捕捉异常
@RequestMapping("/rest/dict")
@Api(tags = "DictController - 字典管理")
@ApiResponses({
        @ApiResponse(code = 200, message = "请求正常"),
        @ApiResponse(code = 4001, message = "系统异常")
})
public class DictController {
    @Autowired
    private SysDictService sysDictServiceImpl;

    /**
     * @api {POST} /rest/dict/list 查询
     * @apiVersion 1.0.0
     * @apiGroup 字典管理
     * @apiName list
     * @apiParam (请求参数) {Number} pageSize
     * @apiParam (请求参数) {Number} pageNum
     * @apiParamExample 请求参数示例
     * pageSize=9187&pageNum=3863
     * @apiParam (请求体) {Number} id id
     * @apiParam (请求体) {String} dictCode 字典类型
     * @apiParam (请求体) {String} dictName 字典名称
     * @apiParam (请求体) {String} dictValue 字典值
     * @apiParam (请求体) {String} dictDesc 字典描述
     * @apiParam (请求体) {String} isDelete 是否删除
     * @apiParam (请求体) {String} version 版本号
     * @apiParam (请求体) {String} createUser 创建人
     * @apiParam (请求体) {String} createTime 创建时间
     * @apiParam (请求体) {String} updateUser 修改人
     * @apiParam (请求体) {String} updateTime 修改时间
     * @apiParamExample 请求体示例
     * {"dictValue":"afSLZoKgYu","dictDesc":"GMEL","dictCode":"G81ZkcH5","createTime":"zerGe4c1sd","isDelete":"DELETE","updateUser":"d35KSMl","dictName":"NgZntayW","createUser":"vfxWki9","updateTime":"og2V86IJ","id":3265,"version":"YD"}
     * @apiSuccess (响应结果) {Number} pageNum
     * @apiSuccess (响应结果) {Number} pageSize
     * @apiSuccess (响应结果) {Number} total
     * @apiSuccess (响应结果) {Number} pages
     * @apiSuccess (响应结果) {Array} list
     * @apiSuccess (响应结果) {Number} list.id id
     * @apiSuccess (响应结果) {String} list.dictCode 字典类型
     * @apiSuccess (响应结果) {String} list.dictName 字典名称
     * @apiSuccess (响应结果) {String} list.dictValue 字典值
     * @apiSuccess (响应结果) {String} list.dictDesc 字典描述
     * @apiSuccess (响应结果) {String} list.isDelete 是否删除
     * @apiSuccess (响应结果) {String} list.version 版本号
     * @apiSuccess (响应结果) {String} list.createUser 创建人
     * @apiSuccess (响应结果) {String} list.createTime 创建时间
     * @apiSuccess (响应结果) {String} list.updateUser 修改人
     * @apiSuccess (响应结果) {String} list.updateTime 修改时间
     * @apiSuccessExample 响应结果示例
     * {"total":1264,"pages":4922,"pageSize":9765,"list":[{"dictValue":"IxY0Vu","dictDesc":"s","dictCode":"2d8o","createTime":"M","isDelete":"DELETE","updateUser":"USnvVafR6","dictName":"f","createUser":"tEJ1d","updateTime":"hzEI5MMI5","id":839,"version":"sDGnW"}],"pageNum":1815}
     */
    @ApiOperation("查询字典表数据，支持条件、分页查询，不支持模糊查询")
    @PostMapping("/list")
    public PageVO<SysDict> list(@RequestBody SysDict sysDict,
                                @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) throws Exception {
        PageQO pageQO = new PageQO(pageNum, pageSize);
        pageQO.setCondition(sysDict);
        PageVO<SysDict> sysDictPageVO = sysDictServiceImpl.selectPage(pageQO);
//        log.info(JSON.parse(sysDictPageVO.getList().get(0)));
        return sysDictServiceImpl.selectPage(pageQO);
    }

    /**
     * @api {POST} /rest/dict/add 新增
     * @apiVersion 1.0.0
     * @apiGroup 字典管理
     * @apiName index
     * @apiDescription 这只是一个测试的接口描述
     * @apiParam (请求体) {Number} id id
     * @apiParam (请求体) {String} dictCode 字典类型
     * @apiParam (请求体) {String} dictName 字典名称
     * @apiParam (请求体) {String} dictValue 字典值
     * @apiParam (请求体) {String} dictDesc 字典描述
     * @apiParam (请求体) {String} isDelete 是否删除
     * @apiParam (请求体) {String} version 版本号
     * @apiParam (请求体) {String} createUser 创建人
     * @apiParam (请求体) {String} createTime 创建时间
     * @apiParam (请求体) {String} updateUser 修改人
     * @apiParam (请求体) {String} updateTime 修改时间
     * @apiParamExample 请求体示例
     * {"dictValue":"T1oaq","dictDesc":"gFRbOs51J","dictCode":"bMpMRL8R","createTime":"TpIFEPtcSS","isDelete":"DELETE","updateUser":"PKm","dictName":"BXuV","createUser":"Ny","updateTime":"nnFgpcPRqA","id":9502,"version":"NlvH47VlU"}
     * @apiSuccess (响应结果) {Number} response
     * @apiSuccessExample 响应结果示例
     * 965
     */
    @ApiOperation("字典表数据：新增")
    @PostMapping("/add")
    public Long add(@RequestBody SysDict sysDict) throws Exception {
        Assert.notNull(sysDict.getDictCode(), "code is not null [ 字典编码不能为空 ]");
        Assert.notNull(sysDict.getDictValue(), "value is not null [ 字典值不能为空 ]");
        Assert.notNull(sysDict.getDictName(), "name is not null [ 字典名称不能为空 ]");
        return sysDictServiceImpl.insert(sysDict);
    }

    /**
     * @api {POST} /rest/dict/update 修改
     * @apiVersion 1.0.0
     * @apiGroup 字典管理
     * @apiName update
     * @apiParam (请求体) {Number} id id
     * @apiParam (请求体) {String} dictCode 字典类型
     * @apiParam (请求体) {String} dictName 字典名称
     * @apiParam (请求体) {String} dictValue 字典值
     * @apiParam (请求体) {String} dictDesc 字典描述
     * @apiParam (请求体) {String} isDelete 是否删除
     * @apiParam (请求体) {String} version 版本号
     * @apiParam (请求体) {String} createUser 创建人
     * @apiParam (请求体) {String} createTime 创建时间
     * @apiParam (请求体) {String} updateUser 修改人
     * @apiParam (请求体) {String} updateTime 修改时间
     * @apiParamExample 请求体示例
     * {"dictValue":"CA","dictDesc":"iRDQ","dictCode":"DTfTtF","createTime":"kvhHq","isDelete":"DELETE","updateUser":"E","dictName":"yNPpZSG","createUser":"mHBxzVNa","updateTime":"FQEE7svVh","id":4303,"version":"I"}
     * @apiSuccess (响应结果) {Number} response
     * @apiSuccessExample 响应结果示例
     * 1131
     */
    @ApiOperation("字典表数据：修改")
    @PostMapping("/update")
    public int update(@RequestBody SysDict sysDict) throws Exception {
        Assert.notNull(sysDict.getId(), "id is not null [ 主键不能为空 ]");
        Assert.notNull(sysDict.getDictCode(), "code is not null [ 字典编码不能为空 ]");
        Assert.notNull(sysDict.getDictValue(), "value is not null [ 字典值不能为空 ]");
        Assert.notNull(sysDict.getDictName(), "name is not null [ 字典名称不能为空 ]");
        return sysDictServiceImpl.updateByPkSelective(sysDict.getId(),sysDict);
    }

    @ApiOperation("字典表数据：删除（物理删除）")
    @GetMapping("/delete/{id}")
    public int delete(@PathVariable("id") Long id) throws Exception {
        return sysDictServiceImpl.deleteByPk(id);
    }
}
