package cn.sell.admin.sys.dysms;


import cn.sell.comm.lock.annotation.EasyLock;
import cn.sell.comm.lock.model.LockType;
import cn.sell.comm.util.DateUtil;
import cn.sell.mbg.sys.sms.constants.SmsConstants;
import cn.sell.mbg.sys.sms.service.IDySmsService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.regex.Pattern;


/**
 * 短信业务
 */
@Log4j2
@RestController
@Api(value = "短信业务")
@ApiResponses({
        @ApiResponse(code = 200, message = "请求正常"),
        @ApiResponse(code = 4001, message = "系统异常")
})
@RequestMapping(value = "/rest/sms", method = {RequestMethod.GET, RequestMethod.POST})
public class IDysmsController {

    @Autowired
    private IDySmsService idysmsService;

    /**
     * 短信发送
     *
     * @param phone
     * @param smsType
     * @return 返回验证码
     * @throws Exception
     */
    @ApiOperation(value = "短信发送", notes = "短信发送")
    @RequestMapping(value = "/sendSms")
    @EasyLock(name = "sms", keys = "#phone", lockType = LockType.Fair, waitTime = 20, leaseTime = 300)
    public String sendSms(@RequestParam(value = "phone", required = false) String phone,
                          @RequestParam(value = "smsType", required = false) String smsType) throws Exception {

        if (phone.isEmpty() || smsType.isEmpty() || "".equals(phone) || "".equals(smsType)) {
            Assert.notNull(phone, "phone is not null [ 手机号不能为空 ]");
            Assert.notNull(smsType, "smsType is not null [ 短信类型不能为空 ]");
        }

        // 发送短信类型
        boolean partnerStatusFlag = Arrays.asList(SmsConstants.SMS_TYPE_ARRAY).contains(smsType);
        if (!partnerStatusFlag) {
            throw new Exception("短信类型错误！！");
        }

        // 手机号检验
        if (!Pattern.matches(SmsConstants.PHONE_REGEX_MOBILE, phone)) {
            throw new Exception("手机号验证失败！！");
        }
        return idysmsService.sendSms(phone, smsType);
    }


    /**
     * 短信查询API
     *
     * @param phone
     * @param smsBizId
     * @param sendDate
     * @param pages
     * @param rows
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "短信查询API", notes = "短信查询API")
    @RequestMapping(value = "/querySendDetails")
    public Object querySendDetails(@RequestParam(value = "phone", required = false) String phone,
                                   @RequestParam(value = "smsType", required = false) String smsType,
                                   @RequestParam(value = "smsBizId", required = false) String smsBizId,
                                   @RequestParam(value = "sendDate", required = false) String sendDate,
                                   @RequestParam(value = "pages", required = false, defaultValue = "1") Integer pages,
                                   @RequestParam(value = "rows", required = false, defaultValue = "10") Integer rows) throws Exception {

        if (phone.isEmpty() || sendDate.isEmpty() || "".equals(phone) || "".equals(sendDate)) {
            throw new Exception("参数不能为空");
        }

        // 验证时间是否在30天内
        if (!DateUtil.isValidDate(sendDate)) {
            throw new Exception("查询时间不能超过30天");
        }

        // 发送短信类型
        boolean partnerStatusFlag = Arrays.asList(SmsConstants.SMS_TYPE_ARRAY).contains(smsType);
        if (!partnerStatusFlag) {
            throw new Exception("短信类型错误！！");
        }

        // 手机号检验
        if (!Pattern.matches(SmsConstants.PHONE_REGEX_MOBILE, phone)) {
            throw new Exception("手机号验证失败！！");
        }

        return idysmsService.sendDetails(phone, smsBizId, sendDate, pages, rows);
    }


    /**
     * 短信批量发送API
     *
     * @param phone
     * @param smsType
     * @param signName
     * @param smsUpExtendCode
     * @param response
     * @param request
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "短信批量发送API", notes = "短信批量发送API")
    @RequestMapping(value = "/sendBatchSms")
    public boolean sendBatchSms(@RequestParam(value = "phone", required = false) String phone,
                                @RequestParam(value = "smsType", required = false) String smsType,
                                @RequestParam(value = "signName", required = false) String signName,
                                @RequestParam(value = "smsUpExtendCode", required = false) String smsUpExtendCode,
                                HttpServletResponse response,
                                HttpServletRequest request) throws Exception {

        if (phone.isEmpty() || smsType.isEmpty() || "".equals(smsType) || "".equals(phone)) {
            throw new Exception("信息不能为空！！");
        }

        // 发送短信类型
        boolean partnerStatusFlag = Arrays.asList(SmsConstants.SMS_TYPE_ARRAY).contains(smsType);
        if (!partnerStatusFlag) {
            throw new Exception("短信类型错误！！");
        }

        // 手机号 循环检验
        JSONArray phone_encode = JSON.parseArray(phone);
        if (phone_encode.size() <= 0 || phone_encode.size() > 100) {
            throw new Exception("手机号验证失败！！");
        }

        for (int i = 0; i < phone_encode.size(); i++) {
            if (!Pattern.matches(SmsConstants.PHONE_REGEX_MOBILE, phone_encode.get(i).toString())) {
                throw new Exception("手机号格式错误！！");
            }
        }

        return idysmsService.sendBatchSms(phone, signName, smsType, smsUpExtendCode);
    }


}
