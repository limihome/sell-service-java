package cn.sell.admin.sys.role;

import cn.sell.comm.model.qo.PageQO;
import cn.sell.comm.model.vo.PageVO;
import cn.sell.comm.result.annotation.ResponseResult;
import cn.sell.mbg.sys.relation.service.UmsRoleMenuRelationService;
import cn.sell.mbg.sys.role.UmsRole;
import cn.sell.mbg.sys.role.service.UmsRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Log4j2
@RestController
@ResponseResult
@RequestMapping("/rest/role")
@Api(tags = "RoleController - 用户角色")
@ApiResponses({
        @ApiResponse(code = 200, message = "请求正常"),
        @ApiResponse(code = 4001, message = "系统异常")
})
public class RoleController {

    @Autowired
    private UmsRoleService umsRoleService;

    @Autowired
    private UmsRoleMenuRelationService umsRoleMenuRelationService;

    @ApiOperation("查询角色信息，支持条件、分页查询，不支持模糊查询")
    @PostMapping("/list")
    public PageVO<UmsRole> list(@RequestBody UmsRole umsRole,
                                @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        PageQO pageQO = new PageQO(pageNum, pageSize);
        pageQO.setCondition(umsRole);
        return umsRoleService.selectPage(pageQO);
    }

    @ApiOperation("查询角色信息，支持条件、分页查询，不支持模糊查询")
    @GetMapping("/all")
    public List<UmsRole> all() {
        UmsRole umsRole = new UmsRole();
        umsRole.setStatus(1);
        return umsRoleService.selectByEntity(umsRole);
    }

    @ApiOperation("添加角色")
    @PostMapping("/create")
    public Long create(@RequestBody UmsRole umsRole) throws Exception {
        Assert.notNull(umsRole.getName(), "name is not null [ 角色名称不能为空 ]");
        Assert.notNull(umsRole.getDescription(), "description is not null [ 角色描述不能为空 ]");
        Assert.notNull(umsRole.getStatus(), "status is note null [ 角色状态不能为空 ]");
        return umsRoleService.insert(umsRole);
    }

    @ApiOperation("删除角色")
    @GetMapping("/delete/{id}")
    public int delete(@PathVariable("id") Long id) {
        Assert.notNull(id, "id is not null [ 角色id不能为空 ]");
        umsRoleMenuRelationService.deleteByRoleId(id);
        return umsRoleService.deleteByPk(id);
    }

    @ApiOperation("修改角色")
    @PostMapping("/update")
    public int update(@RequestBody UmsRole umsRole) {
        Assert.notNull(umsRole.getId(), "id is not null [ 角色id不能为空 ]");
        return umsRoleService.updateByPkSelective(umsRole.getId(), umsRole);
    }
}
