package cn.sell.open.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
/**
 * @ClassName:ApplicationProperty
 * @Description: TODO: 公共开关，key值 属性配置
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Date: 2020/3/25 17:17
 * @Version: 1.0
 **/
@Data
@Configuration
@ConfigurationProperties(prefix = "open.api.common.key")
public class ApplicationProperty {

    /**
     * 是否校验签名 0-不校验
     */
    private Boolean isCheckSign = true;

    /**
     * 验签公钥
     */
    private String publicKey;

}
