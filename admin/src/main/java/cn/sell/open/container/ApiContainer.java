package cn.sell.open.container;

import cn.sell.open.model.ApiModel;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * @ClassName:ApiContainer
 * @Description: TODO: Api 初始化容器
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Date: 2020/3/25 17:06
 * @Version: 1.0
 **/
@Service
public class ApiContainer extends HashMap<String, ApiModel> {
}
