package cn.sell.open.annotation;

import java.lang.annotation.*;
/**
 * @ClassName:OpenApi
 * @Description: TODO: 开放接口注解
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Date: 2020/3/25 17:02
 * @Version: 1.0
 **/
@Documented
@Inherited
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface OpenApi {

    /**
     * api 方法名
     *
     * @return
     */
    String method();

    /**
     * 方法描述
     */
    String desc() default "";
}
