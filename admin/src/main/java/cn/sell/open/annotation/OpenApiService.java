package cn.sell.open.annotation;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;
/**
 * @ClassName:OpenApiService
 * @Description: TODO: 开放接口实现类注解
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Date: 2020/3/25 17:04
 * @Version: 1.0
 **/
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface OpenApiService {
}

