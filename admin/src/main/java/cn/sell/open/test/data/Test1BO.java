package cn.sell.open.test.data;

/**
 * @ClassName:Test1BO
 * @Description: TODO:
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Date: 2020/3/25 18:01
 * @Version: 1.0
 **/

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

/**
 * 使用注解做参数校验
 */
public class Test1BO implements Serializable {
    private static final long serialVersionUID = -1L;

    @Valid
    @NotEmpty(message = "集合不为空！")
    @Size(min = 1, message = "最小为{min}")
    private List<Item> itemList;

    //省略 get/set

    /**
     * 内部类
     */
    public static class Item {
        @NotBlank(message = "username 不能为空！")
        private String username;

        @NotBlank(message = "password 不能为空！")
        private String password;

        @NotBlank(message = "realName 不能为空！")
        private String realName;

        //省略 get/set
    }
}
