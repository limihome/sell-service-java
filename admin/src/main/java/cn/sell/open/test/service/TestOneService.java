package cn.sell.open.test.service;

/**
 * @ClassName:TestOneService
 * @Description: TODO:
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Date: 2020/3/25 18:02
 * @Version: 1.0
 **/

import cn.sell.open.annotation.OpenApi;
import cn.sell.open.test.data.Test1BO;

/**
 * 测试开放接口1
 */
public interface TestOneService {

    /**
     * 方法1
     */
    @OpenApi(method = "open.api.test.one.method1", desc = "测试接口1,方法1")
    void testMethod1(String requestId, Test1BO test1BO);
}
