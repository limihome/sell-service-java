package cn.sell.open.test.service;

/**
 * @ClassName:TestOneServiceImpl
 * @Description: TODO:
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Date: 2020/3/25 18:02
 * @Version: 1.0
 **/

import cn.sell.open.test.data.Test1BO;
import com.alibaba.fastjson.JSON;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

/**
 * 测试开放接口1
 * <p>
 * 注解@OpenApiService > 开放接口自定义注解，用于启动时扫描接口
 */
@Log4j2
@Service
public class TestOneServiceImpl implements TestOneService {

    /**
     * 日志
     */

    /**
     * 方法1
     */
    @Override
    public void testMethod1(String requestId, Test1BO test1BO) {
        log.info("【{}】>> 测试开放接口1 >> 方法1 params={}", requestId, JSON.toJSONString(test1BO));
    }
}