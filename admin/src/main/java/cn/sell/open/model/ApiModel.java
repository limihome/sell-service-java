package cn.sell.open.model;


import lombok.Data;

import java.lang.reflect.Method;

/**
 * @ClassName:ApiModel
 * @Description: TODO: api接口对象
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Date: 2020/3/25 17:07
 * @Version: 1.0
 **/
@Data
public class ApiModel {

    /**
     * 类 spring bean
     */
    private String beanName;

    /**
     * 方法对象
     */
    private Method method;

    /**
     * 业务参数
     */
    private String paramName;

    public ApiModel(String beanName, Method method, String paramName) {
        this.beanName = beanName;
        this.method = method;
        this.paramName = paramName;
    }
    //省略 get/set
}