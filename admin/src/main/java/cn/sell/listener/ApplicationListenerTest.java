package cn.sell.listener;

import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/**
 * 项目启动监听器
 */
@Log4j2
@Component
public class ApplicationListenerTest implements ApplicationListener<ContextRefreshedEvent> {

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        log.info("======================= 项目启动时调用 ===========================");
        ApplicationContext applicationContext = contextRefreshedEvent.getApplicationContext();
        log.info("applicationContext.getDisplayName:{}",applicationContext.getDisplayName());
        log.info("applicationContext.getApplicationName:{}",applicationContext.getApplicationName());
        log.info("applicationContext.getAutowireCapableBeanFactory:{}",applicationContext.getAutowireCapableBeanFactory());
    }

}
