package cn.sell;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import tk.mybatis.spring.annotation.MapperScan;


@SpringBootApplication
@EnableTransactionManagement
@MapperScan(basePackages = "cn.sell.mbg.*.mapper.*")
public class Admin {
    public static void main(String[] args) {
        SpringApplication.run(Admin.class, args);
    }
}
