apidoc -i src/ -o apidoc/

{
  "name": "电商api接口文档",
  "version": "1.0.0",
  "description": "文档总描述，介绍apidoc插件的安装 https://blog.csdn.net/a794922102/article/details/90602327",
  "title": "apidoc浏览器自定义标题",
  "url" : "127.0.0.1:20100",
  "sampleUrl": "如果设置了该参数，那么在文档中便可以看到用于测试接口的一个表单(详情可以查看参数@apiSampleReques)",
  "sampleUrl": "https://apidoc.free.beeceptor.com",
  "order": "接口名称或接口组名称的排序列表如果未定义，那么所有名称会自动排序\"order\": [ \"Error\", \"Define\", \"PostTitleAndError\", PostError\"]",
  "order": "",
  "header": {
    "title": "Introduction",
    "filename": "header.md"
  },
  "footer": {
    "title": "Best practices",
    "filename": "footer.md"
  },
  "template": {
    "withCompare": true,
    "withGenerator": true,
    "aloneDisplay": false
  }
}
