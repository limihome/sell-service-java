define({
  "name": "",
  "version": "0.0.0",
  "description": "",
  "title": "Sell Api",
  "url": "127.0.0.1:20100",
  "sampleUrl": "https://apidoc.free.beeceptor.com",
  "order": "",
  "header": {
    "title": "项目介绍",
    "content": "<h1>Sell</h1>\n<p>单体商户类的电商系统\n包含：web、h5、android、ios、微信小程序、抖音小程序、支付宝小程序、后台管理系统</p>\n<h1>项目地址</h1>\n<ul>\n<li>移动端：<a href=\"www.baidu.com\">www.baidu.com</a></li>\n<li>web端：<a href=\"www.baidu.com\">www.baidu.com</a></li>\n<li>服务端：<a href=\"https://gitee.com/limihome/sell-service-java\">https://gitee.com/limihome/sell-service-java</a></li>\n<li>后台管理：<a href=\"https://gitee.com/limihome/sell-admin-web\">https://gitee.com/limihome/sell-admin-web</a></li>\n</ul>\n<h1>技术选型</h1>\n<table>\n<thead>\n<tr>\n<th style=\"text-align:center\">技术</th>\n<th style=\"text-align:center\">说明</th>\n<th style=\"text-align:center\">官网</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td style=\"text-align:center\">springboot</td>\n<td style=\"text-align:center\">基础框架</td>\n<td style=\"text-align:center\">https://spring.io/projects/spring-boot</td>\n</tr>\n<tr>\n<td style=\"text-align:center\">mybatis</td>\n<td style=\"text-align:center\">持久层框架</td>\n<td style=\"text-align:center\">http://www.mybatis.cn/</td>\n</tr>\n<tr>\n<td style=\"text-align:center\">tkmybatis</td>\n<td style=\"text-align:center\">实现傻瓜式开发</td>\n<td style=\"text-align:center\">http://www.mybatis.cn/</td>\n</tr>\n<tr>\n<td style=\"text-align:center\">apidoc</td>\n<td style=\"text-align:center\">接口文档</td>\n<td style=\"text-align:center\">https://apidocjs.com/</td>\n</tr>\n<tr>\n<td style=\"text-align:center\">xxl-job</td>\n<td style=\"text-align:center\">定时任务</td>\n<td style=\"text-align:center\">https://www.xuxueli.com/xxl-job/</td>\n</tr>\n<tr>\n<td style=\"text-align:center\">WxJava</td>\n<td style=\"text-align:center\">微信开发sdk</td>\n<td style=\"text-align:center\">https://gitee.com/binary/weixin-java-tools</td>\n</tr>\n</tbody>\n</table>\n<h1>项目结构</h1>\n<pre class=\"prettyprint lang-lua\">sell-service-java\n├── admin -- 后台系统服务\n\t├── admin -- 控制层\n\t├── config -- 配置类信息\n\t├── filter -- 过滤器\n\t├── interceptor -- 拦截器\n\t├── listener -- 监听器\n\t├── task -- 定时任务执行器\n\n├── app -- app服务\n├── comm\n\t├── result -- 统一返回报文格式封装\n\t├── base \n\t\tMySqlCrudServiceImpl -- 通用服务基类封装\n\t\t├── mapper -- tk.mybatis接口\n\t\t├── service -- 通用服务接口\n\n├── mbg -- 业务层\n\t├── mapper -- *dao mybatis\n\t├── mapper -- *mapper tk.mybatis\n\t├── service -- 业务层\n\n├── dataSource -- 数据库\n├── xxl-job-admin -- 定时任务调度器\n</pre>\n"
  },
  "footer": {
    "title": "Best practices",
    "filename": ""
  },
  "template": {
    "withCompare": true,
    "withGenerator": true,
    "aloneDisplay": false
  },
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2020-07-24T03:34:47.364Z",
    "url": "http://apidocjs.com",
    "version": "0.20.1"
  }
});
