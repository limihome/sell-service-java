define({ "api": [
  {
    "type": "POST",
    "url": "/rest/sku/add",
    "title": "add",
    "version": "1.0.0",
    "group": "Sku管理",
    "name": "add",
    "parameter": {
      "fields": {
        "请求体": [
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "productId",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "skuCode",
            "description": "<p>sku编码</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "price",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "stock",
            "description": "<p>库存</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "lowStock",
            "description": "<p>预警库存</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "pic",
            "description": "<p>展示图片</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "sale",
            "description": "<p>销量</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "promotionPrice",
            "description": "<p>单品促销价格</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "lockStock",
            "description": "<p>锁定库存</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "spData",
            "description": "<p>商品销售属性，json格式</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求体示例",
          "content": "{\"spData\":\"c\",\"lockStock\":3877,\"productId\":4844,\"promotionPrice\":6583.064807498218,\"lowStock\":5484,\"updateUser\":\"TngNgM\",\"updateTime\":\"7e2LdZS\",\"pic\":\"dK\",\"version\":\"JqJ\",\"sale\":5829,\"createTime\":\"N\",\"price\":5847.159205428188,\"createUser\":\"VFnwpIyy\",\"id\":8503,\"stock\":6848,\"skuCode\":\"EKpRPAQDlN\"}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "response",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "7236",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/sku/SkuStockController.java",
    "groupTitle": "Sku管理",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/sku/add"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/sku/list",
    "title": "list",
    "version": "1.0.0",
    "group": "Sku管理",
    "name": "list",
    "parameter": {
      "fields": {
        "请求参数": [
          {
            "group": "请求参数",
            "type": "Number",
            "optional": false,
            "field": "pageSize",
            "description": ""
          },
          {
            "group": "请求参数",
            "type": "Number",
            "optional": false,
            "field": "pageNum",
            "description": ""
          }
        ],
        "请求体": [
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "productId",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "skuCode",
            "description": "<p>sku编码</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "price",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "stock",
            "description": "<p>库存</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "lowStock",
            "description": "<p>预警库存</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "pic",
            "description": "<p>展示图片</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "sale",
            "description": "<p>销量</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "promotionPrice",
            "description": "<p>单品促销价格</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "lockStock",
            "description": "<p>锁定库存</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "spData",
            "description": "<p>商品销售属性，json格式</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求参数示例",
          "content": "pageSize=4267&pageNum=4715",
          "type": "json"
        },
        {
          "title": "请求体示例",
          "content": "{\"spData\":\"Q5OifwbN\",\"lockStock\":1495,\"productId\":4402,\"promotionPrice\":184.66904554693576,\"lowStock\":712,\"updateUser\":\"OdOH0Ll\",\"updateTime\":\"YH7P\",\"pic\":\"Kwz06bdxTE\",\"version\":\"0YEsBB\",\"sale\":420,\"createTime\":\"n3G\",\"price\":2876.8151161875553,\"createUser\":\"rNV2WC\",\"id\":271,\"stock\":3311,\"skuCode\":\"9d\"}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pageNum",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pageSize",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "total",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pages",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Array",
            "optional": false,
            "field": "list",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.id",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.productId",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.skuCode",
            "description": "<p>sku编码</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.price",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.stock",
            "description": "<p>库存</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.lowStock",
            "description": "<p>预警库存</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.pic",
            "description": "<p>展示图片</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.sale",
            "description": "<p>销量</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.promotionPrice",
            "description": "<p>单品促销价格</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.lockStock",
            "description": "<p>锁定库存</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.spData",
            "description": "<p>商品销售属性，json格式</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "{\"total\":8392,\"pages\":4060,\"pageSize\":7576,\"list\":[{\"spData\":\"fE6\",\"lockStock\":732,\"productId\":283,\"promotionPrice\":3395.129594055911,\"lowStock\":1423,\"updateUser\":\"hVceW\",\"updateTime\":\"tXyBZNsb\",\"pic\":\"CWNB\",\"version\":\"F2G\",\"sale\":360,\"createTime\":\"aLVCY\",\"price\":5070.752567943089,\"createUser\":\"u6rmvu\",\"id\":3525,\"stock\":3661,\"skuCode\":\"x\"}],\"pageNum\":4216}",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/sku/SkuStockController.java",
    "groupTitle": "Sku管理",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/sku/list"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/sku/update",
    "title": "update",
    "version": "1.0.0",
    "group": "Sku管理",
    "name": "update",
    "parameter": {
      "fields": {
        "请求体": [
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "productId",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "skuCode",
            "description": "<p>sku编码</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "price",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "stock",
            "description": "<p>库存</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "lowStock",
            "description": "<p>预警库存</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "pic",
            "description": "<p>展示图片</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "sale",
            "description": "<p>销量</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "promotionPrice",
            "description": "<p>单品促销价格</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "lockStock",
            "description": "<p>锁定库存</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "spData",
            "description": "<p>商品销售属性，json格式</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求体示例",
          "content": "{\"spData\":\"D3Wsy\",\"lockStock\":1797,\"productId\":6754,\"promotionPrice\":1848.3681415585017,\"lowStock\":9890,\"updateUser\":\"IY\",\"updateTime\":\"CqX\",\"pic\":\"1O0tvoP\",\"version\":\"6\",\"sale\":6356,\"createTime\":\"Lg769Vxa\",\"price\":7345.9597508933875,\"createUser\":\"QAwzk2\",\"id\":5721,\"stock\":1240,\"skuCode\":\"XVvDakho\"}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "response",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "2910",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/sku/SkuStockController.java",
    "groupTitle": "Sku管理",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/sku/update"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/admin/list",
    "title": "list",
    "version": "1.0.0",
    "group": "后台用户管理",
    "name": "list",
    "parameter": {
      "fields": {
        "请求参数": [
          {
            "group": "请求参数",
            "type": "Number",
            "optional": false,
            "field": "pageSize",
            "description": ""
          },
          {
            "group": "请求参数",
            "type": "Number",
            "optional": false,
            "field": "pageNum",
            "description": ""
          }
        ],
        "请求体": [
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "icon",
            "description": "<p>头像</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>邮箱</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "nickName",
            "description": "<p>昵称</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "note",
            "description": "<p>备注信息</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "loginTime",
            "description": "<p>最后登录时间</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>帐号启用状态：0-&gt;禁用；1-&gt;启用</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求参数示例",
          "content": "pageSize=3710&pageNum=6436",
          "type": "json"
        },
        {
          "title": "请求体示例",
          "content": "{\"note\":\"t\",\"nickName\":\"Lwxk\",\"icon\":\"A\",\"updateUser\":\"2xn2dU\",\"updateTime\":\"Hih\",\"version\":\"t4WjLV\",\"password\":\"wUP\",\"loginTime\":1409491560959,\"createTime\":\"lmyJqe\",\"createUser\":\"dMOhDg\",\"id\":6753,\"email\":\"FL\",\"username\":\"ygu\",\"status\":8871}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pageNum",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pageSize",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "total",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pages",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Array",
            "optional": false,
            "field": "list",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.id",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.username",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.password",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.icon",
            "description": "<p>头像</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.email",
            "description": "<p>邮箱</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.nickName",
            "description": "<p>昵称</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.note",
            "description": "<p>备注信息</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.loginTime",
            "description": "<p>最后登录时间</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.status",
            "description": "<p>帐号启用状态：0-&gt;禁用；1-&gt;启用</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "{\"total\":1585,\"pages\":8595,\"pageSize\":2973,\"list\":[{\"note\":\"1CQ0zZ27\",\"nickName\":\"F\",\"icon\":\"PTAkp0RcZA\",\"updateUser\":\"3BB5EKsTp\",\"updateTime\":\"io\",\"version\":\"vg\",\"password\":\"dQrNFQ\",\"loginTime\":803950336212,\"createTime\":\"83fh4tb\",\"createUser\":\"yi0m\",\"id\":487,\"email\":\"vvmaYklp\",\"username\":\"Nd0\",\"status\":2443}],\"pageNum\":452}",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/admin/AdminController.java",
    "groupTitle": "后台用户管理",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/admin/list"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/admin/add",
    "title": "saveOrUpdate",
    "version": "1.0.0",
    "group": "后台用户管理",
    "name": "saveOrUpdate",
    "parameter": {
      "fields": {
        "请求体": [
          {
            "group": "请求体",
            "type": "Number",
            "optional": true,
            "field": "id",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "icon",
            "description": "<p>头像</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>邮箱</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "nickName",
            "description": "<p>昵称</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "note",
            "description": "<p>备注信息</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "loginTime",
            "description": "<p>最后登录时间</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>帐号启用状态：0-&gt;禁用；1-&gt;启用</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求体示例",
          "content": "{\"note\":\"zqxkdB8Qiy\",\"nickName\":\"Om1nY6x\",\"icon\":\"LgiN\",\"updateUser\":\"gE9T2XTEUZ\",\"updateTime\":\"2zVcHhIb\",\"version\":\"hhfCt\",\"password\":\"1ll\",\"loginTime\":3093579037947,\"createTime\":\"YA0iGQkDh\",\"createUser\":\"H6jr5x75\",\"id\":6458,\"email\":\"hVIogHBW\",\"username\":\"IFM5\",\"status\":977}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "response",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "7093",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/admin/AdminController.java",
    "groupTitle": "后台用户管理",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/admin/add"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/category/add",
    "title": "新增",
    "version": "1.0.0",
    "group": "商品分类管理",
    "name": "add",
    "parameter": {
      "fields": {
        "请求体": [
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "parentId",
            "description": "<p>上级分类的编号：0表示一级分类</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>名称</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "level",
            "description": "<p>分类级别：0-&gt;1级；1-&gt;2级</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "productCount",
            "description": "<p>商品数量</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "productUnit",
            "description": "<p>商品单位</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "navStatus",
            "description": "<p>是否显示在导航栏：0-&gt;不显示；1-&gt;显示</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "showStatus",
            "description": "<p>显示状态：0-&gt;不显示；1-&gt;显示</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "icon",
            "description": "<p>图标</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "keywords",
            "description": "<p>关键字</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>描述</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求体示例",
          "content": "{\"keywords\":\"ZFTwq6\",\"level\":2198,\"icon\":\"4\",\"description\":\"MWDbg\",\"updateUser\":\"J\",\"showStatus\":381,\"updateTime\":\"u61axY7SZ\",\"productUnit\":\"S15\",\"sort\":178,\"productCount\":8140,\"version\":\"28NR\",\"parentId\":9813,\"createTime\":\"SAAK2KE\",\"name\":\"r4\",\"createUser\":\"TLRMZdh\",\"id\":8513,\"navStatus\":8586}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "response",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "8665",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/category/CategoryController.java",
    "groupTitle": "商品分类管理",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/category/add"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/category/delete/{id}",
    "title": "删除",
    "version": "1.0.0",
    "group": "商品分类管理",
    "name": "delete",
    "parameter": {
      "fields": {
        "请求参数": [
          {
            "group": "请求参数",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "请求参数示例",
          "content": "id=3218",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "response",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "2643",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/category/CategoryController.java",
    "groupTitle": "商品分类管理",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/category/delete/{id}"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/category/list",
    "title": "查询",
    "version": "1.0.0",
    "group": "商品分类管理",
    "name": "list",
    "parameter": {
      "fields": {
        "请求参数": [
          {
            "group": "请求参数",
            "type": "Number",
            "optional": false,
            "field": "pageSize",
            "description": ""
          },
          {
            "group": "请求参数",
            "type": "Number",
            "optional": false,
            "field": "pageNum",
            "description": ""
          }
        ],
        "请求体": [
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "parentId",
            "description": "<p>上级分类的编号：0表示一级分类</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>名称</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "level",
            "description": "<p>分类级别：0-&gt;1级；1-&gt;2级</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "productCount",
            "description": "<p>商品数量</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "productUnit",
            "description": "<p>商品单位</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "navStatus",
            "description": "<p>是否显示在导航栏：0-&gt;不显示；1-&gt;显示</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "showStatus",
            "description": "<p>显示状态：0-&gt;不显示；1-&gt;显示</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "icon",
            "description": "<p>图标</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "keywords",
            "description": "<p>关键字</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>描述</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求参数示例",
          "content": "pageSize=9154&pageNum=6249",
          "type": "json"
        },
        {
          "title": "请求体示例",
          "content": "{\"keywords\":\"j7u\",\"level\":1765,\"icon\":\"7SxSGLD\",\"description\":\"0K9AsQ\",\"updateUser\":\"49P\",\"showStatus\":8784,\"updateTime\":\"g4a3oj\",\"productUnit\":\"4ByT\",\"sort\":7959,\"productCount\":1756,\"version\":\"5\",\"parentId\":5077,\"createTime\":\"WMK4GZ9sf\",\"name\":\"SV\",\"createUser\":\"WRwDjhXNb9\",\"id\":9925,\"navStatus\":2873}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pageNum",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pageSize",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "total",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pages",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Array",
            "optional": false,
            "field": "list",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.id",
            "description": "<p>id</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.parentId",
            "description": "<p>上级分类的编号：0表示一级分类</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.name",
            "description": "<p>名称</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.level",
            "description": "<p>分类级别：0-&gt;1级；1-&gt;2级</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.productCount",
            "description": "<p>商品数量</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.productUnit",
            "description": "<p>商品单位</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.navStatus",
            "description": "<p>是否显示在导航栏：0-&gt;不显示；1-&gt;显示</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.showStatus",
            "description": "<p>显示状态：0-&gt;不显示；1-&gt;显示</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.icon",
            "description": "<p>图标</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.keywords",
            "description": "<p>关键字</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.description",
            "description": "<p>描述</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "{\"total\":5384,\"pages\":2963,\"pageSize\":7986,\"list\":[{\"keywords\":\"Z\",\"level\":9700,\"icon\":\"I\",\"description\":\"MKd\",\"updateUser\":\"dwAPUeSVX\",\"showStatus\":397,\"updateTime\":\"hqzOv\",\"productUnit\":\"s6A4Kspu\",\"sort\":9785,\"productCount\":1674,\"version\":\"xJ3Ox\",\"parentId\":4839,\"createTime\":\"EVHqKzIi6\",\"name\":\"8\",\"createUser\":\"XEQf5DD\",\"id\":9191,\"navStatus\":8502}],\"pageNum\":2072}",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/category/CategoryController.java",
    "groupTitle": "商品分类管理",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/category/list"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/category/update",
    "title": "修改",
    "version": "1.0.0",
    "group": "商品分类管理",
    "name": "update",
    "parameter": {
      "fields": {
        "请求体": [
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "parentId",
            "description": "<p>上级分类的编号：0表示一级分类</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>名称</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "level",
            "description": "<p>分类级别：0-&gt;1级；1-&gt;2级</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "productCount",
            "description": "<p>商品数量</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "productUnit",
            "description": "<p>商品单位</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "navStatus",
            "description": "<p>是否显示在导航栏：0-&gt;不显示；1-&gt;显示</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "showStatus",
            "description": "<p>显示状态：0-&gt;不显示；1-&gt;显示</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "icon",
            "description": "<p>图标</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "keywords",
            "description": "<p>关键字</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>描述</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求体示例",
          "content": "{\"keywords\":\"FDrRoQ1sMx\",\"level\":7441,\"icon\":\"Ak0guc\",\"description\":\"rhKYY0\",\"updateUser\":\"n0Oufe6l\",\"showStatus\":467,\"updateTime\":\"7V6YrdHB\",\"productUnit\":\"1y\",\"sort\":1146,\"productCount\":7250,\"version\":\"p7jkrUHjHN\",\"parentId\":9929,\"createTime\":\"CNuBZxr\",\"name\":\"N0IXd\",\"createUser\":\"A\",\"id\":2761,\"navStatus\":9403}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "response",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "1689",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/category/CategoryController.java",
    "groupTitle": "商品分类管理",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/category/update"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/attribute/params/add",
    "title": "新增",
    "version": "1.0.0",
    "group": "商品参数信息",
    "name": "add",
    "parameter": {
      "fields": {
        "请求体": [
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "productId",
            "description": "<p>商品id</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "productAttributeId",
            "description": "<p>商品属性id</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "value",
            "description": "<p>手动添加规格或参数的值，参数单值，规格有多个时以逗号隔开</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求体示例",
          "content": "{\"productId\":5929,\"createTime\":\"cf9t\",\"updateUser\":\"9T4U\",\"createUser\":\"G\",\"updateTime\":\"r\",\"id\":2163,\"productAttributeId\":453,\"value\":\"1e46MAuTy\",\"version\":\"Rp\"}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "response",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "2127",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/attribute/AttributeParamsController.java",
    "groupTitle": "商品参数信息",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/attribute/params/add"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/attribute/params/delete/{id}",
    "title": "delete",
    "version": "1.0.0",
    "group": "商品参数信息",
    "name": "delete",
    "parameter": {
      "fields": {
        "请求参数": [
          {
            "group": "请求参数",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "请求参数示例",
          "content": "id=9946",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "response",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "2562",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/attribute/AttributeParamsController.java",
    "groupTitle": "商品参数信息",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/attribute/params/delete/{id}"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/attribute/params/list",
    "title": "查询",
    "version": "1.0.0",
    "group": "商品参数信息",
    "name": "list",
    "description": "<p>查询属性分类value，支持条件、分页查询，不支持模糊查询</p>",
    "parameter": {
      "fields": {
        "请求参数": [
          {
            "group": "请求参数",
            "type": "Number",
            "optional": false,
            "field": "pageSize",
            "description": ""
          },
          {
            "group": "请求参数",
            "type": "Number",
            "optional": false,
            "field": "pageNum",
            "description": ""
          }
        ],
        "请求体": [
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "productId",
            "description": "<p>商品id</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "productAttributeId",
            "description": "<p>商品属性id</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "value",
            "description": "<p>手动添加规格或参数的值，参数单值，规格有多个时以逗号隔开</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求参数示例",
          "content": "pageSize=3223&pageNum=2852",
          "type": "json"
        },
        {
          "title": "请求体示例",
          "content": "{\"productId\":3507,\"createTime\":\"9Sf\",\"updateUser\":\"6ADWR5\",\"createUser\":\"pZ\",\"updateTime\":\"l0B\",\"id\":6177,\"productAttributeId\":7849,\"value\":\"n06NVo\",\"version\":\"p\"}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pageNum",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pageSize",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "total",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pages",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Array",
            "optional": false,
            "field": "list",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.id",
            "description": "<p>id</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.productId",
            "description": "<p>商品id</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.productAttributeId",
            "description": "<p>商品属性id</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.value",
            "description": "<p>手动添加规格或参数的值，参数单值，规格有多个时以逗号隔开</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "{\"total\":9859,\"pages\":1478,\"pageSize\":3697,\"list\":[{\"productId\":6069,\"createTime\":\"KfTFxuZfI\",\"updateUser\":\"zxLa1mEC\",\"createUser\":\"iTh\",\"updateTime\":\"LSDRT\",\"id\":2220,\"productAttributeId\":7051,\"value\":\"dsRYtM5rJo\",\"version\":\"BL558r\"}],\"pageNum\":5869}",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/attribute/AttributeParamsController.java",
    "groupTitle": "商品参数信息",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/attribute/params/list"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/attribute/params/list/tree",
    "title": "查询tree",
    "version": "1.0.0",
    "group": "商品参数信息",
    "name": "listToTree",
    "description": "<p>查询属性分类value，支持条件、分页查询，不支持模糊查询。返回tree结构数据</p>",
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Array",
            "optional": false,
            "field": "response",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "response.id",
            "description": "<p>id</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "response.productId",
            "description": "<p>商品id</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "response.productAttributeId",
            "description": "<p>商品属性id</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "response.value",
            "description": "<p>手动添加规格或参数的值，参数单值，规格有多个时以逗号隔开</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "response.version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "response.createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "response.createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "response.updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "response.updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "[{\"productId\":9141,\"createTime\":\"wXRUnSLkY5\",\"updateUser\":\"C0fjw\",\"createUser\":\"Zso40tHGg\",\"updateTime\":\"uag\",\"id\":6651,\"productAttributeId\":8998,\"value\":\"N9n0a5U\",\"version\":\"YPhBffb\"}]",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/attribute/AttributeParamsController.java",
    "groupTitle": "商品参数信息",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/attribute/params/list/tree"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/attribute/params/update",
    "title": "修改",
    "version": "1.0.0",
    "group": "商品参数信息",
    "name": "update",
    "parameter": {
      "fields": {
        "请求体": [
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "productId",
            "description": "<p>商品id</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "productAttributeId",
            "description": "<p>商品属性id</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "value",
            "description": "<p>手动添加规格或参数的值，参数单值，规格有多个时以逗号隔开</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求体示例",
          "content": "{\"productId\":1698,\"createTime\":\"dYoY67K\",\"updateUser\":\"0\",\"createUser\":\"OuPZ\",\"updateTime\":\"Mm\",\"id\":3896,\"productAttributeId\":6692,\"value\":\"97v7VH7\",\"version\":\"IaMpX8J4i\"}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "response",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "3758",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/attribute/AttributeParamsController.java",
    "groupTitle": "商品参数信息",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/attribute/params/update"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/attribute/add",
    "title": "add",
    "version": "1.0.0",
    "group": "商品属性管理",
    "name": "add",
    "parameter": {
      "fields": {
        "请求体": [
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "productAttributeCategoryId",
            "description": "<p>商品属性分类id</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>名称</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "selectType",
            "description": "<p>属性选择类型：0-&gt;唯一；1-&gt;单选；2-&gt;多选；对应属性和参数意义不同；</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "inputType",
            "description": "<p>属性录入方式：0-&gt;手工录入；1-&gt;从列表中选取</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "inputList",
            "description": "<p>可选值列表，以逗号隔开</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "sort",
            "description": "<p>排序字段：最高的可以单独上传图片</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "filterType",
            "description": "<p>分类筛选样式：1-&gt;普通；1-&gt;颜色</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "searchType",
            "description": "<p>检索类型；0-&gt;不需要进行检索；1-&gt;关键字检索；2-&gt;范围检索</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "relatedStatus",
            "description": "<p>相同属性产品是否关联；0-&gt;不关联；1-&gt;关联</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "handAddStatus",
            "description": "<p>是否支持手动新增；0-&gt;不支持；1-&gt;支持</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "type",
            "description": "<p>属性的类型；0-&gt;规格；1-&gt;参数</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "请求体示例",
          "content": "{\"relatedStatus\":8147,\"productAttributeCategoryId\":418,\"searchType\":3474,\"updateUser\":\"TFF\",\"updateTime\":\"AnYxZT\",\"sort\":2662,\"type\":812,\"version\":\"fmQ\",\"inputList\":\"hft\",\"handAddStatus\":6082,\"createTime\":\"rPE\",\"name\":\"5jVNo\",\"selectType\":8173,\"inputType\":6297,\"createUser\":\"ae785AsUQX\",\"id\":4202,\"filterType\":7633}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "response",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "3198",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/attribute/AttributeSpecController.java",
    "groupTitle": "商品属性管理",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/attribute/add"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/attribute/delete/{id}",
    "title": "delete",
    "version": "1.0.0",
    "group": "商品属性管理",
    "name": "delete",
    "description": "<p>商品属性管理：删除</p>",
    "parameter": {
      "fields": {
        "请求参数": [
          {
            "group": "请求参数",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "请求参数示例",
          "content": "id=9101",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "response",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "9779",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/attribute/AttributeSpecController.java",
    "groupTitle": "商品属性管理",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/attribute/delete/{id}"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/attribute/list",
    "title": "查询属性数据",
    "version": "1.0.0",
    "group": "商品属性管理",
    "name": "list",
    "parameter": {
      "fields": {
        "请求参数": [
          {
            "group": "请求参数",
            "type": "Number",
            "optional": false,
            "field": "pageSize",
            "description": ""
          },
          {
            "group": "请求参数",
            "type": "Number",
            "optional": false,
            "field": "pageNum",
            "description": "<p>;</p>"
          }
        ],
        "请求体": [
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "productAttributeCategoryId",
            "description": "<p>商品属性分类id</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>名称</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "selectType",
            "description": "<p>属性选择类型：0-&gt;唯一；1-&gt;单选；2-&gt;多选；对应属性和参数意义不同；</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "inputType",
            "description": "<p>属性录入方式：0-&gt;手工录入；1-&gt;从列表中选取</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "inputList",
            "description": "<p>可选值列表，以逗号隔开；</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "sort",
            "description": "<p>排序字段：最高的可以单独上传图片;</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "filterType",
            "description": "<p>分类筛选样式：1-&gt;普通；1-&gt;颜色</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "searchType",
            "description": "<p>检索类型；0-&gt;不需要进行检索；1-&gt;关键字检索；2-&gt;范围检索</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "relatedStatus",
            "description": "<p>相同属性产品是否关联；0-&gt;不关联；1-&gt;关联</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "handAddStatus",
            "description": "<p>是否支持手动新增；0-&gt;不支持；1-&gt;支持</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "type",
            "description": "<p>属性的类型；0-&gt;规格；1-&gt;参数</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "请求参数示例",
          "content": "pageSize=4990&pageNum=4296",
          "type": "json"
        },
        {
          "title": "请求体示例",
          "content": "{\"relatedStatus\":7162,\"productAttributeCategoryId\":4974,\"searchType\":9852,\"updateUser\":\"rwx4zd\",\"updateTime\":\"sto\",\"sort\":197,\"type\":7162,\"version\":\"e9\",\"inputList\":\"XfRzDv7\",\"handAddStatus\":950,\"createTime\":\"QilxRjIXl\",\"name\":\"MZ0ApIVIC2\",\"selectType\":5193,\"inputType\":1110,\"createUser\":\"kbkqoJV\",\"id\":3208,\"filterType\":9683}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pageNum",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pageSize",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "total",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pages",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Array",
            "optional": false,
            "field": "list",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.id",
            "description": "<p>id</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.productAttributeCategoryId",
            "description": "<p>商品属性分类id</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.name",
            "description": "<p>名称</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.selectType",
            "description": "<p>属性选择类型：0-&gt;唯一；1-&gt;单选；2-&gt;多选；对应属性和参数意义不同；</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.inputType",
            "description": "<p>属性录入方式：0-&gt;手工录入；1-&gt;从列表中选取</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.inputList",
            "description": "<p>可选值列表，以逗号隔开</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.sort",
            "description": "<p>排序字段：最高的可以单独上传图片</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.filterType",
            "description": "<p>分类筛选样式：1-&gt;普通；1-&gt;颜色</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.searchType",
            "description": "<p>检索类型；0-&gt;不需要进行检索；1-&gt;关键字检索；2-&gt;范围检索</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.relatedStatus",
            "description": "<p>相同属性产品是否关联；0-&gt;不关联；1-&gt;关联</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.handAddStatus",
            "description": "<p>是否支持手动新增；0-&gt;不支持；1-&gt;支持</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.type",
            "description": "<p>属性的类型；0-&gt;规格；1-&gt;参数</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.version",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.createUser",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.createTime",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.updateUser",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.updateTime",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "{\"total\":9023,\"pages\":186,\"pageSize\":4614,\"list\":[{\"relatedStatus\":3344,\"productAttributeCategoryId\":1606,\"searchType\":9692,\"updateUser\":\"t\",\"updateTime\":\"msvaYAVFc\",\"sort\":2109,\"type\":1814,\"version\":\"8IB4L\",\"inputList\":\"byYQEv\",\"handAddStatus\":3567,\"createTime\":\"0ghk\",\"name\":\"6CrYV8\",\"selectType\":7811,\"inputType\":7647,\"createUser\":\"U7y\",\"id\":8198,\"filterType\":4836}],\"pageNum\":7334}",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/attribute/AttributeSpecController.java",
    "groupTitle": "商品属性管理",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/attribute/list"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/product/add",
    "title": "新增",
    "version": "1.0.0",
    "group": "商品管理",
    "name": "add",
    "parameter": {
      "fields": {
        "请求体": [
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "brandId",
            "description": "<p>品牌id</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "productCategoryId",
            "description": "<p>品牌分类id</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "feightTemplateId",
            "description": "<p>运费模版id</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "productAttributeCategoryId",
            "description": "<p>品牌属性分类id</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>商品名称</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "pic",
            "description": "<p>图片</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "productSn",
            "description": "<p>货号</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "publishStatus",
            "description": "<p>上架状态：0-&gt;下架；1-&gt;上架</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "newStatus",
            "description": "<p>新品状态:0-&gt;不是新品；1-&gt;新品</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "recommandStatus",
            "description": "<p>推荐状态；0-&gt;不推荐；1-&gt;推荐</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "verifyStatus",
            "description": "<p>审核状态：0-&gt;未审核；1-&gt;审核通过</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "sale",
            "description": "<p>销量</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "price",
            "description": "<p>价格</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "promotionPrice",
            "description": "<p>促销价格</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "giftGrowth",
            "description": "<p>赠送的成长值</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "giftPoint",
            "description": "<p>赠送的积分</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "usePointLimit",
            "description": "<p>限制使用的积分数</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "subTitle",
            "description": "<p>副标题</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>商品描述</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "originalPrice",
            "description": "<p>市场价</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "stock",
            "description": "<p>库存</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "lowStock",
            "description": "<p>库存预警值</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "unit",
            "description": "<p>单位</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "weight",
            "description": "<p>商品重量，默认为克</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "previewStatus",
            "description": "<p>是否为预告商品：0-&gt;不是；1-&gt;是</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "serviceIds",
            "description": "<p>以逗号分割的产品服务：1-&gt;无忧退货；2-&gt;快速退款；3-&gt;免费包邮</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "keywords",
            "description": "<p>关键字</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "note",
            "description": "<p>备注</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "albumPics",
            "description": "<p>画册图片，连产品图片限制为5张，以逗号分割</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "detailTitle",
            "description": "<p>详情标题</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "detailDesc",
            "description": "<p>详情描述</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "detailHtml",
            "description": "<p>产品详情网页内容</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "detailMobileHtml",
            "description": "<p>移动端网页详情</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "promotionStartTime",
            "description": "<p>促销开始时间</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "promotionEndTime",
            "description": "<p>促销结束时间</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "promotionPerLimit",
            "description": "<p>活动限购数量</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "promotionType",
            "description": "<p>促销类型：0-&gt;没有促销使用原价;1-&gt;使用促销价；2-&gt;使用会员价；3-&gt;使用阶梯价格；4-&gt;使用满减价格；5-&gt;限时购</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "productCategoryName",
            "description": "<p>产品分类名称</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "brandName",
            "description": "<p>品牌名称</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "deleteStatus",
            "description": "<p>删除状态：0-&gt;未删除；1-&gt;已删除</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求体示例",
          "content": "{\"note\":\"gtef\",\"newStatus\":6155,\"originalPrice\":4746.462483576931,\"keywords\":\"VX\",\"promotionPrice\":5468.874948972637,\"productCategoryName\":\"KGifULe7\",\"feightTemplateId\":9164,\"description\":\"WV7hm1\",\"lowStock\":5047,\"pic\":\"JGEkkE\",\"productCategoryId\":2965,\"recommandStatus\":8216,\"verifyStatus\":9212,\"subTitle\":\"PnunU1R\",\"price\":4930.573080492335,\"usePointLimit\":41,\"deleteStatus\":8221,\"id\":7847,\"giftGrowth\":7520,\"stock\":9236,\"publishStatus\":191,\"promotionType\":1064,\"brandName\":\"NccGchxq\",\"productSn\":\"WSBcrKs\",\"albumPics\":\"rEegAAhSo\",\"productAttributeCategoryId\":4825,\"promotionStartTime\":3470809214767,\"weight\":6271.012884715087,\"detailTitle\":\"ffZ85yzteg\",\"updateUser\":\"XfMjJ4T9\",\"detailHtml\":\"NQcbyl43JM\",\"updateTime\":\"S2\",\"sort\":5619,\"giftPoint\":5173,\"version\":\"MMx7rcUk7\",\"promotionPerLimit\":7276,\"detailDesc\":\"wCj\",\"sale\":5139,\"unit\":\"Gi\",\"serviceIds\":\"yFT\",\"detailMobileHtml\":\"WC253\",\"createTime\":\"FLtQYa\",\"brandId\":7002,\"name\":\"9eT8G7fy\",\"previewStatus\":4494,\"createUser\":\"FpKx7xM6\",\"promotionEndTime\":2641102692229}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "response",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "8241",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/product/ProductController.java",
    "groupTitle": "商品管理",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/product/add"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/product/update",
    "title": "修改",
    "version": "1.0.0",
    "group": "商品管理",
    "name": "update",
    "parameter": {
      "fields": {
        "请求体": [
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "brandId",
            "description": "<p>品牌id</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "productCategoryId",
            "description": "<p>品牌分类id</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "feightTemplateId",
            "description": "<p>运费模版id</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "productAttributeCategoryId",
            "description": "<p>品牌属性分类id</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>商品名称</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "pic",
            "description": "<p>图片</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "productSn",
            "description": "<p>货号</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "publishStatus",
            "description": "<p>上架状态：0-&gt;下架；1-&gt;上架</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "newStatus",
            "description": "<p>新品状态:0-&gt;不是新品；1-&gt;新品</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "recommandStatus",
            "description": "<p>推荐状态；0-&gt;不推荐；1-&gt;推荐</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "verifyStatus",
            "description": "<p>审核状态：0-&gt;未审核；1-&gt;审核通过</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "sale",
            "description": "<p>销量</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "price",
            "description": "<p>价格</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "promotionPrice",
            "description": "<p>促销价格</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "giftGrowth",
            "description": "<p>赠送的成长值</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "giftPoint",
            "description": "<p>赠送的积分</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "usePointLimit",
            "description": "<p>限制使用的积分数</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "subTitle",
            "description": "<p>副标题</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>商品描述</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "originalPrice",
            "description": "<p>市场价</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "stock",
            "description": "<p>库存</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "lowStock",
            "description": "<p>库存预警值</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "unit",
            "description": "<p>单位</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "weight",
            "description": "<p>商品重量，默认为克</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "previewStatus",
            "description": "<p>是否为预告商品：0-&gt;不是；1-&gt;是</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "serviceIds",
            "description": "<p>以逗号分割的产品服务：1-&gt;无忧退货；2-&gt;快速退款；3-&gt;免费包邮</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "keywords",
            "description": "<p>关键字</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "note",
            "description": "<p>备注</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "albumPics",
            "description": "<p>画册图片，连产品图片限制为5张，以逗号分割</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "detailTitle",
            "description": "<p>详情标题</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "detailDesc",
            "description": "<p>详情描述</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "detailHtml",
            "description": "<p>产品详情网页内容</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "detailMobileHtml",
            "description": "<p>移动端网页详情</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "promotionStartTime",
            "description": "<p>促销开始时间</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "promotionEndTime",
            "description": "<p>促销结束时间</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "promotionPerLimit",
            "description": "<p>活动限购数量</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "promotionType",
            "description": "<p>促销类型：0-&gt;没有促销使用原价;1-&gt;使用促销价；2-&gt;使用会员价；3-&gt;使用阶梯价格；4-&gt;使用满减价格；5-&gt;限时购</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "productCategoryName",
            "description": "<p>产品分类名称</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "brandName",
            "description": "<p>品牌名称</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "deleteStatus",
            "description": "<p>删除状态：0-&gt;未删除；1-&gt;已删除</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求体示例",
          "content": "{\"note\":\"o9xCiu0cI\",\"newStatus\":8452,\"originalPrice\":5046.2155012817075,\"keywords\":\"HWig\",\"promotionPrice\":6289.582114495689,\"productCategoryName\":\"EDt3\",\"feightTemplateId\":7069,\"description\":\"5Rh4SP4\",\"lowStock\":9436,\"pic\":\"F\",\"productCategoryId\":7658,\"recommandStatus\":4682,\"verifyStatus\":6681,\"subTitle\":\"h\",\"price\":9067.205936306367,\"usePointLimit\":6010,\"deleteStatus\":7020,\"id\":7180,\"giftGrowth\":5416,\"stock\":6827,\"publishStatus\":8781,\"promotionType\":5301,\"brandName\":\"0e\",\"productSn\":\"mlrOxAqz\",\"albumPics\":\"AnAhtcKy6k\",\"productAttributeCategoryId\":6621,\"promotionStartTime\":1819942751205,\"weight\":788.6988147132756,\"detailTitle\":\"IXBLcK3K\",\"updateUser\":\"wbZ\",\"detailHtml\":\"vTbH\",\"updateTime\":\"kCMmGwTOY\",\"sort\":9525,\"giftPoint\":7369,\"version\":\"9ufMz9QC\",\"promotionPerLimit\":8793,\"detailDesc\":\"iu\",\"sale\":219,\"unit\":\"NLIp\",\"serviceIds\":\"senKv\",\"detailMobileHtml\":\"7Cyh\",\"createTime\":\"TujD6cqX\",\"brandId\":1897,\"name\":\"np6\",\"previewStatus\":3193,\"createUser\":\"PjZVytRh\",\"promotionEndTime\":3001992393368}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "response",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "6617",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/product/ProductController.java",
    "groupTitle": "商品管理",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/product/update"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/product/list",
    "title": "list",
    "version": "1.0.0",
    "group": "商品管理",
    "name": "查询",
    "parameter": {
      "fields": {
        "请求参数": [
          {
            "group": "请求参数",
            "type": "Number",
            "optional": false,
            "field": "pageSize",
            "description": ""
          },
          {
            "group": "请求参数",
            "type": "Number",
            "optional": false,
            "field": "pageNum",
            "description": ""
          }
        ],
        "请求体": [
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "brandId",
            "description": "<p>品牌id</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "productCategoryId",
            "description": "<p>品牌分类id</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "feightTemplateId",
            "description": "<p>运费模版id</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "productAttributeCategoryId",
            "description": "<p>品牌属性分类id</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>商品名称</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "pic",
            "description": "<p>图片</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "productSn",
            "description": "<p>货号</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "publishStatus",
            "description": "<p>上架状态：0-&gt;下架；1-&gt;上架</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "newStatus",
            "description": "<p>新品状态:0-&gt;不是新品；1-&gt;新品</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "recommandStatus",
            "description": "<p>推荐状态；0-&gt;不推荐；1-&gt;推荐</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "verifyStatus",
            "description": "<p>审核状态：0-&gt;未审核；1-&gt;审核通过</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "sale",
            "description": "<p>销量</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "price",
            "description": "<p>价格</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "promotionPrice",
            "description": "<p>促销价格</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "giftGrowth",
            "description": "<p>赠送的成长值</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "giftPoint",
            "description": "<p>赠送的积分</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "usePointLimit",
            "description": "<p>限制使用的积分数</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "subTitle",
            "description": "<p>副标题</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>商品描述</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "originalPrice",
            "description": "<p>市场价</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "stock",
            "description": "<p>库存</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "lowStock",
            "description": "<p>库存预警值</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "unit",
            "description": "<p>单位</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "weight",
            "description": "<p>商品重量，默认为克</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "previewStatus",
            "description": "<p>是否为预告商品：0-&gt;不是；1-&gt;是</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "serviceIds",
            "description": "<p>以逗号分割的产品服务：1-&gt;无忧退货；2-&gt;快速退款；3-&gt;免费包邮</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "keywords",
            "description": "<p>关键字</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "note",
            "description": "<p>备注</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "albumPics",
            "description": "<p>画册图片，连产品图片限制为5张，以逗号分割</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "detailTitle",
            "description": "<p>详情标题</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "detailDesc",
            "description": "<p>详情描述</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "detailHtml",
            "description": "<p>产品详情网页内容</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "detailMobileHtml",
            "description": "<p>移动端网页详情</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "promotionStartTime",
            "description": "<p>促销开始时间</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "promotionEndTime",
            "description": "<p>促销结束时间</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "promotionPerLimit",
            "description": "<p>活动限购数量</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "promotionType",
            "description": "<p>促销类型：0-&gt;没有促销使用原价;1-&gt;使用促销价；2-&gt;使用会员价；3-&gt;使用阶梯价格；4-&gt;使用满减价格；5-&gt;限时购</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "productCategoryName",
            "description": "<p>产品分类名称</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "brandName",
            "description": "<p>品牌名称</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "deleteStatus",
            "description": "<p>删除状态：0-&gt;未删除；1-&gt;已删除</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求参数示例",
          "content": "pageSize=9294&pageNum=718",
          "type": "json"
        },
        {
          "title": "请求体示例",
          "content": "{\"note\":\"q2xG1\",\"newStatus\":8766,\"originalPrice\":4833.404713585017,\"keywords\":\"p3Ssa\",\"promotionPrice\":2753.7586842531705,\"productCategoryName\":\"OocBT0j\",\"feightTemplateId\":5019,\"description\":\"o2dc2\",\"lowStock\":8652,\"pic\":\"BqloQipbM6\",\"productCategoryId\":1982,\"recommandStatus\":1269,\"verifyStatus\":9323,\"subTitle\":\"e9pAP\",\"price\":1510.6463901445532,\"usePointLimit\":1582,\"deleteStatus\":3733,\"id\":6390,\"giftGrowth\":9855,\"stock\":7210,\"publishStatus\":2420,\"promotionType\":3841,\"brandName\":\"BNLmim\",\"productSn\":\"FBVW\",\"albumPics\":\"DQlu\",\"productAttributeCategoryId\":2168,\"promotionStartTime\":1679274141000,\"weight\":5001.000711894837,\"detailTitle\":\"yWgurFG\",\"updateUser\":\"pjh\",\"detailHtml\":\"O47RwHGfl\",\"updateTime\":\"fPHOwcdtc\",\"sort\":7398,\"giftPoint\":2075,\"version\":\"K0D\",\"promotionPerLimit\":9502,\"detailDesc\":\"Ky\",\"sale\":6968,\"unit\":\"rqK2\",\"serviceIds\":\"r3Mcn\",\"detailMobileHtml\":\"IH\",\"createTime\":\"QiVFsiDCb\",\"brandId\":1774,\"name\":\"aUr6zE8pag\",\"previewStatus\":8388,\"createUser\":\"TpiRG2Ww\",\"promotionEndTime\":226164435400}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pageNum",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pageSize",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "total",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pages",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Array",
            "optional": false,
            "field": "list",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.id",
            "description": "<p>id</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.brandId",
            "description": "<p>品牌id</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.productCategoryId",
            "description": "<p>品牌分类id</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.feightTemplateId",
            "description": "<p>运费模版id</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.productAttributeCategoryId",
            "description": "<p>品牌属性分类id</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.name",
            "description": "<p>商品名称</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.pic",
            "description": "<p>图片</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.productSn",
            "description": "<p>货号</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.publishStatus",
            "description": "<p>上架状态：0-&gt;下架；1-&gt;上架</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.newStatus",
            "description": "<p>新品状态:0-&gt;不是新品；1-&gt;新品</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.recommandStatus",
            "description": "<p>推荐状态；0-&gt;不推荐；1-&gt;推荐</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.verifyStatus",
            "description": "<p>审核状态：0-&gt;未审核；1-&gt;审核通过</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.sale",
            "description": "<p>销量</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.price",
            "description": "<p>价格</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.promotionPrice",
            "description": "<p>促销价格</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.giftGrowth",
            "description": "<p>赠送的成长值</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.giftPoint",
            "description": "<p>赠送的积分</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.usePointLimit",
            "description": "<p>限制使用的积分数</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.subTitle",
            "description": "<p>副标题</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.description",
            "description": "<p>商品描述</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.originalPrice",
            "description": "<p>市场价</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.stock",
            "description": "<p>库存</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.lowStock",
            "description": "<p>库存预警值</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.unit",
            "description": "<p>单位</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.weight",
            "description": "<p>商品重量，默认为克</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.previewStatus",
            "description": "<p>是否为预告商品：0-&gt;不是；1-&gt;是</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.serviceIds",
            "description": "<p>以逗号分割的产品服务：1-&gt;无忧退货；2-&gt;快速退款；3-&gt;免费包邮</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.keywords",
            "description": "<p>关键字</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.note",
            "description": "<p>备注</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.albumPics",
            "description": "<p>画册图片，连产品图片限制为5张，以逗号分割</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.detailTitle",
            "description": "<p>详情标题</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.detailDesc",
            "description": "<p>详情描述</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.detailHtml",
            "description": "<p>产品详情网页内容</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.detailMobileHtml",
            "description": "<p>移动端网页详情</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.promotionStartTime",
            "description": "<p>促销开始时间</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.promotionEndTime",
            "description": "<p>促销结束时间</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.promotionPerLimit",
            "description": "<p>活动限购数量</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.promotionType",
            "description": "<p>促销类型：0-&gt;没有促销使用原价;1-&gt;使用促销价；2-&gt;使用会员价；3-&gt;使用阶梯价格；4-&gt;使用满减价格；5-&gt;限时购</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.productCategoryName",
            "description": "<p>产品分类名称</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.brandName",
            "description": "<p>品牌名称</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.deleteStatus",
            "description": "<p>删除状态：0-&gt;未删除；1-&gt;已删除</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "{\"total\":6675,\"pages\":2562,\"pageSize\":57,\"list\":[{\"note\":\"8StDvbPQuS\",\"newStatus\":2691,\"originalPrice\":1125.314375336548,\"keywords\":\"VI62Hq\",\"promotionPrice\":8208.9435249092,\"productCategoryName\":\"OOp\",\"feightTemplateId\":7979,\"description\":\"pAW420qzj\",\"lowStock\":9693,\"pic\":\"vm\",\"productCategoryId\":8076,\"recommandStatus\":5276,\"verifyStatus\":2720,\"subTitle\":\"xnhfEKZx\",\"price\":5871.142428200869,\"usePointLimit\":798,\"deleteStatus\":9402,\"id\":3361,\"giftGrowth\":1419,\"stock\":8683,\"publishStatus\":3423,\"promotionType\":7142,\"brandName\":\"yT\",\"productSn\":\"L4PdAI\",\"albumPics\":\"pgMLlofY\",\"productAttributeCategoryId\":548,\"promotionStartTime\":713433669960,\"weight\":4750.01873044824,\"detailTitle\":\"BgEiP\",\"updateUser\":\"Lu8Qa\",\"detailHtml\":\"5a\",\"updateTime\":\"ZNLOlBf\",\"sort\":2313,\"giftPoint\":3120,\"version\":\"IjF4IaWr0\",\"promotionPerLimit\":9145,\"detailDesc\":\"61Uv2sR\",\"sale\":2922,\"unit\":\"S\",\"serviceIds\":\"ih0\",\"detailMobileHtml\":\"K\",\"createTime\":\"UP\",\"brandId\":2338,\"name\":\"1AFJQ4\",\"previewStatus\":7924,\"createUser\":\"ZLp\",\"promotionEndTime\":620145863815}],\"pageNum\":1417}",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/product/ProductController.java",
    "groupTitle": "商品管理",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/product/list"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/address/add",
    "title": "add",
    "version": "1.0.0",
    "group": "地址信息",
    "name": "add",
    "description": "<p>地址信息</p>",
    "parameter": {
      "fields": {
        "请求体": [
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "userId",
            "description": "<p>会员编号</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>收货人名称</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "phoneNumber",
            "description": "<p>收货人手机号</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "defaultStatus",
            "description": "<p>是否为默认</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "postCode",
            "description": "<p>邮政编码</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "province",
            "description": "<p>省份/直辖市</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>城市</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "region",
            "description": "<p>区</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "detailAddress",
            "description": "<p>详细地址(街道)</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求体示例",
          "content": "{\"city\":\"iiuQKt2s5\",\"updateUser\":\"gCj438\",\"updateTime\":\"Go443\",\"userId\":2938,\"version\":\"923s7r\",\"defaultStatus\":5610,\"phoneNumber\":\"nssFW\",\"province\":\"FU37\",\"createTime\":\"8Adp5Zswu\",\"name\":\"9N\",\"detailAddress\":\"C8xicwoHS\",\"postCode\":\"AX\",\"createUser\":\"YTHkvu\",\"id\":7728,\"region\":\"xq25LwT\"}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "response",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "8729",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/address/AddressController.java",
    "groupTitle": "地址信息",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/address/add"
      }
    ]
  },
  {
    "type": "GET",
    "url": "/rest/address/delete/{id}",
    "title": "delete",
    "version": "1.0.0",
    "group": "地址信息",
    "name": "delete",
    "description": "<p>地址信息</p>",
    "parameter": {
      "fields": {
        "请求参数": [
          {
            "group": "请求参数",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "请求参数示例",
          "content": "id=6065",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "response",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "2764",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/address/AddressController.java",
    "groupTitle": "地址信息",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/address/delete/{id}"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/address/list",
    "title": "list",
    "version": "1.0.0",
    "group": "地址信息",
    "name": "list",
    "description": "<p>地址信息</p>",
    "parameter": {
      "fields": {
        "请求参数": [
          {
            "group": "请求参数",
            "type": "Number",
            "optional": false,
            "field": "pageSize",
            "description": ""
          },
          {
            "group": "请求参数",
            "type": "Number",
            "optional": false,
            "field": "pageNum",
            "description": ""
          }
        ],
        "请求体": [
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "userId",
            "description": "<p>会员编号</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>收货人名称</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "phoneNumber",
            "description": "<p>收货人手机号</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "defaultStatus",
            "description": "<p>是否为默认</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "postCode",
            "description": "<p>邮政编码</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "province",
            "description": "<p>省份/直辖市</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>城市</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "region",
            "description": "<p>区</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "detailAddress",
            "description": "<p>详细地址(街道)</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求参数示例",
          "content": "pageSize=4043&pageNum=5473",
          "type": "json"
        },
        {
          "title": "请求体示例",
          "content": "{\"city\":\"8uz\",\"updateUser\":\"dGX0OFA\",\"updateTime\":\"DPAxv7Rlg\",\"userId\":6122,\"version\":\"2q2WYCmWRf\",\"defaultStatus\":9794,\"phoneNumber\":\"twO\",\"province\":\"KO\",\"createTime\":\"HQ\",\"name\":\"aunKO\",\"detailAddress\":\"sw2AMf7Yb\",\"postCode\":\"HJ1hbgc\",\"createUser\":\"zakjwtNb\",\"id\":6305,\"region\":\"e539I\"}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pageNum",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pageSize",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "total",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pages",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Array",
            "optional": false,
            "field": "list",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.id",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.userId",
            "description": "<p>会员编号</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.name",
            "description": "<p>收货人名称</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.phoneNumber",
            "description": "<p>收货人手机号</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.defaultStatus",
            "description": "<p>是否为默认</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.postCode",
            "description": "<p>邮政编码</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.province",
            "description": "<p>省份/直辖市</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.city",
            "description": "<p>城市</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.region",
            "description": "<p>区</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.detailAddress",
            "description": "<p>详细地址(街道)</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "{\"total\":4742,\"pages\":6217,\"pageSize\":7607,\"list\":[{\"city\":\"aBIVY\",\"updateUser\":\"F5jl24L1\",\"updateTime\":\"C8tuNq\",\"userId\":5166,\"version\":\"HS8vI\",\"defaultStatus\":8151,\"phoneNumber\":\"dCU\",\"province\":\"MK\",\"createTime\":\"9uJYMqgH3v\",\"name\":\"20hGCFkL\",\"detailAddress\":\"uE8g\",\"postCode\":\"iKCy\",\"createUser\":\"fkRdQhtpg5\",\"id\":5248,\"region\":\"V1u\"}],\"pageNum\":2514}",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/address/AddressController.java",
    "groupTitle": "地址信息",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/address/list"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/address/update",
    "title": "update",
    "version": "1.0.0",
    "group": "地址信息",
    "name": "update",
    "description": "<p>地址信息</p>",
    "parameter": {
      "fields": {
        "请求体": [
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "userId",
            "description": "<p>会员编号</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>收货人名称</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "phoneNumber",
            "description": "<p>收货人手机号</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "defaultStatus",
            "description": "<p>是否为默认</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "postCode",
            "description": "<p>邮政编码</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "province",
            "description": "<p>省份/直辖市</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>城市</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "region",
            "description": "<p>区</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "detailAddress",
            "description": "<p>详细地址(街道)</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求体示例",
          "content": "{\"city\":\"tzMOjj\",\"updateUser\":\"0wpC\",\"updateTime\":\"GXQMyrlky\",\"userId\":1907,\"version\":\"oCQ5m\",\"defaultStatus\":6183,\"phoneNumber\":\"rlMsDEhQOZ\",\"province\":\"EBQ4hAqJF2\",\"createTime\":\"yZNy412V1\",\"name\":\"uxXVF8kJM\",\"detailAddress\":\"roQLpjami\",\"postCode\":\"bXfrVMz\",\"createUser\":\"2Q\",\"id\":3974,\"region\":\"0SveQ\"}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "response",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "4897",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/address/AddressController.java",
    "groupTitle": "地址信息",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/address/update"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/dict/add",
    "title": "新增",
    "version": "1.0.0",
    "group": "字典管理",
    "name": "index",
    "description": "<p>这只是一个测试的接口描述</p>",
    "parameter": {
      "fields": {
        "请求体": [
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "dictCode",
            "description": "<p>字典类型</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "dictName",
            "description": "<p>字典名称</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "dictValue",
            "description": "<p>字典值</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "dictDesc",
            "description": "<p>字典描述</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "isDelete",
            "description": "<p>是否删除</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求体示例",
          "content": "{\"dictValue\":\"T1oaq\",\"dictDesc\":\"gFRbOs51J\",\"dictCode\":\"bMpMRL8R\",\"createTime\":\"TpIFEPtcSS\",\"isDelete\":\"DELETE\",\"updateUser\":\"PKm\",\"dictName\":\"BXuV\",\"createUser\":\"Ny\",\"updateTime\":\"nnFgpcPRqA\",\"id\":9502,\"version\":\"NlvH47VlU\"}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "response",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "965",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/dict/DictController.java",
    "groupTitle": "字典管理",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/dict/add"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/dict/list",
    "title": "查询",
    "version": "1.0.0",
    "group": "字典管理",
    "name": "list",
    "parameter": {
      "fields": {
        "请求参数": [
          {
            "group": "请求参数",
            "type": "Number",
            "optional": false,
            "field": "pageSize",
            "description": ""
          },
          {
            "group": "请求参数",
            "type": "Number",
            "optional": false,
            "field": "pageNum",
            "description": ""
          }
        ],
        "请求体": [
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "dictCode",
            "description": "<p>字典类型</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "dictName",
            "description": "<p>字典名称</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "dictValue",
            "description": "<p>字典值</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "dictDesc",
            "description": "<p>字典描述</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "isDelete",
            "description": "<p>是否删除</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求参数示例",
          "content": "pageSize=9187&pageNum=3863",
          "type": "json"
        },
        {
          "title": "请求体示例",
          "content": "{\"dictValue\":\"afSLZoKgYu\",\"dictDesc\":\"GMEL\",\"dictCode\":\"G81ZkcH5\",\"createTime\":\"zerGe4c1sd\",\"isDelete\":\"DELETE\",\"updateUser\":\"d35KSMl\",\"dictName\":\"NgZntayW\",\"createUser\":\"vfxWki9\",\"updateTime\":\"og2V86IJ\",\"id\":3265,\"version\":\"YD\"}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pageNum",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pageSize",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "total",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pages",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Array",
            "optional": false,
            "field": "list",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.id",
            "description": "<p>id</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.dictCode",
            "description": "<p>字典类型</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.dictName",
            "description": "<p>字典名称</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.dictValue",
            "description": "<p>字典值</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.dictDesc",
            "description": "<p>字典描述</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.isDelete",
            "description": "<p>是否删除</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "{\"total\":1264,\"pages\":4922,\"pageSize\":9765,\"list\":[{\"dictValue\":\"IxY0Vu\",\"dictDesc\":\"s\",\"dictCode\":\"2d8o\",\"createTime\":\"M\",\"isDelete\":\"DELETE\",\"updateUser\":\"USnvVafR6\",\"dictName\":\"f\",\"createUser\":\"tEJ1d\",\"updateTime\":\"hzEI5MMI5\",\"id\":839,\"version\":\"sDGnW\"}],\"pageNum\":1815}",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/dict/DictController.java",
    "groupTitle": "字典管理",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/dict/list"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/dict/update",
    "title": "修改",
    "version": "1.0.0",
    "group": "字典管理",
    "name": "update",
    "parameter": {
      "fields": {
        "请求体": [
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "dictCode",
            "description": "<p>字典类型</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "dictName",
            "description": "<p>字典名称</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "dictValue",
            "description": "<p>字典值</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "dictDesc",
            "description": "<p>字典描述</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "isDelete",
            "description": "<p>是否删除</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求体示例",
          "content": "{\"dictValue\":\"CA\",\"dictDesc\":\"iRDQ\",\"dictCode\":\"DTfTtF\",\"createTime\":\"kvhHq\",\"isDelete\":\"DELETE\",\"updateUser\":\"E\",\"dictName\":\"yNPpZSG\",\"createUser\":\"mHBxzVNa\",\"updateTime\":\"FQEE7svVh\",\"id\":4303,\"version\":\"I\"}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "response",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "1131",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/dict/DictController.java",
    "groupTitle": "字典管理",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/dict/update"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/attribute/category/add",
    "title": "add",
    "version": "1.0.0",
    "group": "属性分类",
    "name": "add",
    "description": "<p>属性分类：新增</p>",
    "parameter": {
      "fields": {
        "请求体": [
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>名称</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "attributeCount",
            "description": "<p>属性数量</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "paramCount",
            "description": "<p>参数数量</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求体示例",
          "content": "{\"paramCount\":9046,\"createTime\":\"aJfM\",\"name\":\"4WYgk\",\"attributeCount\":8739,\"updateUser\":\"ZkJg\",\"createUser\":\"rB7I1cmUW\",\"updateTime\":\"A6G0\",\"id\":484,\"version\":\"Yn5L6ic\"}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "response",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "6176",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/attribute/AttributeTypeController.java",
    "groupTitle": "属性分类",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/attribute/category/add"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/attribute/category/delete/{id}",
    "title": "delete",
    "version": "1.0.0",
    "group": "属性分类",
    "name": "delete",
    "description": "<p>属性分类：删除</p>",
    "parameter": {
      "fields": {
        "请求参数": [
          {
            "group": "请求参数",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "请求参数示例",
          "content": "id=5110",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "response",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "3407",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/attribute/AttributeTypeController.java",
    "groupTitle": "属性分类",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/attribute/category/delete/{id}"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/attribute/category/list",
    "title": "list",
    "version": "1.0.0",
    "group": "属性分类",
    "name": "list",
    "description": "<p>查询</p>",
    "parameter": {
      "fields": {
        "请求参数": [
          {
            "group": "请求参数",
            "type": "Number",
            "optional": false,
            "field": "pageSize",
            "description": ""
          },
          {
            "group": "请求参数",
            "type": "Number",
            "optional": false,
            "field": "pageNum",
            "description": ""
          }
        ],
        "请求体": [
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "attributeCount",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "paramCount",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "请求参数示例",
          "content": "pageSize=7966&pageNum=6769",
          "type": "json"
        },
        {
          "title": "请求体示例",
          "content": "{\"paramCount\":3133,\"createTime\":\"uX4kz\",\"name\":\"EIzTT4epF\",\"attributeCount\":7291,\"updateUser\":\"14ntA8\",\"createUser\":\"DOkD0oZnq9\",\"updateTime\":\"HxoQAf\",\"id\":8614,\"version\":\"X\"}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pageNum",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pageSize",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "total",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pages",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Array",
            "optional": false,
            "field": "list",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.id",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.name",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.attributeCount",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.paramCount",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.version",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.createUser",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.createTime",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.updateUser",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.updateTime",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "{\"total\":6983,\"pages\":9135,\"pageSize\":8402,\"list\":[{\"paramCount\":8874,\"createTime\":\"H1\",\"name\":\"ekw9vzWXpM\",\"attributeCount\":7762,\"updateUser\":\"bfF\",\"createUser\":\"Gj8J\",\"updateTime\":\"yZXHt\",\"id\":6167,\"version\":\"dMo8rPh\"}],\"pageNum\":5591}",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/attribute/AttributeTypeController.java",
    "groupTitle": "属性分类",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/attribute/category/list"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/attribute/category/list/tree",
    "title": "listToTree",
    "version": "1.0.0",
    "group": "属性分类",
    "name": "listToTree",
    "description": "<p>查询返回tree结构</p>",
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Array",
            "optional": false,
            "field": "response",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "response.id",
            "description": "<p>id</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "response.name",
            "description": "<p>名称</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "response.attributeCount",
            "description": "<p>属性数量</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "response.paramCount",
            "description": "<p>参数数量</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "response.version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "response.createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "response.createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "response.updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "response.updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "[{\"paramCount\":9491,\"createTime\":\"ymF\",\"name\":\"U6JV\",\"attributeCount\":6523,\"updateUser\":\"rdTLy3e5\",\"createUser\":\"BJ1AjV9\",\"updateTime\":\"AbQcjO\",\"id\":6825,\"version\":\"Pcl81\"}]",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/attribute/AttributeTypeController.java",
    "groupTitle": "属性分类",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/attribute/category/list/tree"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/attribute/category/update",
    "title": "update",
    "version": "1.0.0",
    "group": "属性分类",
    "name": "update",
    "description": "<p>属性分类：修改</p>",
    "parameter": {
      "fields": {
        "请求体": [
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>名称</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "attributeCount",
            "description": "<p>属性数量</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "paramCount",
            "description": "<p>参数数量</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求体示例",
          "content": "{\"paramCount\":178,\"createTime\":\"6\",\"name\":\"5pgfTw\",\"attributeCount\":9334,\"updateUser\":\"r3V\",\"createUser\":\"64\",\"updateTime\":\"SPXUggxQZ7\",\"id\":1487,\"version\":\"81jLkIc5vG\"}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "response",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "4326",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/attribute/AttributeTypeController.java",
    "groupTitle": "属性分类",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/attribute/category/update"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/file/uploads",
    "title": "上传文件",
    "version": "1.0.0",
    "group": "文件上传",
    "name": "uploadFile",
    "description": "<p>可批量上传文件</p>",
    "parameter": {
      "fields": {
        "请求参数": [
          {
            "group": "请求参数",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "请求参数示例",
          "content": "type=qMISoeg",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Object",
            "optional": false,
            "field": "response",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "null",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/file/FileController.java",
    "groupTitle": "文件上传",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/file/uploads"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/user/register",
    "title": "注册",
    "version": "1.0.0",
    "group": "用户管理",
    "name": "add",
    "parameter": {
      "fields": {
        "请求体": [
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "memberLevelId",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>用户名</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>密码</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "nickname",
            "description": "<p>昵称</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>手机号码</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>帐号启用状态:0-&gt;启用；1-&gt;禁用</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": "<p>注册时间</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "icon",
            "description": "<p>头像</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "gender",
            "description": "<p>性别：0-&gt;未知；1-&gt;男；2-&gt;女</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "birthday",
            "description": "<p>生日</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>所做城市</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "job",
            "description": "<p>职业</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "personalizedSignature",
            "description": "<p>个性签名</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "sourceType",
            "description": "<p>用户来源</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "integration",
            "description": "<p>积分</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "growth",
            "description": "<p>成长值</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "luckeyCount",
            "description": "<p>剩余抽奖次数</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "historyIntegration",
            "description": "<p>历史积分数量</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "onLineStatus",
            "description": "<p>websockt在线状态，0：离线，1：在线</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "receiverDetailAddress",
            "description": "<p>收货地址</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求体示例",
          "content": "{\"birthday\":1704459909425,\"memberLevelId\":7675,\"gender\":6471,\"city\":\"CJLgd1\",\"icon\":\"vsZ9y\",\"password\":\"1NQI\",\"historyIntegration\":5502,\"nickname\":\"qXEQw\",\"id\":7421,\"updateUser\":\"W2RerEwH\",\"updateTime\":\"owWO\",\"receiverDetailAddress\":\"v\",\"version\":\"LDcmD1bO\",\"onLineStatus\":2767,\"phone\":\"XQ1cXXW\",\"createTime\":\"NqN\",\"sourceType\":250,\"integration\":6795,\"growth\":7516,\"createUser\":\"L1bf\",\"job\":\"OfZH\",\"personalizedSignature\":\"JVm\",\"luckeyCount\":9705,\"username\":\"EKO\",\"status\":641}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "memberLevelId",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>用户名</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>密码</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "nickname",
            "description": "<p>昵称</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>手机号码</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>帐号启用状态:0-&gt;启用；1-&gt;禁用</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": "<p>注册时间</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "icon",
            "description": "<p>头像</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "gender",
            "description": "<p>性别：0-&gt;未知；1-&gt;男；2-&gt;女</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "birthday",
            "description": "<p>生日</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>所做城市</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "job",
            "description": "<p>职业</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "personalizedSignature",
            "description": "<p>个性签名</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "sourceType",
            "description": "<p>用户来源</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "integration",
            "description": "<p>积分</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "growth",
            "description": "<p>成长值</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "luckeyCount",
            "description": "<p>剩余抽奖次数</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "historyIntegration",
            "description": "<p>历史积分数量</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "onLineStatus",
            "description": "<p>websockt在线状态，0：离线，1：在线</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "receiverDetailAddress",
            "description": "<p>收货地址</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "{\"birthday\":1701035750914,\"memberLevelId\":1747,\"gender\":8433,\"city\":\"CEH\",\"icon\":\"1\",\"password\":\"xv\",\"historyIntegration\":1276,\"nickname\":\"4\",\"id\":8061,\"updateUser\":\"v9x\",\"updateTime\":\"LU\",\"receiverDetailAddress\":\"G9ihFmYqA\",\"version\":\"iv8yHLgr\",\"onLineStatus\":563,\"phone\":\"327\",\"createTime\":\"dg4kk7cl4Z\",\"sourceType\":6668,\"integration\":5090,\"growth\":3152,\"createUser\":\"Xi8b95Jh\",\"job\":\"cUa\",\"personalizedSignature\":\"lkVcRb\",\"luckeyCount\":6554,\"username\":\"DNVYyFy8\",\"status\":2440}",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/user/UserController.java",
    "groupTitle": "用户管理",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/user/register"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/user/list",
    "title": "查询",
    "version": "1.0.0",
    "group": "用户管理",
    "name": "list",
    "parameter": {
      "fields": {
        "请求参数": [
          {
            "group": "请求参数",
            "type": "Number",
            "optional": false,
            "field": "pageSize",
            "description": ""
          },
          {
            "group": "请求参数",
            "type": "Number",
            "optional": false,
            "field": "pageNum",
            "description": ""
          }
        ],
        "请求体": [
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "memberLevelId",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>用户名</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>密码</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "nickname",
            "description": "<p>昵称</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>手机号码</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>帐号启用状态:0-&gt;启用；1-&gt;禁用</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": "<p>注册时间</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "icon",
            "description": "<p>头像</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "gender",
            "description": "<p>性别：0-&gt;未知；1-&gt;男；2-&gt;女</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "birthday",
            "description": "<p>生日</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>所做城市</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "job",
            "description": "<p>职业</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "personalizedSignature",
            "description": "<p>个性签名</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "sourceType",
            "description": "<p>用户来源</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "integration",
            "description": "<p>积分</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "growth",
            "description": "<p>成长值</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "luckeyCount",
            "description": "<p>剩余抽奖次数</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "historyIntegration",
            "description": "<p>历史积分数量</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "onLineStatus",
            "description": "<p>websockt在线状态，0：离线，1：在线</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "receiverDetailAddress",
            "description": "<p>收货地址</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求参数示例",
          "content": "pageSize=6947&pageNum=9838",
          "type": "json"
        },
        {
          "title": "请求体示例",
          "content": "{\"birthday\":2721753555148,\"memberLevelId\":5885,\"gender\":7075,\"city\":\"le\",\"icon\":\"NckdL00\",\"password\":\"9Z2tI4xHyB\",\"historyIntegration\":4812,\"nickname\":\"g4Yt\",\"id\":5072,\"updateUser\":\"Jc5SY6d\",\"updateTime\":\"agsN\",\"receiverDetailAddress\":\"TcgHw6q\",\"version\":\"pmRJwe\",\"onLineStatus\":3061,\"phone\":\"U4ZNmL\",\"createTime\":\"Nv6NWs\",\"sourceType\":5717,\"integration\":5821,\"growth\":1487,\"createUser\":\"p\",\"job\":\"96NhrR0\",\"personalizedSignature\":\"L6KZlScUo\",\"luckeyCount\":7204,\"username\":\"hAET7lAqQ\",\"status\":9686}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pageNum",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pageSize",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "total",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pages",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Array",
            "optional": false,
            "field": "list",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.id",
            "description": "<p>id</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.memberLevelId",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.username",
            "description": "<p>用户名</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.password",
            "description": "<p>密码</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.nickname",
            "description": "<p>昵称</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.phone",
            "description": "<p>手机号码</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.status",
            "description": "<p>帐号启用状态:0-&gt;启用；1-&gt;禁用</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.createTime",
            "description": "<p>注册时间</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.icon",
            "description": "<p>头像</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.gender",
            "description": "<p>性别：0-&gt;未知；1-&gt;男；2-&gt;女</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.birthday",
            "description": "<p>生日</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.city",
            "description": "<p>所做城市</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.job",
            "description": "<p>职业</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.personalizedSignature",
            "description": "<p>个性签名</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.sourceType",
            "description": "<p>用户来源</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.integration",
            "description": "<p>积分</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.growth",
            "description": "<p>成长值</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.luckeyCount",
            "description": "<p>剩余抽奖次数</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.historyIntegration",
            "description": "<p>历史积分数量</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.onLineStatus",
            "description": "<p>websockt在线状态，0：离线，1：在线</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.receiverDetailAddress",
            "description": "<p>收货地址</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "{\"total\":1968,\"pages\":9631,\"pageSize\":3208,\"list\":[{\"birthday\":3728077325108,\"memberLevelId\":6302,\"gender\":6664,\"city\":\"Ct4CDZ\",\"icon\":\"uLMe\",\"password\":\"OpxBaeIA1\",\"historyIntegration\":8134,\"nickname\":\"hed\",\"id\":4874,\"updateUser\":\"cQ2V\",\"updateTime\":\"9Y\",\"receiverDetailAddress\":\"LKYLL25H9R\",\"version\":\"PQh0cpnE\",\"onLineStatus\":9680,\"phone\":\"JZ5f\",\"createTime\":\"1SRI3Yehj\",\"sourceType\":1783,\"integration\":2692,\"growth\":4131,\"createUser\":\"GlmUI\",\"job\":\"jeg\",\"personalizedSignature\":\"0mp5\",\"luckeyCount\":7878,\"username\":\"kGK8R93uz\",\"status\":4554}],\"pageNum\":1323}",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/user/UserController.java",
    "groupTitle": "用户管理",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/user/list"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/user/login",
    "title": "登录",
    "version": "1.0.0",
    "group": "用户管理",
    "name": "login",
    "parameter": {
      "fields": {
        "请求体": [
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "memberLevelId",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>用户名</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>密码</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "nickname",
            "description": "<p>昵称</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>手机号码</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>帐号启用状态:0-&gt;启用；1-&gt;禁用</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": "<p>注册时间</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "icon",
            "description": "<p>头像</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "gender",
            "description": "<p>性别：0-&gt;未知；1-&gt;男；2-&gt;女</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "birthday",
            "description": "<p>生日</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>所做城市</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "job",
            "description": "<p>职业</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "personalizedSignature",
            "description": "<p>个性签名</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "sourceType",
            "description": "<p>用户来源</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "integration",
            "description": "<p>积分</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "growth",
            "description": "<p>成长值</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "luckeyCount",
            "description": "<p>剩余抽奖次数</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "historyIntegration",
            "description": "<p>历史积分数量</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "onLineStatus",
            "description": "<p>websockt在线状态，0：离线，1：在线</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "receiverDetailAddress",
            "description": "<p>收货地址</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求体示例",
          "content": "{\"birthday\":2362086056640,\"memberLevelId\":617,\"gender\":1182,\"city\":\"G\",\"icon\":\"Vnw\",\"password\":\"8sdfij\",\"historyIntegration\":9312,\"nickname\":\"mVfeY\",\"id\":4912,\"updateUser\":\"Gokt\",\"updateTime\":\"9kyEP\",\"receiverDetailAddress\":\"VEQ7t6M8t\",\"version\":\"Av\",\"onLineStatus\":8192,\"phone\":\"b5T2vsV\",\"createTime\":\"fl9cLr7U\",\"sourceType\":8365,\"integration\":8596,\"growth\":2921,\"createUser\":\"hSM\",\"job\":\"cl3smfwo\",\"personalizedSignature\":\"V\",\"luckeyCount\":3049,\"username\":\"pN0sNDxM\",\"status\":6619}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "memberLevelId",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>用户名</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>密码</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "nickname",
            "description": "<p>昵称</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>手机号码</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>帐号启用状态:0-&gt;启用；1-&gt;禁用</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": "<p>注册时间</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "icon",
            "description": "<p>头像</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "gender",
            "description": "<p>性别：0-&gt;未知；1-&gt;男；2-&gt;女</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "birthday",
            "description": "<p>生日</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>所做城市</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "job",
            "description": "<p>职业</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "personalizedSignature",
            "description": "<p>个性签名</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "sourceType",
            "description": "<p>用户来源</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "integration",
            "description": "<p>积分</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "growth",
            "description": "<p>成长值</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "luckeyCount",
            "description": "<p>剩余抽奖次数</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "historyIntegration",
            "description": "<p>历史积分数量</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "onLineStatus",
            "description": "<p>websockt在线状态，0：离线，1：在线</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "receiverDetailAddress",
            "description": "<p>收货地址</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "{\"birthday\":186096533,\"memberLevelId\":4299,\"gender\":5265,\"city\":\"QrR8\",\"icon\":\"zV5\",\"password\":\"GQtR6L1bg\",\"historyIntegration\":7944,\"nickname\":\"ed3\",\"id\":5777,\"updateUser\":\"t4qNR\",\"updateTime\":\"c\",\"receiverDetailAddress\":\"I\",\"version\":\"U\",\"onLineStatus\":6490,\"phone\":\"RxkFE9\",\"createTime\":\"UpPZJddP\",\"sourceType\":7420,\"integration\":1499,\"growth\":7194,\"createUser\":\"jOgL1AQ5Wy\",\"job\":\"QiLaI\",\"personalizedSignature\":\"Y3amz\",\"luckeyCount\":2277,\"username\":\"WZ\",\"status\":1031}",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/user/UserController.java",
    "groupTitle": "用户管理",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/user/login"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/user/update",
    "title": "修改",
    "version": "1.0.0",
    "group": "用户管理",
    "name": "update",
    "parameter": {
      "fields": {
        "请求体": [
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "memberLevelId",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>用户名</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>密码</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "nickname",
            "description": "<p>昵称</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>手机号码</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>帐号启用状态:0-&gt;启用；1-&gt;禁用</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": "<p>注册时间</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "icon",
            "description": "<p>头像</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "gender",
            "description": "<p>性别：0-&gt;未知；1-&gt;男；2-&gt;女</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "birthday",
            "description": "<p>生日</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>所做城市</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "job",
            "description": "<p>职业</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "personalizedSignature",
            "description": "<p>个性签名</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "sourceType",
            "description": "<p>用户来源</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "integration",
            "description": "<p>积分</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "growth",
            "description": "<p>成长值</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "luckeyCount",
            "description": "<p>剩余抽奖次数</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "historyIntegration",
            "description": "<p>历史积分数量</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "onLineStatus",
            "description": "<p>websockt在线状态，0：离线，1：在线</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "receiverDetailAddress",
            "description": "<p>收货地址</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求体示例",
          "content": "{\"birthday\":3081300571385,\"memberLevelId\":4186,\"gender\":5062,\"city\":\"dg\",\"icon\":\"ur7IVSjRA\",\"password\":\"fdIJk9F\",\"historyIntegration\":7383,\"nickname\":\"qAy3duRpE\",\"id\":4065,\"updateUser\":\"ndSM\",\"updateTime\":\"ywIsL5\",\"receiverDetailAddress\":\"JWJ19H\",\"version\":\"55Y9K9TZE\",\"onLineStatus\":978,\"phone\":\"fV1\",\"createTime\":\"K5EwlPgJ\",\"sourceType\":1161,\"integration\":3652,\"growth\":4507,\"createUser\":\"iF1v\",\"job\":\"OD\",\"personalizedSignature\":\"2\",\"luckeyCount\":4681,\"username\":\"OY\",\"status\":8075}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "response",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "8987",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/user/UserController.java",
    "groupTitle": "用户管理",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/user/update"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/user/updateByPhone",
    "title": "根据手机号修改",
    "version": "1.0.0",
    "group": "用户管理",
    "name": "updateByPhone",
    "description": "<p>根据手机号修改</p>",
    "parameter": {
      "fields": {
        "请求体": [
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "memberLevelId",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>用户名</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>密码</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "nickname",
            "description": "<p>昵称</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>手机号码</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>帐号启用状态:0-&gt;启用；1-&gt;禁用</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": "<p>注册时间</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "icon",
            "description": "<p>头像</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "gender",
            "description": "<p>性别：0-&gt;未知；1-&gt;男；2-&gt;女</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "birthday",
            "description": "<p>生日</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>所做城市</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "job",
            "description": "<p>职业</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "personalizedSignature",
            "description": "<p>个性签名</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "sourceType",
            "description": "<p>用户来源</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "integration",
            "description": "<p>积分</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "growth",
            "description": "<p>成长值</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "luckeyCount",
            "description": "<p>剩余抽奖次数</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "historyIntegration",
            "description": "<p>历史积分数量</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "onLineStatus",
            "description": "<p>websockt在线状态，0：离线，1：在线</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "receiverDetailAddress",
            "description": "<p>收货地址</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求体示例",
          "content": "{\"birthday\":518429789457,\"memberLevelId\":6132,\"gender\":5726,\"city\":\"qAu\",\"icon\":\"lhc0pQ7\",\"password\":\"LejguVHJ6\",\"historyIntegration\":5408,\"nickname\":\"REVj1P\",\"id\":7541,\"updateUser\":\"J9k2ZRJC1C\",\"updateTime\":\"OheKa0r2U\",\"receiverDetailAddress\":\"bzG5mJgg5\",\"version\":\"YO\",\"onLineStatus\":185,\"phone\":\"gYkEPr\",\"createTime\":\"52j0Ee\",\"sourceType\":8034,\"integration\":4187,\"growth\":2251,\"createUser\":\"Pfi6my\",\"job\":\"SObmKBKpx\",\"personalizedSignature\":\"C7\",\"luckeyCount\":3768,\"username\":\"9gr5ZU\",\"status\":8504}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "memberLevelId",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>用户名</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>密码</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "nickname",
            "description": "<p>昵称</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>手机号码</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>帐号启用状态:0-&gt;启用；1-&gt;禁用</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": "<p>注册时间</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "icon",
            "description": "<p>头像</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "gender",
            "description": "<p>性别：0-&gt;未知；1-&gt;男；2-&gt;女</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "birthday",
            "description": "<p>生日</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>所做城市</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "job",
            "description": "<p>职业</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "personalizedSignature",
            "description": "<p>个性签名</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "sourceType",
            "description": "<p>用户来源</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "integration",
            "description": "<p>积分</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "growth",
            "description": "<p>成长值</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "luckeyCount",
            "description": "<p>剩余抽奖次数</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "historyIntegration",
            "description": "<p>历史积分数量</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "onLineStatus",
            "description": "<p>websockt在线状态，0：离线，1：在线</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "receiverDetailAddress",
            "description": "<p>收货地址</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "{\"birthday\":1000143703271,\"memberLevelId\":5692,\"gender\":8196,\"city\":\"qTWHgg\",\"icon\":\"69z\",\"password\":\"R0qKtRib\",\"historyIntegration\":5460,\"nickname\":\"ue\",\"id\":6803,\"updateUser\":\"Ui\",\"updateTime\":\"g3fmMCj2L7\",\"receiverDetailAddress\":\"sKmfuuX9\",\"version\":\"OrT\",\"onLineStatus\":5538,\"phone\":\"UmPS9Chf2a\",\"createTime\":\"rhaI0Wv0O\",\"sourceType\":6152,\"integration\":6477,\"growth\":727,\"createUser\":\"sudOoH\",\"job\":\"R9\",\"personalizedSignature\":\"OJlfqRJ\",\"luckeyCount\":2363,\"username\":\"v7jJO1zUZF\",\"status\":772}",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/user/UserController.java",
    "groupTitle": "用户管理",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/user/updateByPhone"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/advertise/add",
    "title": "add",
    "version": "1.0.0",
    "group": "轮播图管理",
    "name": "add",
    "description": "<p>地址信息</p>",
    "parameter": {
      "fields": {
        "请求体": [
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>轮播名称</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "type",
            "description": "<p>轮播位置：0-&gt;PC首页轮播；1-&gt;app首页轮播</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "pic",
            "description": "<p>轮播图，图片地址</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "startTime",
            "description": "<p>轮播开始时间</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "endTime",
            "description": "<p>轮播结束时间</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>上下线状态：0-&gt;下线；1-&gt;上线</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "clickCount",
            "description": "<p>点击数</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "orderCount",
            "description": "<p>下单数</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "url",
            "description": "<p>链接地址</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "note",
            "description": "<p>备注</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求体示例",
          "content": "{\"note\":\"p0ewP\",\"clickCount\":4015,\"orderCount\":7147,\"updateUser\":\"LmBTFGY\",\"updateTime\":\"5RssuzVZXZ\",\"pic\":\"xFeYv6ZK\",\"sort\":9133,\"type\":7742,\"version\":\"Fnu\",\"url\":\"E9ew0Wo\",\"createTime\":\"dqM256bp4M\",\"name\":\"SrEQVWmBe\",\"startTime\":161645319535,\"createUser\":\"SVDWlc\",\"id\":6849,\"endTime\":1417907911207,\"status\":1499}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "response",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "3739",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/advertise/AdvertiseController.java",
    "groupTitle": "轮播图管理",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/advertise/add"
      }
    ]
  },
  {
    "type": "GET",
    "url": "/rest/advertise/delete/{id}",
    "title": "delete",
    "version": "1.0.0",
    "group": "轮播图管理",
    "name": "delete",
    "description": "<p>地址信息</p>",
    "parameter": {
      "fields": {
        "请求参数": [
          {
            "group": "请求参数",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "请求参数示例",
          "content": "id=2281",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "response",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "4041",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/advertise/AdvertiseController.java",
    "groupTitle": "轮播图管理",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/advertise/delete/{id}"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/advertise/homeByList",
    "title": "homeByList",
    "version": "1.0.0",
    "group": "轮播图管理",
    "name": "homeByList",
    "description": "<p>地址信息</p>",
    "parameter": {
      "fields": {
        "请求参数": [
          {
            "group": "请求参数",
            "type": "Number",
            "optional": false,
            "field": "pageSize",
            "description": ""
          },
          {
            "group": "请求参数",
            "type": "Number",
            "optional": false,
            "field": "pageNum",
            "description": ""
          }
        ],
        "请求体": [
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>轮播名称</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "type",
            "description": "<p>轮播位置：0-&gt;PC首页轮播；1-&gt;app首页轮播</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "pic",
            "description": "<p>轮播图，图片地址</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "startTime",
            "description": "<p>轮播开始时间</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "endTime",
            "description": "<p>轮播结束时间</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>上下线状态：0-&gt;下线；1-&gt;上线</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "clickCount",
            "description": "<p>点击数</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "orderCount",
            "description": "<p>下单数</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "url",
            "description": "<p>链接地址</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "note",
            "description": "<p>备注</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求参数示例",
          "content": "pageSize=3425&pageNum=1190",
          "type": "json"
        },
        {
          "title": "请求体示例",
          "content": "{\"note\":\"b\",\"clickCount\":5022,\"orderCount\":8360,\"updateUser\":\"WILxCsj\",\"updateTime\":\"wW4\",\"pic\":\"trow9ZTF4\",\"sort\":9761,\"type\":7647,\"version\":\"H7d8d\",\"url\":\"RA\",\"createTime\":\"4m2q\",\"name\":\"XPxcf6Hy7h\",\"startTime\":3691661729675,\"createUser\":\"L91fr\",\"id\":8059,\"endTime\":4115588208189,\"status\":4533}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pageNum",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pageSize",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "total",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pages",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Array",
            "optional": false,
            "field": "list",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.id",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.name",
            "description": "<p>轮播名称</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.type",
            "description": "<p>轮播位置：0-&gt;PC首页轮播；1-&gt;app首页轮播</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.pic",
            "description": "<p>轮播图，图片地址</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.startTime",
            "description": "<p>轮播开始时间</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.endTime",
            "description": "<p>轮播结束时间</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.status",
            "description": "<p>上下线状态：0-&gt;下线；1-&gt;上线</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.clickCount",
            "description": "<p>点击数</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.orderCount",
            "description": "<p>下单数</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.url",
            "description": "<p>链接地址</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.note",
            "description": "<p>备注</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "{\"total\":291,\"pages\":4464,\"pageSize\":3028,\"list\":[{\"note\":\"b3CjJJro\",\"clickCount\":7847,\"orderCount\":1070,\"updateUser\":\"fK\",\"updateTime\":\"yqa\",\"pic\":\"IVwVRirvF\",\"sort\":6892,\"type\":8479,\"version\":\"Z2BSkAz\",\"url\":\"ttVT\",\"createTime\":\"slK\",\"name\":\"HanCXmegKZ\",\"startTime\":1246993358943,\"createUser\":\"7tl6nebmw\",\"id\":2392,\"endTime\":2063311696,\"status\":9411}],\"pageNum\":2301}",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/advertise/AdvertiseController.java",
    "groupTitle": "轮播图管理",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/advertise/homeByList"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/advertise/list",
    "title": "list",
    "version": "1.0.0",
    "group": "轮播图管理",
    "name": "list",
    "description": "<p>地址信息</p>",
    "parameter": {
      "fields": {
        "请求参数": [
          {
            "group": "请求参数",
            "type": "Number",
            "optional": false,
            "field": "pageSize",
            "description": ""
          },
          {
            "group": "请求参数",
            "type": "Number",
            "optional": false,
            "field": "pageNum",
            "description": ""
          }
        ],
        "请求体": [
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>轮播名称</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "type",
            "description": "<p>轮播位置：0-&gt;PC首页轮播；1-&gt;app首页轮播</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "pic",
            "description": "<p>轮播图，图片地址</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "startTime",
            "description": "<p>轮播开始时间</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "endTime",
            "description": "<p>轮播结束时间</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>上下线状态：0-&gt;下线；1-&gt;上线</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "clickCount",
            "description": "<p>点击数</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "orderCount",
            "description": "<p>下单数</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "url",
            "description": "<p>链接地址</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "note",
            "description": "<p>备注</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求参数示例",
          "content": "pageSize=5421&pageNum=8282",
          "type": "json"
        },
        {
          "title": "请求体示例",
          "content": "{\"note\":\"74nqGc\",\"clickCount\":1708,\"orderCount\":8361,\"updateUser\":\"eybiGSxZ9P\",\"updateTime\":\"jt\",\"pic\":\"niz6y\",\"sort\":6428,\"type\":2357,\"version\":\"e\",\"url\":\"S\",\"createTime\":\"MI\",\"name\":\"TVWqWOa\",\"startTime\":2063178107084,\"createUser\":\"V51f2\",\"id\":7080,\"endTime\":2509708538950,\"status\":4780}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pageNum",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pageSize",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "total",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "pages",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Array",
            "optional": false,
            "field": "list",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.id",
            "description": ""
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.name",
            "description": "<p>轮播名称</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.type",
            "description": "<p>轮播位置：0-&gt;PC首页轮播；1-&gt;app首页轮播</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.pic",
            "description": "<p>轮播图，图片地址</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.startTime",
            "description": "<p>轮播开始时间</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.endTime",
            "description": "<p>轮播结束时间</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.status",
            "description": "<p>上下线状态：0-&gt;下线；1-&gt;上线</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.clickCount",
            "description": "<p>点击数</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.orderCount",
            "description": "<p>下单数</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.url",
            "description": "<p>链接地址</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.note",
            "description": "<p>备注</p>"
          },
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "list.sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "响应结果",
            "type": "String",
            "optional": false,
            "field": "list.updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "{\"total\":6961,\"pages\":3840,\"pageSize\":6705,\"list\":[{\"note\":\"ebHn\",\"clickCount\":4507,\"orderCount\":243,\"updateUser\":\"4\",\"updateTime\":\"kssOgSvFCt\",\"pic\":\"zafS\",\"sort\":207,\"type\":4759,\"version\":\"jWfy\",\"url\":\"NG9wIm4iK\",\"createTime\":\"h2s4qvC\",\"name\":\"84M\",\"startTime\":247572183164,\"createUser\":\"2Dld7R\",\"id\":7430,\"endTime\":2252812915495,\"status\":7625}],\"pageNum\":750}",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/advertise/AdvertiseController.java",
    "groupTitle": "轮播图管理",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/advertise/list"
      }
    ]
  },
  {
    "type": "POST",
    "url": "/rest/advertise/update",
    "title": "update",
    "version": "1.0.0",
    "group": "轮播图管理",
    "name": "update",
    "description": "<p>地址信息</p>",
    "parameter": {
      "fields": {
        "请求体": [
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": ""
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>轮播名称</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "type",
            "description": "<p>轮播位置：0-&gt;PC首页轮播；1-&gt;app首页轮播</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "pic",
            "description": "<p>轮播图，图片地址</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "startTime",
            "description": "<p>轮播开始时间</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "endTime",
            "description": "<p>轮播结束时间</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>上下线状态：0-&gt;下线；1-&gt;上线</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "clickCount",
            "description": "<p>点击数</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "orderCount",
            "description": "<p>下单数</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "url",
            "description": "<p>链接地址</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "note",
            "description": "<p>备注</p>"
          },
          {
            "group": "请求体",
            "type": "Number",
            "optional": false,
            "field": "sort",
            "description": "<p>排序</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>版本号</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createUser",
            "description": "<p>创建人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "createTime",
            "description": "<p>创建时间</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateUser",
            "description": "<p>修改人</p>"
          },
          {
            "group": "请求体",
            "type": "String",
            "optional": false,
            "field": "updateTime",
            "description": "<p>修改时间</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "请求体示例",
          "content": "{\"note\":\"WD\",\"clickCount\":1061,\"orderCount\":6961,\"updateUser\":\"ksBsOv\",\"updateTime\":\"zx\",\"pic\":\"1tlTBTT\",\"sort\":7865,\"type\":6662,\"version\":\"G\",\"url\":\"0AQSi9O\",\"createTime\":\"5ZsxPZ\",\"name\":\"NsQ7gEQ\",\"startTime\":3201660947483,\"createUser\":\"MxhkHxC\",\"id\":5958,\"endTime\":1865649399668,\"status\":4006}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Number",
            "optional": false,
            "field": "response",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "4139",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/advertise/AdvertiseController.java",
    "groupTitle": "轮播图管理",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/advertise/update"
      }
    ]
  },
  {
    "type": "GET",
    "url": "/rest/home/data",
    "title": "查询",
    "version": "1.0.0",
    "group": "首页信息查询",
    "name": "list",
    "description": "<p>首页信息查询</p>",
    "success": {
      "fields": {
        "响应结果": [
          {
            "group": "响应结果",
            "type": "Object",
            "optional": false,
            "field": "response",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "响应结果示例",
          "content": "{}",
          "type": "json"
        }
      ]
    },
    "filename": "src/main/java/cn/xyg/admin/home/HomeController.java",
    "groupTitle": "首页信息查询",
    "sampleRequest": [
      {
        "url": "https://apidoc.free.beeceptor.com/rest/home/data"
      }
    ]
  }
] });
