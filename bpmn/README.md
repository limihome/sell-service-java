# Bpmn Ativiti

流程引擎介绍 Activiti针对工作流的一系列操作，都封装在他的服务组件当中，因此要学习activiti，首先要做的就是学习了解他的常用服务组件。

# Ativiti 重要概念
| 英语 | 中文名称 | 说明 | 
| :--------------: |:--------------: | :------------:  |
| deployment |部署id| :------------:  |
| processInstanceId |流程实例id| :------------:  |
| executionId |执行对象id| :------------:  |
| ProcessInstance|流程实例|代表流程定义的执行实例。如范冰冰请了一天的假，她就必须发出一个流程实例的申请。<br>一个流程实例包括了所有的运行节点。我们可以利用这个对象来了解当前流程实例的进度等信息。<br>流程实例就表示一个流程从开始到结束的最大的流程分支，即一个流程中流程实例只有一个| 
| Execution|执行对象|Activiti用这个对象去描述流程执行的每一个节点。<br>在没有并发的情况下，Execution就是同ProcessInstance。<br>流程按照流程定义的规则执行一次的过程，就可以表示执行对象Execution| 
* 一个流程中，执行对象可以存在多个，但是流程实例只能有一个。
* 当流程按照规则只执行一次的时候，那么流程实例就是执行对象。

# Ativiti 
activiti的监听分为两种，一种是任务监听器，另一种执行监听器。
任务监听器：在经理办理请假任务时，自动获取“请假”所在的部门 的经理。
执行监听器：在任务结束后，自动修改 “请假单的状态” 为 已审批。

# Ativiti 常用服务组件的基本介绍

| 服务 | 说明 | 
| :--------------: | :------------:  |
| RepositoryService|提供一系列管理流程定义和流程部署的API。| 
| RuntimeService |在流程运行时对流程实例进行管理与控制。| 
| TaskService|对流程任务进行管理，例如任务提醒、任务完成和创建任务分本任务等。| 
| IdentityService(7已删除) |提供对流程角色数据进行管理的API，这些角色数据包括用户组、用户以及它们之间的关系。| 
|ManagementService|提供对流程引擎进行管理和维护的服务。| 
| HistoryService |对流程的历史数据进行操作，包括查询、删除这些历史数据。|
| FormService (7已删除)|表单Service，用于读取和流程，任务相关的表单数据。|
|DynamicBpmnService|使用该服务，可以不需要重新部署流程模型，就可以实现对流程模型的部分修改。|

# 数据表以及命名规则
Activiti 的表都以act_开头，第二部分是表示表的用途的两个字母缩写标识，用途也和服务的API对应。

| 表名 | 说明 |
| :--------------: | :------------:  |
|act_hi_*|'hi’表示 history，此前缀的表包含历史数据，如历史(结束)流程实例，变量，任务等等。|
|act_ge_*|'ge’表示 general，此前缀的表为通用数据，用于不同场景中。|
|act_evt_*|'evt’表示 event，此前缀的表为事件日志。|
|act_procdef_*|'procdef’表示 processdefine，此前缀的表为记录流程定义信息。|
|act_re_*|'re’表示 repository，此前缀的表包含了流程定义和流程静态资源(图片，规则等等)。|
|act_ru_*|'ru’表示 runtime，此前缀的表是记录运行时的数据，包含流程实例，任务，变量，异步任务等运行中的数据。<br>Activiti只在流程实例执行过程中保存这些数据，在流程结束时就会删除这些记录。|

### 通用数据(act_ge_)

| 表名 | 说明 |
| :--------------: | :------------:  |
|act_ge_bytearray|二进制数据表，存储通用的流程定义和流程资源。|
|act_ge_property|系统相关属性，属性数据表存储整个流程引擎级别的数据，初始化表结构时，会默认插入三条记录。|

### 流程定义表(act_re_)

| 表名 | 说明 |
| :--------------: | :------------:  |
|act_re_deployment	|部署信息表|
|act_re_model|流程设计模型部署表|
|act_re_procdef|流程定义数据表|

### 运行实例表(act_ru_)

| 表名 					| 说明 				|
| :--------------: 		| :------------:  	|
|act_ru_deadletter_job	|作业死亡信息表，作业失败超过重试次数|
|act_ru_event_subscr	|运行时事件表|
|act_ru_execution		|运行时流程执行实例表|
|act_ru_identitylink	|运行时用户信息表|
|act_ru_integration		|运行时积分表|
|act_ru_job				|运行时作业信息表|
|act_ru_suspended_job	|运行时作业暂停表|
|act_ru_task			|运行时任务信息表|
|act_ru_timer_job		|运行时定时器作业表|
|act_ru_variable		|运行时变量信息表|

### 历史流程表(act_hi_)

| 表名 					| 说明 				|
| :--------------: 		| :------------:  	|
|act_hi_actinst			|历史节点表|
|act_hi_attachment		|历史附件表|
|act_hi_comment			|历史意见表|
|act_hi_detail			|历史详情表，提供历史变量的查询|
|act_hi_detail			|历史流程用户信息表|
|act_hi_procinst		|历史流程实例表|
|act_hi_taskinst		|历史任务实例表|
|act_hi_varinst			|历史变量表|

### 其他表

| 表名 					| 说明 				|
| :--------------: 		| :------------:  	|
|act_evt_log			|流程引擎的通用事件日志记录表|
|act_procdef_info		|流程定义的动态变更信息|

 

# 如何理解服务组件之间的关系

如何理解服务组件之间的关系，必须先从工作流的使用角度来理解。 以报销流程为例：
工作流并不是凭空出现的，并不是员工提起报销请求，系统就毫无根据的生成了工作流。而是需要管理员来规定报销这个工作流中各个环节，各个条件。
那么管理员如何来使用activiti来定义一个新的工作流呢？RepositoryService为我们提供了与此相关的一系列方法，可以让我们能够定义工作流并将工作流部署到服务器当中去。可以将RepositoryService理解为创建java面向对象思想中的类。
当有了一个流程的定义，员工如何去申请一个报销流程呢，因为每一个员工情况都不一样，他们都需要有个独立的流程来解决各自的问题（类似于java中的对象）。
RuntimeService为我们提供了相应的方法，来根据一个流程定义去创建流程实例，并可以对流程实例进行一系列的操作。
此处还有一个task的概念，task可以理解为我们流程图中的一个审批任务（节点），一个流程实例只要被创建了并且还没有结束，就一个其中某一个节点上。而我们也可以使用RuntimeService来获取我们流程当前所处于task。而TaskService则可以对task进行完成、管理各种操作。
此外activiti还通过IdentityService提供给我们了统一管理提起流程、参与流程的所有用户和用户组的一系列方法，并允许通过taskService中的方法为task设定系统中的审批用户或者用户组。
而HistoryService则是因为我们有时需要查询已经走完全部流程的流程实例，他给我们提供一系列操作历史数据的方法。 ManagementService和DynamicBpmnService暂时用到的还比较少，以后再补充。//todo

# 引擎API
引擎 API 是与 Activiti 交互最常用的方式。主要的出发点是 ProcessEngine，可以使用配置一节中介绍的几种方式对其进行
创建。由 ProcessEngine，可以获取到含有工作流/BPM 方法的不同服务。ProcessEngine 以及那些服务对象都是线程安全的。
所以可以为整个服务器保留一份它的引用
通过@Autowired注解的方式来获取我们的服务组件，如下:<br>

@Autowired <br>
private RuntimeService runtimeService;

@Autowired <br>
private RepositoryService repositoryService;

@Autowired <br>
private TaskService taskService;

@Autowired <br>
private IdentityService identityService;

@Autowired <br>
private HistoryService historyService;
