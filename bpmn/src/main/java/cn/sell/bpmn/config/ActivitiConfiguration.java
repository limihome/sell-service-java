package cn.sell.bpmn.config;
//
//import com.alibaba.excel.util.StringUtils;
//import org.activiti.api.process.runtime.events.ProcessCompletedEvent;
//import org.activiti.api.process.runtime.events.listener.ProcessRuntimeEventListener;
//import org.activiti.api.task.runtime.events.TaskAssignedEvent;
//import org.activiti.api.task.runtime.events.TaskCandidateGroupAddedEvent;
//import org.activiti.api.task.runtime.events.TaskCandidateUserAddedEvent;
//import org.activiti.api.task.runtime.events.TaskCompletedEvent;
//import org.activiti.api.task.runtime.events.listener.TaskCandidateEventListener;
//import org.activiti.api.task.runtime.events.listener.TaskRuntimeEventListener;
//import org.activiti.engine.RuntimeService;
//import org.activiti.engine.impl.persistence.entity.ExecutionEntity;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//@Configuration
//public class ActivitiConfiguration extends DefaultActivityBehaviorFactoryMappingConfigurer {
//
//    private final RuntimeService runtimeService;
//
//    private final ActRuTaskLogService actRuTaskLogService;
//
//    public ActivitiConfiguration(RuntimeService runtimeService, ActRuTaskLogService actRuTaskLogService) {
//        this.runtimeService = runtimeService;
//        this.actRuTaskLogService = actRuTaskLogService;
//    }
//
//    @Bean
//    public TaskRuntimeEventListener<TaskAssignedEvent> taskAssignedListener() {
//        return taskAssigned -> {
//            ExecutionEntity execution = (ExecutionEntity) runtimeService.createProcessInstanceQuery()
//                    .processInstanceId(taskAssigned.getProcessInstanceId()).singleResult();
//            String startUserId = execution.getStartUserId();
//            String fileProcInstId = this.fileProcInstId(execution);
//
//            // 排除发起申请的任务，给 assignee 发消息
//            if (!taskAssigned.getEntity().getAssignee().equals(startUserId)) {
//                Task task = taskAssigned.getEntity();
//                ActRuTaskLog taskLog = new ActRuTaskLog(task.getProcessInstanceId(), task.getId(),
//                        taskAssigned.getEntity().getAssignee(), String.format(NotifyConstants.PENDING_WARN,
//                        this.userName(startUserId), this.processType(fileProcInstId), this.projName(execution)),
//                        NotifyTypeConstants.CANDIDATE);
//                actRuTaskLogService.create(taskLog);
//            }
//        };
//    }
//
//    @Bean
//    public TaskRuntimeEventListener<TaskCompletedEvent> taskCompletedListener() {
//        return taskCompleted -> {
//
//            ExecutionEntity execution = (ExecutionEntity) runtimeService.createProcessInstanceQuery()
//                    .processInstanceId(taskCompleted.getProcessInstanceId()).singleResult();
//            String startUserId = execution.getStartUserId();
//            String fileProcInstId = this.fileProcInstId(execution);
//            Task task = taskCompleted.getEntity();
//            // 发起审批，给抄送人、协助人发消息
//            if (!taskCompleted.getEntity().getAssignee().equals(startUserId)) {
//                // 任务所有人
//                String owner = taskCompleted.getEntity().getOwner();
//                ActRuTaskLog taskLog = new ActRuTaskLog(task.getProcessInstanceId(), task.getId(),
//                        this.userName(owner), String.format(NotifyConstants.PENDING_WARN,
//                        this.userName(startUserId), this.processType(fileProcInstId), this.projName(execution)),
//                        NotifyTypeConstants.CANDIDATE);
//                actRuTaskLogService.create(taskLog);
//            } else {
//                // 给发起人发送任务处理结果的通知
//                ActRuTaskLog taskLog = new ActRuTaskLog(task.getProcessInstanceId(), task.getId(),
//                        taskCompleted.getEntity().getAssignee(), String.format(NotifyConstants.PENDING,
//                        this.userName(startUserId), this.processType(fileProcInstId), this.projName(execution),
//                        this.userName(task.getAssignee()), ""), NotifyTypeConstants.PENDING);
//                actRuTaskLogService.create(taskLog);
//            }
//        };
//    }
//
//    @Bean
//    public TaskCandidateEventListener<TaskCandidateUserAddedEvent> taskCandidateUserEventListener() {
//        return taskCandidateEvent -> log.info(">>> Task Candidate User Add: '"
//                + taskCandidateEvent.getEntity().toString());
//    }
//
//    @Bean
//    public TaskCandidateEventListener<TaskCandidateGroupAddedEvent> taskCandidateGroupEventListener() {
//        return taskCandidateEvent -> log.info(">>> Task Candidate Group Add: '"
//                + taskCandidateEvent.getEntity().toString());
//    }
//
//    @Bean
//    public ProcessRuntimeEventListener<ProcessCompletedEvent> processCompletedEventListener() {
//        return processCompletedEvent -> log.info("===>>> Process Completed: '"
//                + processCompletedEvent.getEntity().toString());
//    }
//
//    /**
//     * 获取流程表单名
//     *
//     * @param executionEntity 执行对象
//     * @return 表单名
//     */
//    private String projName(ExecutionEntity executionEntity) {
//        Object processInstanceName = executionEntity.getVariable("projName");
//        return null == processInstanceName ? "" : processInstanceName.toString();
//    }
//
//    /**
//     * 获取流程文件ID
//     *
//     * @param executionEntity 执行对象
//     * @return 文件ID
//     */
//    private String fileProcInstId(ExecutionEntity executionEntity) {
//        Object fileProcInstId = executionEntity.getVariable("fileProcInstId");
//        return fileProcInstId == null ? "" : fileProcInstId.toString();
//    }
//
//    /**
//     * 审批类型
//     *
//     * @param fileProcInstId 文件ID
//     * @return 类型
//     */
//    private String processType(String fileProcInstId) {
//        return StringUtils.hasText(fileProcInstId) ? "用印" : "合同";
//    }
//
//    /**
//     * 获取姓名
//     *
//     * @param userId 用户ID
//     * @return 用户姓名
//     */
//    private String userName(String userId) {
//        return userId + "操作人";
//    }
//}
