package cn.sell.bpmn.repository;

import org.activiti.bpmn.model.BpmnModel;
import org.activiti.engine.repository.Deployment;

import java.io.FileInputStream;
import java.util.Map;
import java.util.zip.ZipInputStream;

/**
 * 流程图部署相关的服务
 */
public interface BpmReposittoryService {

    /**
     * 通过字符串部署流程
     *
     * @param processName  部署流程的名称
     * @param bpmnContext  流程图文本
     * @param resourceName 流程图资源名称
     * @return
     */
    Deployment DeploymentProcessDefinition(String processName, String bpmnContext, String resourceName);

    /**
     * 通过文件流部署流程
     *
     * @param processName     部署流程的名称
     * @param zipInputStream 流程图文件流
     * @return
     */
    Deployment DeploymentProcessDefinition(String processName,  ZipInputStream zipInputStream);

    Deployment DeploymentProcessDefinition(String processName, String resourceName, BpmnModel bpmnModel);

    /**
     * 查询流程定义
     * ProcessDefinition 流程定义
     *
     * @param deploymentId         部署id
     * @param processDefinitionKey 流程定义key
     * @param pageSiez             每页条数
     * @param pageNum              当前页
     * @return
     */
    Map<String, Object> queryProcessDefinition
    (String deploymentId, String processDefinitionKey, int pageSiez, int pageNum);

    /**
     * 根据部署id删除已经部署的流程，以及流程定义
     * 如果该流程定义下没有正在运行的流程，则可以用普通删除。如果是有关联的信息，用级联删除。
     * 项目开发中使用级联删除的情况比较多，删除操作一般只开放给超级管理员使用。
     *
     * @param deploymentId 部署id
     * @param cascade      是否级联删除
     */
    void deleteProcessDefinition(String deploymentId, boolean cascade);

    /**
     * 根据流程定义的key删除已经部署的流程，以及流程定义，级联删除
     *
     * @param processDefinitionKey
     */
    void deleteProcessDefinitionByKey(String processDefinitionKey);
}
