package cn.sell.bpmn.task.impl;

import cn.sell.bpmn.task.BpmTaskService;
import lombok.extern.log4j.Log4j2;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Log4j2
@Service
public class BpmTaskServiceImpl implements BpmTaskService {
    @Autowired
    TaskService taskService;

    /**
     * 查询当前人的个人任务
     */
    public List<Task> getMyPersonalTask(String assignee) {
        List<Task> list = taskService
                .createTaskQuery()//创建任务的查询对象
                .taskAssignee(assignee) // 指定个人任务查询，指定办理人
                .list();
        for (Task task : list) {
            log.info("任务ID：" + task.getId());
            log.info("任务名称：" + task.getName());
            log.info("任务创建时间：" + task.getCreateTime());
            log.info("任务创建人：" + task.getAssignee());
            log.info("流程实例ID：" + task.getProcessInstanceId());
            log.info("执行对象ID：" + task.getExecutionId());
            log.info("流程定义ID：" + task.getProcessDefinitionId());
            log.info("============================================");
        }
        return list;
    }

    /*完成任务*/
    public void completeMyPersonalTask(String taskId) {
        taskService.complete(taskId);
        /**
         * act_ru_task(运行时任务数据表，任务表)、act_ru_execution(执行对象表)、
         * act_ru_identitylink(流程用户表)、ACT_RU_VARIABLE(运行时流程变量数据表)
         */
    }
}
