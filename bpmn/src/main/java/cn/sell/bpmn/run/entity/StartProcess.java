package cn.sell.bpmn.run.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class StartProcess implements Serializable {

    /**
     * 用户编号
     */
    String userId;
    /**
     * 代理人编号
     */
    String agentId;
    /**
     * 流程定义key
     */
    String processInstanceByKey;
    /**
     * 流程定义ID
     */
    String processInstanceById;
    /**
     * 业务编号
     */
    String businessKey;
    /**
     * 流程变量
     */
    ProcessVariable processVariables;
}
