package cn.sell.bpmn.listener;

import lombok.extern.log4j.Log4j2;
import org.activiti.engine.delegate.*;
import org.activiti.engine.delegate.TaskListener;
import org.activiti.engine.delegate.event.ActivitiEvent;
import org.activiti.engine.delegate.event.ActivitiEventListener;
import org.activiti.engine.impl.delegate.TriggerableActivityBehavior;

@Log4j2
public class ActivitiListenner implements ActivitiEventListener {

    /**
     *
     */
    private static final long serialVersionUID = -3759054058055401826L;

    @Override
    public void onEvent(ActivitiEvent event) {

    }

    @Override
    public boolean isFailOnException() {
        return false;
    }
}
