package cn.sell.bpmn.listener;

import lombok.extern.log4j.Log4j2;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.impl.delegate.TriggerableActivityBehavior;

/*
 * 消息接收节点监听方法
 */
@Log4j2
public class ReceiveListener implements TriggerableActivityBehavior {
    /**
     *
     */
    private static final long serialVersionUID = -8826768727410261800L;

    protected void handle(DelegateExecution execution) {
        // TODO Auto-generated method stub
        log.info("获取当前活动任务的配置编号", execution.getCurrentActivityId());
        System.out.println("#############################:" + execution.getCurrentActivityId());
    }

    @Override
    public void execute(DelegateExecution arg0) {
        // TODO Auto-generated method stub
        log.info("#############################:" + arg0.getCurrentActivityId());
    }

    @Override
    public void trigger(DelegateExecution arg0, String arg1, Object arg2) {
        // TODO Auto-generated method stub
        log.info("#############################:" + arg0.getCurrentActivityId());
    }


}
