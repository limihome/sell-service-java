package cn.sell.bpmn.listener;

import lombok.extern.log4j.Log4j2;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;

@Log4j2
public class JavaDelegateActivitiListenner implements JavaDelegate {
    @Override
    public void execute(DelegateExecution execution) {
        System.out.println("executionId：" + execution.getId() + " ActivitiListenner" + this.toString());
    }
}
