package cn.sell.bpmn.listener;


import lombok.extern.log4j.Log4j2;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.activiti.engine.delegate.JavaDelegate;

/*
 * 自动任务执行过程
 */
@Log4j2
public class ServerListener implements JavaDelegate, ExecutionListener {

    @Override
    public void execute(DelegateExecution execution) {
        // TODO Auto-generated method stub
        log.info("=================================::自动任务执行：ServerListener");
        // 获取当前活动id bpm_wftask 的Id task_def_id
        String task_def_id = execution.getCurrentActivityId();
        String brchcd = "";
        String wfrole = "";
//        bizlog.info("获取当前活动任务的配置编号", task_def_id);
        // 获取当前流程实例的申请编号
        // 流程公共变量
        // 根据 task_def_id poc_def_id 查询 bpm_wftask
        // 将更新后的流程变量重新放入流程中 放入当前流程变量即可
        //        ActivitiUtil.getRuntimeService().setVariables(execution.getId(),
        //                wfVar);
    }

    @Override
    public void notify(DelegateExecution execution) {

    }
}

