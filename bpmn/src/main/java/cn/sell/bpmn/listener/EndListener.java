package cn.sell.bpmn.listener;


import lombok.extern.log4j.Log4j2;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

@Log4j2
public class EndListener implements ExecutionListener {
    /**
     *
     */
    private static final long serialVersionUID = -4768395048131200982L;

    @Override
    public void notify(DelegateExecution execution) {
        log.info("=================================::流程运行结束：EndListener");
        log.info("=================================::"+execution.getCurrentActivityId());
        log.info("=================================::"+execution.getProcessInstanceId());
        //TODO: 修改业务数据状态为结束状态
    }

}
