package cn.sell.bpmn.enum;

public enum ActivitiEventType {

  // ENTITY ：流程实例，发起流程时，从流程模板中创建实例
  ENTITY_CREATED,  // 创建

  ENTITY_INITIALIZED,  // 初始化完成（如果这个实体的创建会包含子实体的创建，这个事件会在子实体都创建/初始化完成后被触发，这是与ENTITY_CREATED的区别）

  ENTITY_UPDATED,  // 更新

  ENTITY_DELETED,  // 删除

  ENTITY_SUSPENDED,  // 暂停（会被ProcessDefinitions, ProcessInstances 和 Tasks抛出）

  ENTITY_ACTIVATED,  // 激活（会被ProcessDefinitions, ProcessInstances 和 Tasks抛出）

  // 定时器
  TIMER_SCHEDULED,  // 创建

  TIMER_FIRED,  // 触发

  // 作业
  JOB_CANCELED,  // 取消

  JOB_EXECUTION_SUCCESS,  // 执行成功

  JOB_EXECUTION_FAILURE,  // 执行失败

  JOB_RETRIES_DECREMENTED,  // 重试减少（因为作业执行失败，导致重试次数减少）

  CUSTOM,  // 自定义

  // 引擎
  ENGINE_CREATED,  // 创建

  ENGINE_CLOSED,  // 关闭

  // 流程节点
  ACTIVITY_STARTED,  // 开始

  ACTIVITY_COMPLETED,  // 完成

  ACTIVITY_CANCELLED,  // 取消

  ACTIVITY_SIGNALED,  // 收到了一个信号

  ACTIVITY_COMPENSATE,  // 将要被补偿

  ACTIVITY_MESSAGE_SENT,  // 消息发送

  ACTIVITY_MESSAGE_WAITING,  // 消息等待

  ACTIVITY_MESSAGE_RECEIVED,  // 消息接收

  ACTIVITY_ERROR_RECEIVED,  // 接收失败

  // 流程历史
  HISTORIC_ACTIVITY_INSTANCE_CREATED,  // 创建

  HISTORIC_ACTIVITY_INSTANCE_ENDED,  // 结束

  // 队列流程
  SEQUENCEFLOW_TAKEN,  // 已采取

  UNCAUGHT_BPMN_ERROR,  // 未获取到bpmn 异常

  // 变量
  VARIABLE_CREATED,  // 创建

  VARIABLE_UPDATED,  // 更新

  VARIABLE_DELETED,  // 删除

  // 任务
  TASK_CREATED,  // 创建（它位于ENTITY_CREATE事件之后。当任务是由流程创建时，这个事件会在TaskListener执行之前被执行）

  TASK_ASSIGNED,  // 分配

  TASK_COMPLETED,  // 完成（它会在ENTITY_DELETE事件之前触发。当任务是流程一部分时，事件会在流程继续运行之前，   后续事件将是ACTIVITY_COMPLETE，对应着完成任务的节点）

  // 进程
  PROCESS_STARTED,  // 开始

  PROCESS_COMPLETED,  // 完成（在最后一个节点的ACTIVITY_COMPLETED事件之后触发。 当流程到达的状态，没有任何后续连线时， 流程就会结束。）

  PROCESS_COMPLETED_WITH_ERROR_END_EVENT,  // 异常结束

  PROCESS_CANCELLED,  // 取消

  HISTORIC_PROCESS_INSTANCE_CREATED,  // 流程实例创建

  HISTORIC_PROCESS_INSTANCE_ENDED,  // 流程实例创建

  // 成员
  MEMBERSHIP_CREATED,  // 用户被添加到一个组里

  MEMBERSHIP_DELETED,  // 用户被从一个组中删除

  MEMBERSHIPS_DELETED;  // 所有成员被从一个组中删除

  // other code ...
}
