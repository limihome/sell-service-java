package cn.sell.demo.hql.base.impl;

import cn.sell.demo.hql.commons.HqlFilter;
import cn.sell.demo.hql.commons.SqlFilter;
import com.hk.dao.base.IBaseDao;
import com.hk.dao.base.IBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;

/**
 * 基础业务逻辑
 * 
 * @author zzx
 * 
 * @param <T>
 */
@Service
public class BaseServiceImpl<T> implements IBaseService<T> {

	@Autowired
	public IBaseDao<T> baseDao;

	@Override
	public Serializable save(T o) {
		return baseDao.save(o);
	}

	@Override
	public void delete(T o) {
		baseDao.delete(o);
	}

	@Override
	public void update(T o) {
		baseDao.update(o);
	}

	@Override
	public void saveOrUpdate(T o) {
		baseDao.saveOrUpdate(o);
	}

	@Override
	public T getById(Serializable id) {
		Class<T> c = (Class<T>) ((ParameterizedType) getClass()
				.getGenericSuperclass()).getActualTypeArguments()[0];
		return baseDao.getById(c, id);
	}

	@Override
	public T getByHql(String hql) {
		return baseDao.getByHql(hql);
	}

	@Override
	public T getByHql(String hql, Map<String, Object> params) {
		return baseDao.getByHql(hql, params);
	}

	@Override
	public T getByFilter(HqlFilter hqlFilter) {
		String className = ((Class<T>) ((ParameterizedType) getClass()
				.getGenericSuperclass()).getActualTypeArguments()[0]).getName();
		String hql = "select distinct t from " + className + " t";
		return getByHql(hql + hqlFilter.getWhereAndOrderHql(),
				hqlFilter.getParams());
	}

	@Override
	public List<T> find() {
		return findByFilter(new HqlFilter());
	}

	@Override
	public List<T> find(String hql) {
		return baseDao.find(hql);
	}

	@Override
	public List<T> find(String hql, Map<String, Object> params) {
		return baseDao.find(hql, params);
	}

	@Override
	public List<T> findByFilter(HqlFilter hqlFilter) {
		String className = ((Class<T>) ((ParameterizedType) getClass()
				.getGenericSuperclass()).getActualTypeArguments()[0]).getName();
		String hql = "select distinct t from " + className + " t";
		return find(hql + hqlFilter.getWhereAndOrderHql(),
				hqlFilter.getParams());
	}

	@Override
	public List<T> find(String hql, int page, int rows) {
		return baseDao.find(hql, page, rows);
	}

	@Override
	public List<T> find(String hql, Map<String, Object> params, int page,
			int rows) {
		return baseDao.find(hql, params, page, rows);
	}

	@Override
	public List<T> find(int page, int rows) {
		return findByFilter(new HqlFilter(), page, rows);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> findByFilter(HqlFilter hqlFilter, int page, int rows) {
		String className = ((Class<T>) ((ParameterizedType) getClass()
				.getGenericSuperclass()).getActualTypeArguments()[0]).getName();
		String hql = "select distinct t from " + className + " t";
		return find(hql + hqlFilter.getWhereAndOrderHql(),
				hqlFilter.getParams(), page, rows);
	}

	@Override
	public Long count(String hql) {
		return baseDao.count(hql);
	}

	@Override
	public Long count(String hql, Map<String, Object> params) {
		return baseDao.count(hql, params);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long countByFilter(HqlFilter hqlFilter) {
		String className = ((Class<T>) ((ParameterizedType) getClass()
				.getGenericSuperclass()).getActualTypeArguments()[0]).getName();
		String hql = "select count(distinct t) from " + className + " t";
		return count(hql + hqlFilter.getWhereHql(), hqlFilter.getParams());
	}

	@Override
	public Long count() {
		return countByFilter(new HqlFilter());
	}

	@Override
	public int executeHql(String hql) {
		return baseDao.executeUpdateHql(hql);
	}

	@Override
	public int executeHql(String hql, Map<String, Object> params) {
		return baseDao.executeHql(hql, params);
	}

	@Override
	public List executeListHql(String hql) {
		return baseDao.executeListHql(hql);
	}

	@Override
	public List findBySql(String sql) {
		return baseDao.findBySql(sql);
	}

	@Override
	public List<T> findEntityBySql(String sql,Class clz) {
		return baseDao.findEntityBySql(sql,clz);
	}

	@Override
	public List findBySql(String sql, int page, int rows) {
		return baseDao.findBySql(sql, page, rows);
	}

	@Override
	public List findBySql(String sql, Map<String, Object> params) {
		return baseDao.findBySql(sql, params);
	}

	@Override
	public List findBySql(String sql, Map<String, Object> params, int page,
			int rows) {
		return baseDao.findBySql(sql, params, page, rows);
	}
	
	@Override
	public int executeSql(String sql) {
		return baseDao.executeSql(sql);
	}

	@Override
	public int executeUpdateSql(String sql) {
		return baseDao.executeSql(sql);
	}

	@Override
	public int executeSql(String sql, Map<String, Object> params) {
		return baseDao.executeSql(sql, params);
	}

	@Override
	public int executeUpdateHql(String sql, Map<String, Object> params) {
		return baseDao.executeHql(sql, params);
	}

	@Override
	public BigInteger countBySql(String sql) {
		return baseDao.countBySql(sql);
	}

	@Override
	public BigInteger countBySql(String sql, Map<String, Object> params) {
		return baseDao.countBySql(sql, params);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<T> findBySqlFilter(SqlFilter sqlFilter) {
		Class<T> c = (Class<T>) ((ParameterizedType) getClass()
				.getGenericSuperclass()).getActualTypeArguments()[0];
		String sql = "select distinct t.* from " + sqlFilter.getTableName()
				+ " t";
		return baseDao.queryBySql(c, sql + sqlFilter.getWhereAndOrderHql(),
				sqlFilter.getParams());
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> findBySqlFilter(SqlFilter sqlFilter, int page, int rows) {
		Class<T> c = (Class<T>) ((ParameterizedType) getClass()
				.getGenericSuperclass()).getActualTypeArguments()[0];
		String sql = "select distinct t.* from " + sqlFilter.getTableName()
				+ " t";
		return baseDao.queryBySql(c, sql + sqlFilter.getWhereAndOrderHql(),
				sqlFilter.getParams(), page, rows);
	}

	@Override
	public BigInteger countBySqlFilter(SqlFilter sqlFilter) {
		String sql = "select count(distinct t.id) from "
				+ sqlFilter.getTableName() + " t";
		return countBySql(sql + sqlFilter.getWhereHql(), sqlFilter.getParams());
	}

	@Override
	public void merge(T o) {
		baseDao.merge(o);
		
	}

	@Override
	public List executeListHql(String hql, int firstResult, int maxCount) {
		
		return baseDao.executeListHql(hql, firstResult, maxCount);
	}
}
