package com.hk.dao.base;

import cn.sell.demo.hql.commons.HqlFilter;
import cn.sell.demo.hql.commons.SqlFilter;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;



/**
 * 基础业务逻辑类，其他service继承此service获得基本的业�?
 * 
 * @author Sam
 * 
 * @param <T>
 */
public interface IBaseService<T> {

	/**
	 * 保存�?��对象
	 * 
	 * @param o
	 *            对象
	 * @return 对象的ID
	 */
	public Serializable save(T o);

	/**
	 * 删除�?��对象
	 * 
	 * @param o
	 *            对象
	 */
	public void delete(T o);

	/**
	 * 更新�?��对象
	 * 
	 * @param o
	 *            对象
	 */
	public void update(T o);

	/**
	 *  更新时候先查询
	 * @param o
	 */
	public void merge(T o);
	
	/**
	 * 保存或更新一个对�?
	 * 
	 * @param o
	 *            对象
	 */
	public void saveOrUpdate(T o);

	/**
	 * 通过主键获得对象
	 * 
	 * @param c
	 *            类名.class
	 * @param id
	 *            主键
	 * @return 对象
	 */
	public T getById(Serializable id);

	/**
	 * 通过HQL语句获取�?��对象
	 * 
	 * @param hql
	 *            HQL语句
	 * @return 对象
	 */
	public T getByHql(String hql);

	/**
	 * 通过HQL语句获取�?��对象
	 * 
	 * @param hql
	 *            HQL语句
	 * @param params
	 *            参数
	 * @return 对象
	 */
	public T getByHql(String hql, Map<String, Object> params);

	/**
	 * 通过HqlFilter获取�?��对象
	 * 
	 * @param hqlFilter
	 * @return
	 */
	public T getByFilter(HqlFilter hqlFilter);

	/**
	 * 获得对象列表
	 * 
	 * @return
	 */
	public List<T> find();

	/**
	 * 获得对象列表
	 * 
	 * @param hql
	 *            HQL语句
	 * @return List
	 */
	public List<T> find(String hql);

	/**
	 * 获得对象列表
	 * 
	 * @param hql
	 *            HQL语句
	 * @param params
	 *            参数
	 * @return List
	 */
	public List<T> find(String hql, Map<String, Object> params);

	/**
	 * 获得对象列表
	 * 
	 * @param hqlFilter
	 * @return
	 */
	public List<T> findByFilter(HqlFilter hqlFilter);

	/**
	 * 获得分页后的对象列表
	 * 
	 * @param hql
	 *            HQL语句
	 * @param page
	 *            要显示第几页
	 * @param rows
	 *            每页显示多少�?
	 * @return List
	 */
	public List<T> find(String hql, int page, int rows);

	/**
	 * 获得分页后的对象列表
	 * 
	 * @param hql
	 *            HQL语句
	 * @param params
	 *            参数
	 * @param page
	 *            要显示第几页
	 * @param rows
	 *            每页显示多少�?
	 * @return List
	 */
	public List<T> find(String hql, Map<String, Object> params, int page,
			int rows);

	/**
	 * 获得分页后的对象列表
	 * 
	 * @param page
	 * @param rows
	 * @return
	 */
	public List<T> find(int page, int rows);

	/**
	 * 获得分页后的对象列表
	 * 
	 * @param hqlFilter
	 * @param page
	 * @param rows
	 * @return
	 */
	public List<T> findByFilter(HqlFilter hqlFilter, int page, int rows);

	/**
	 * 统计数目
	 * 
	 * @param hql
	 *            HQL语句(select count(*) from T)
	 * @return long
	 */
	public Long count(String hql);

	/**
	 * 统计数目
	 * 
	 * @param hql
	 *            HQL语句(select count(*) from T where xx = :xx)
	 * @param params
	 *            参数
	 * @return long
	 */
	public Long count(String hql, Map<String, Object> params);

	/**
	 * 统计数目
	 * 
	 * @param hqlFilter
	 * @return
	 */
	public Long countByFilter(HqlFilter hqlFilter);

	/**
	 * 统计数目
	 * 
	 * @return long
	 */
	public Long count();

	/**
	 * 执行�?��HQL语句
	 * 
	 * @param hql
	 *            HQL语句
	 * @return 响应结果数目
	 */
	public int executeHql(String hql);

	/**
	 * 执行�?��HQL语句
	 * 
	 * @param hql
	 *            HQL语句
	 * @param params
	 *            参数
	 * @return 响应结果数目
	 */
	public int executeHql(String hql, Map<String, Object> params);

	public int executeUpdateHql(String hql, Map<String, Object> params);

	public int executeUpdateSql(String hql);

	public List executeListHql(String hql);
	
	public List executeListHql(String hql ,int firstResult ,int maxCount);

	/**
	 * 获得结果�?
	 * 
	 * @param sql
	 *            SQL语句
	 * @return 结果�?
	 */
	public List findBySql(String sql);

	/**
	 * 获得结果�?
	 *
	 * @param sql
	 *            SQL语句
	 * @return 结果�?
	 */
	public List<T> findEntityBySql(String sql,Class clz);

	/**
	 * 获得结果�?
	 * 
	 * @param sql
	 *            SQL语句
	 * @param page
	 *            要显示第几页
	 * @param rows
	 *            每页显示多少�?
	 * @return 结果�?
	 */
	public List findBySql(String sql, int page, int rows);

	/**
	 * 获得结果�?
	 * 
	 * @param sql
	 *            SQL语句
	 * @param params
	 *            参数
	 * @return 结果�?
	 */
	public List findBySql(String sql, Map<String, Object> params);

	/**
	 * 获得结果�?
	 * 
	 * @param sql
	 *            SQL语句
	 * @param params
	 *            参数
	 * @param page
	 *            要显示第几页
	 * @param rows
	 *            每页显示多少�?
	 * @return 结果�?
	 */
	public List findBySql(String sql, Map<String, Object> params, int page,
			int rows);

	/**
	 * 执行SQL语句
	 * 
	 * @param sql
	 *            SQL语句
	 * @return 响应行数
	 */
	public int executeSql(String sql);

	/**
	 * 执行SQL语句
	 * 
	 * @param sql
	 *            SQL语句
	 * @param params
	 *            参数
	 * @return 响应行数
	 */
	public int executeSql(String sql, Map<String, Object> params);

	/**
	 * 统计
	 * 
	 * @param sql
	 *            SQL语句
	 * @return 数目
	 */
	public BigInteger countBySql(String sql);

	/**
	 * 统计
	 * 
	 * @param sql
	 *            SQL语句
	 * @param params
	 *            参数
	 * @return 数目
	 */
	public BigInteger countBySql(String sql, Map<String, Object> params);

	public List<T> findBySqlFilter(SqlFilter sqlFilter);

	public List<T> findBySqlFilter(SqlFilter sqlFilter, int page, int rows) ;

	public BigInteger countBySqlFilter(SqlFilter sqlFilter) ;

}
