package com.hk.dao.base;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

/**
 * 
 * @ClassName: JdbcBaseDaoI
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author zzx
 * @date 2014-5-29 上午09:07:37
 */
public interface IJdbcBaseDao<T> {
	public void execute(String sql) throws DataAccessException ;
	
	public int update(String sql) throws DataAccessException;
	
	public int update(String sql,Object[]obj) throws DataAccessException;
	
	public int update(String sql,Object[]obj,int[] type) throws DataAccessException ;
	
	public List query(String sql);
	
	public <T> List<T> query(String sql,RowMapper rowMapper) ;
	
	public JdbcTemplate getJdbcTemplate();

}
