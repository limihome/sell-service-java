package cn.sell.demo.hql.base.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

/**
 * 
 * @ClassName: JdbcBaseDao
 * @Description: TODO 使用JDBC处理数据库交互
 * @author Sam
 * @date 2014-5-3 上午02:25:55
 */
@SuppressWarnings("unchecked")
@Repository
public class JdbcBaseDao implements com.hk.dao.base.IJdbcBaseDao {

	@Resource
	private JdbcTemplate jdbcTemplate;

	/**
	 * 执行普通SQL语句
	 * 
	 * @param sql
	 * @throws DataAccessException
	 */
	public void execute(String sql) throws DataAccessException {
		Assert.hasText(sql, "Sql must be not null!");
		this.jdbcTemplate.execute(sql);
	}

	/**
	 * 执行update语句
	 * 
	 * @param sql
	 * @return 被影响的行数
	 * @throws DataAccessException
	 */
	public int update(String sql) throws DataAccessException {
		Assert.hasText(sql, "Sql must be not null!");
		return this.jdbcTemplate.update(sql);
	}
	
	/**
	 * 执行update语句
	 * @param sql
	 * @param 被影响的行数
	 * @param type
	 * @return
	 * @throws DataAccessException
	 */
	public int update(String sql,Object[]obj) throws DataAccessException {
		Assert.hasText(sql, "Sql must be not null!");
		return this.jdbcTemplate.update(sql, obj);
	}
	
	/**
	 * 执行update语句
	 * @param sql
	 * @param 被影响的行数
	 * @param type
	 * @return
	 * @throws DataAccessException
	 */
	public int update(String sql,Object[]obj,int[] type) throws DataAccessException {
		Assert.hasText(sql, "Sql must be not null!");
		return this.jdbcTemplate.update(sql, obj, type);
	}
	
	/**
	 * 执行sql语句得到一个结果列表
	 * @param sql
	 * @return
	 */
	public List query(String sql) {
		Assert.hasText(sql, "Sql must be not null!");
		return this.jdbcTemplate.queryForList(sql);
	}
	

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	
	/**
	 * 执行sql语句得到一个结果列表
	 * @param sql
	 * @param rowMapper
	 * @return
	 */
	@Override
	public  List query(String sql, RowMapper rowMapper) {
		Assert.hasText(sql, "Sql must be not null!");
		return this.jdbcTemplate.query(sql, rowMapper);
	}
	
}
