# Sell

单体商户类的电商系统
该项目为前后端分离项目的后台部分。
整体项目包含：Web、H5、Android、Ios、微信小程序、抖音小程序、支付宝小程序、后台管理系统、后台

# 项目地址

- 移动端：[www.baidu.com](www.baidu.com)
- web端：[www.baidu.com](www.baidu.com)
- 服务端：[https://gitee.com/limihome/](www.baidu.com)
- 后台管理：[https://gitee.com/limihome/](www.baidu.com)

# 技术选型

|    技术    |      说明      |                    官网                    |
| :--------: | :------------: | :----------------------------------------: |
| springboot |    基础框架    |   https://spring.io/projects/spring-boot   |
|  mybatis   |   持久层框架   |           http://www.mybatis.cn/           |
| tkmybatis  | 实现傻瓜式开发 |           http://www.mybatis.cn/           |
|   apidoc   |    接口文档    |           https://apidocjs.com/            |
|  xxl-job   |    定时任务    |      https://www.xuxueli.com/xxl-job/      |
|   WxJava   |  微信开发sdk   | https://gitee.com/binary/weixin-java-tools |
|   lombok   | 没有。项目会报错 |         https://projectlombok.org/         |
|   Hutool   |小而全的Java工具类库|       https://www.hutool.cn/docs/#/         |
|   redis   |内存中的数据结构存储系统(key-value存储系统)|       http://www.redis.cn/         |

# 项目结构

```lua
sell-service-java
├── admin -- 后台系统服务
├───┼── admin -- 控制层
├───┼── config -- 配置类信息
├───┼── filter -- 过滤器
├───┼── interceptor -- 拦截器
├───┼── listener -- 监听器
├───┼── task -- 定时任务执行器
├── app -- app服务
├───┼── app -- 控制层
├───┼── config -- 配置类信息
├───┼── filter -- 过滤器
├───┼── interceptor -- 拦截器
├───┼── listener -- 监听器
├───┼── task -- 定时任务执行器
├── comm -- 公共模块
├───┼── base -- 通用服务
├───┼───┼── MySqlCrudServiceImpl -- 通用服务基类封装，
├───┼───┼── mapper -- tk.mybatis接口
├───┼───┼── service -- 通用服务接口
├───┼── result -- 统一返回报文格式封装
├── mbg -- service层
├───┼── crm -- 客户相关
├───┼── dms -- 营销相关
├───┼── oms -- 订单相关
├───┼── pms -- 订单相关
├───┼── sys -- 系统相关
├───┼── erp -- 财务相关
├───┼── bpm -- 流程相关
├───┼── hrm -- 人力相关
├───┼── Generator -- 反向生成dao
├───┼───┼── mapper -- *dao mybatis
├───┼───┼── mapper -- *mapper tk.mybatis
├───┼───┴── service -- 业务层
├── dataSource -- 数据库
└── xxl-job-admin -- 定时任务 可直接启动
```