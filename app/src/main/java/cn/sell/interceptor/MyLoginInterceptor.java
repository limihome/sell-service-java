package cn.sell.interceptor;


import lombok.extern.log4j.Log4j2;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * @ClasssName: MyLoginInterceptor
 * @description: TODO：登录拦截器
 * @Auctior: limi
 * @Date: 2019/7/3011:23
 **/
@Log4j2
public class MyLoginInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest req, HttpServletResponse res, Object arg2) throws Exception {
        log.debug("=================== 拦截器开始工作 ===================");
        String requestURI = req.getRequestURI();

//        if (requestURI.contains("/rest/user/login")) {
//            log.info("直接放行！");
//            return true;
//        }
        log.debug("app:=================== 在请求处理之前进行调用（Controller方法调用之前） ===================");
        return true;// 只有返回true才会继续向下执行，返回false取消当前请求
    }

    @Override
    public void postHandle(HttpServletRequest req, HttpServletResponse arg1, Object res, ModelAndView arg3)
            throws Exception {
        log.debug("app:>>>>>>>>>>请求处理之后进行调用，但是在视图被渲染之前（Controller方法调用之后）");
    }

    @Override
    public void afterCompletion(HttpServletRequest req, HttpServletResponse res, Object arg2, Exception arg3)
            throws Exception {
        log.debug("app:>>>>>>>>>>在整个请求结束之后被调用，也就是在DispatcherServlet 渲染了对应的视图之后执行（主要是用于进行资源清理工作）");
    }
}
