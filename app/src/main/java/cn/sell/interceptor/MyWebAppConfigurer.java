package cn.sell.interceptor;

import cn.sell.comm.result.interceptor.ResponseResultInterceptor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.servlet.config.annotation.*;

/**
 * @ClasssName: MyTdWebAppConfigurer
 * @description: TODO：拦截器
 * @Auctior: limi
 * @Date: 2019/7/30 11:19
 **/
@Log4j2
@Configuration
public class MyWebAppConfigurer extends WebMvcConfigurationSupport {

    private final static String ApiUri = "/rest/**";

    /**
     * 解决跨域问题
     **/
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        super.addCorsMappings(registry);
        registry.addMapping(ApiUri)
                .allowedHeaders("*")
                .allowedMethods("POST","GET")
                .allowedOrigins("*");

    }

    /**
     * 添加拦截器
     **/
    @Override
    protected void addInterceptors(InterceptorRegistry registry) {
        // 加入请求信息打印拦截器
        registry.addInterceptor(new MyLoginInterceptor()).addPathPatterns(ApiUri);

        // 加入返回格式拦截器,只有打开时才生效
        registry.addInterceptor(new ResponseResultInterceptor()).addPathPatterns(ApiUri);
        super.addInterceptors(registry);
    }

    /**
     * 这里配置视图解析器
     **/
    @Override
    protected void configureViewResolvers(ViewResolverRegistry registry) {
        log.info("MyTdWebAppConfigurer.configureViewResolvers");
        super.configureViewResolvers(registry);
    }

    /**
     * 配置内容裁决的一些选项
     **/
    @Override
    protected void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
        log.info("MyTdWebAppConfigurer.configureContentNegotiation");
        super.configureContentNegotiation(configurer);
    }

    /**
     * 视图跳转控制器
     **/
    @Override
    protected void addViewControllers(ViewControllerRegistry registry) {
        log.info("MyTdWebAppConfigurer.addViewControllers");
//        registry.addViewController("/").setViewName("home/login");// 展示默认首页
        super.addViewControllers(registry);
    }

    /**
     * 静态资源处理
     **/
    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        log.info("MyTdWebAppConfigurer.addResourceHandlers");
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
        registry.addResourceHandler("/templates/**").addResourceLocations("classpath:/templates/");
    }

    /**
     * 默认静态资源处理器
     **/
    protected void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        log.info("MyTdWebAppConfigurer.configureDefaultServletHandling");
        // super.configureDefaultServletHandling(configurer);
        // configurer.enable("stati");
        configurer.enable();
    }

}

