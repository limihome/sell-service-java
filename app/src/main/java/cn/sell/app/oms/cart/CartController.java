package cn.sell.app.oms.cart;

import cn.sell.comm.model.qo.PageQO;
import cn.sell.comm.model.vo.PageVO;
import cn.sell.comm.result.annotation.ResponseResult;
import cn.sell.mbg.dms.advertise.SmsHomeAdvertise;
import cn.sell.mbg.dms.advertise.service.SmsHomeAdvertiseService;
import cn.sell.mbg.oms.cart.OmsCartItem;
import cn.sell.mbg.oms.cart.service.OmsCartItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Description: 轮播图管理控制台
 * @Date: 2020/3/21 20:40
 * @Version: 1.0
 **/

@Log4j2
@RestController
@ResponseResult // 正常情况下统一返回格式需要增加这个注解，错误情况架构会自动捕捉异常
@RequestMapping("/rest/cart")
@Api(tags = "CartController - 购物车")
public class CartController {

    @Autowired
    private OmsCartItemService omsCartItemService;

    @ApiOperation("购物车查询")
    @GetMapping("/{userId}/list")
    public List<OmsCartItem> list(
            @PathVariable("userId") Long userId) {
        OmsCartItem omsCartItem = new OmsCartItem();
        omsCartItem.setUserId(userId);
        return omsCartItemService.selectByEntity(omsCartItem);
    }

    @ApiOperation("购物车查询")
    @GetMapping("/{userId}/add")
    public List<OmsCartItem> add(
            @PathVariable("userId") Long userId) {
        OmsCartItem omsCartItem = new OmsCartItem();
        omsCartItem.setUserId(userId);
        return omsCartItemService.selectByEntity(omsCartItem);
    }

}
    