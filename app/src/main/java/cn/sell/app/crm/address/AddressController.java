package cn.sell.app.crm.address;

import cn.sell.comm.model.qo.PageQO;
import cn.sell.comm.model.vo.PageVO;
import cn.sell.comm.result.annotation.ResponseResult;
import cn.sell.mbg.crm.address.UmsUserAddress;
import cn.sell.mbg.crm.address.service.UmsUserAddressService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Description: 地址信息控制台
 * @Date: 2020/3/21 20:40
 * @Version: 1.0
 **/

@Log4j2
@RestController
@ResponseResult // 正常情况下统一返回格式需要增加这个注解，错误情况架构会自动捕捉异常
@RequestMapping("/rest/address")
@Api(tags = "AddressController - 地址管理")
@ApiResponses({
        @ApiResponse(code = 200, message = "请求正常"),
        @ApiResponse(code = 4001, message = "系统异常")
})
public class AddressController {

    @Autowired
    private UmsUserAddressService umsUserAddressServiceImpl;

    @ApiOperation("地址管理：支持条件、分页、排序查询，不支持模糊查询")
    @PostMapping("/list")
    public List<UmsUserAddress> list(@RequestBody UmsUserAddress userAddress) {
        Assert.notNull(userAddress.getUserId(), "userId is not null [ 会员编号不能为空 ]");
        return umsUserAddressServiceImpl.selectByEntity(userAddress);
    }

    @ApiOperation("地址管理：新增")
    @PostMapping("/add")
    public Long add(@RequestBody UmsUserAddress userAddress) throws Exception {
        Assert.notNull(userAddress.getPhoneNumber(), "phoneNumber is not null [ 手机号不能为空 ]");
        Assert.notNull(userAddress.getName(), "name is not null [ 收货人名称不能为空 ]");
        Assert.notNull(userAddress.getUserId(), "userId is not null [ 会员编号不能为空 ]");
        Assert.notNull(userAddress.getDetailAddress(), "detailAddress is not null [ 详细地址不能为空 ]");
        return umsUserAddressServiceImpl.insert(userAddress);
    }

    @ApiOperation("地址管理：修改")
    @PostMapping("/update")
    public int update(@RequestBody UmsUserAddress userAddress) {
        Assert.notNull(userAddress.getId(), "id is not null [ 主键不能为空 ]");
        Assert.notNull(userAddress.getPhoneNumber(), "phoneNumber is not null [ 手机号不能为空 ]");
        Assert.notNull(userAddress.getName(), "name is not null [ 收货人名称不能为空 ]");
        Assert.notNull(userAddress.getUserId(), "userId is not null [ 会员编号不能为空 ]");
        Assert.notNull(userAddress.getDetailAddress(), "detailAddress is not null [ 详细地址不能为空 ]");
        return umsUserAddressServiceImpl.updateByPk(userAddress.getId(), userAddress);
    }

    @ApiOperation("地址管理：删除")
    @GetMapping("/delete/{id}")
    public int delete(@PathVariable("id") Long id) throws Exception {
        Assert.notNull(id, "id is not null [ 主键不能为空 ]");
        UmsUserAddress umsUserAddress = umsUserAddressServiceImpl.selectByPk(id);
        if(umsUserAddress.getDefaultStatus().equals(1)){
            throw new Exception("默认地址无法删除！请先选择其他地址为默认地址！！！");
        }
        return umsUserAddressServiceImpl.deleteByPk(id);
    }

}
