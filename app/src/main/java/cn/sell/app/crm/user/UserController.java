package cn.sell.app.crm.user;

import cn.sell.comm.result.annotation.ResponseResult;
import cn.sell.mbg.crm.user.UmsUser;
import cn.sell.mbg.crm.user.service.UmsUserService;
import cn.sell.mbg.crm.user.view.UserInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName:CategoryController
 * @Description: TODO: 商品分类，
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Date: 2020/3/23 11:52
 * @Version: 1.0
 **/
@Log4j2
@RestController
@ResponseResult // 正常情况下统一返回格式需要增加这个注解，错误情况架构会自动捕捉异常
@RequestMapping("/rest/user")
@Api(tags = "UserController - 用户信息")
@ApiResponses({
        @ApiResponse(code = 200, message = "请求正常"),
        @ApiResponse(code = 4001, message = "系统异常")
})
public class UserController {

    @Autowired
    private UmsUserService umsUserServiceImpl;

    @ApiOperation("app用户登录")
    @PostMapping("/login")
    public UmsUser login(
            @RequestBody UmsUser umsUser) throws Exception {
        return umsUserServiceImpl.login(umsUser);
    }

    @ApiOperation("app用户信息查询")
    @GetMapping("/userInfo/{userId}")
    public UserInfo userInfo(
            @PathVariable("userId") Long userId) {
        return umsUserServiceImpl.userInfo(userId);
    }

}
