package cn.sell.app.dms.advertise;

import cn.sell.comm.model.qo.PageQO;
import cn.sell.comm.model.vo.PageVO;
import cn.sell.comm.result.annotation.ResponseResult;
import cn.sell.mbg.dms.advertise.SmsHomeAdvertise;
import cn.sell.mbg.dms.advertise.service.SmsHomeAdvertiseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Description: 轮播图管理控制台
 * @Date: 2020/3/21 20:40
 * @Version: 1.0
 **/

@Log4j2
@RestController
@ResponseResult // 正常情况下统一返回格式需要增加这个注解，错误情况架构会自动捕捉异常
@RequestMapping("/rest/advertise")
@Api(tags = "AdvertiseController - 轮播图")
public class AdvertiseController {

    @Autowired
    private SmsHomeAdvertiseService smsHomeAdvertiseServiceImpl;

    @ApiOperation("轮播图管理：支持条件、分页、排序查询，不支持模糊查询")
    @GetMapping("/info")
    public PageVO<SmsHomeAdvertise> list(
            @RequestParam(value = "type") Integer type,
            @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        SmsHomeAdvertise smsHomeAdvertise = new SmsHomeAdvertise();
        smsHomeAdvertise.setType(type);
        PageQO pageQO = new PageQO(pageNum, pageSize);
        pageQO.setCondition(smsHomeAdvertise);
        return smsHomeAdvertiseServiceImpl.selectPage(pageQO);
    }

}
    