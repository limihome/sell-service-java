package cn.sell.app.pms.category;

import cn.sell.comm.model.qo.PageQO;
import cn.sell.comm.model.vo.PageVO;
import cn.sell.comm.result.annotation.ResponseResult;
import cn.sell.mbg.pms.category.PmsProductCategory;
import cn.sell.mbg.pms.category.service.PmsProductCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClassName:CategoryController
 * @Description: TODO: 商品分类，
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Date: 2020/3/23 11:52
 * @Version: 1.0
 **/
@Log4j2
@RestController
@ResponseResult // 正常情况下统一返回格式需要增加这个注解，错误情况架构会自动捕捉异常
@RequestMapping("/rest/product/category")
@Api(tags = "CategoryController - 商品分类")
@ApiResponses({
        @ApiResponse(code = 200, message = "请求正常"),
        @ApiResponse(code = 4001, message = "系统异常")
})
public class CategoryController {

    @Autowired
    private PmsProductCategoryService productCategoryServiceImpl;

    @ApiOperation("查询商品分类数据，支持条件、分页查询，不支持模糊查询")
    @PostMapping("/list")
    public PageVO<PmsProductCategory> list(@RequestBody PmsProductCategory productCategory,
                                @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) throws Exception {
        PageQO pageQO = new PageQO(pageNum, pageSize);
        pageQO.setCondition(productCategory);
        return productCategoryServiceImpl.selectPage(pageQO);
    }

    @ApiOperation("查询商品分类数据：返回tree")
    @GetMapping("/tree")
    public List<PmsProductCategory> toTree() throws Exception {
        return productCategoryServiceImpl.toTree();
    }

    @ApiOperation("查询商品分类数据：导航栏显示")
    @GetMapping("/navList")
    public List<PmsProductCategory> navList() throws Exception {
        return productCategoryServiceImpl.toTree();
    }
}
