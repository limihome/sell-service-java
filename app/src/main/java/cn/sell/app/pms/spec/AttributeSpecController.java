package cn.sell.app.pms.spec;

import cn.sell.comm.model.qo.PageQO;
import cn.sell.comm.model.vo.PageVO;
import cn.sell.comm.result.annotation.ResponseResult;
import cn.sell.mbg.pms.attribute.ProductAttributeSpec;
import cn.sell.mbg.pms.attribute.service.AttributeSpecService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName:AttributeCategory
 * @Description: 商品规格
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Date: 2020/3/23 15:14
 * @Version: 1.0
 **/

@Log4j2
@RestController
@ResponseResult // 正常情况下统一返回格式需要增加这个注解，错误情况架构会自动捕捉异常
@RequestMapping("/rest/attribute/spec")
@Api(tags = "attributeController - 商品规格")
@ApiResponses({
        @ApiResponse(code = 200, message = "请求正常"),
        @ApiResponse(code = 4001, message = "系统异常")
})
public class AttributeSpecController {

    @Autowired
    private AttributeSpecService attributeSpecServiceImpl;

    @ApiOperation("查询商品规格：支持条件、分页查询，不支持模糊查询")
    @PostMapping("/list")
    public PageVO<ProductAttributeSpec> list(@RequestBody ProductAttributeSpec attributeSpec,
                                             @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                             @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) throws Exception {
        PageQO pageQO = new PageQO(pageNum, pageSize);
        pageQO.setCondition(attributeSpec);
        return attributeSpecServiceImpl.selectPage(pageQO);
    }

}
