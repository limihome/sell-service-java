package cn.sell.app.pms.product;

import cn.sell.comm.model.qo.PageQO;
import cn.sell.comm.model.vo.PageVO;
import cn.sell.comm.result.annotation.ResponseResult;
import cn.sell.mbg.pms.product.PmsProduct;
import cn.sell.mbg.pms.product.service.PmsProductService;
import cn.sell.mbg.pms.product.view.PmsProductParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author: Limi
 * @Contact : qq(2393839633),Email(13924223985@163.com)
 * @Description: TODO: 商品管理
 * @Date: 2020/3/23 9:39
 * @Version: 1.0
 **/

@Log4j2
@RestController
@ResponseResult // 正常情况下统一返回格式需要增加这个注解，错误情况架构会自动捕捉异常
@RequestMapping("/rest/product")
@Api(tags = "ProductController - 商品信息")
@ApiResponses({
        @ApiResponse(code = 200, message = "请求正常"),
        @ApiResponse(code = 4001, message = "系统异常")
})
public class ProductController {

    @Autowired
    private PmsProductService productService;

    @ApiOperation("查询商品表数据，支持条件、分页查询，不支持模糊查询")
    @GetMapping("/list")
    public PageVO<PmsProduct> list(
            @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) throws Exception {
        PageQO pageQO = new PageQO(pageNum, pageSize);
        PageVO<PmsProduct> pmsProductPageVO = productService.selectPage(pageQO);
        return pmsProductPageVO;
    }

    @ApiOperation("商品表数据:获取商品")
    @GetMapping("/detail/{id}")
    public PmsProductParam getProductDetailByIdResultByApp(@PathVariable("id") Long id) throws Exception {
        return productService.getProductById(id);
    }

//    @ApiOperation("商品信息：商品详情")
//    @GetMapping("/detail/{id}") await
//    public PmsProductParam getProductById(@PathVariable("id") Long id) throws Exception {
//        return productService.getProductById(id);
//    }

}
