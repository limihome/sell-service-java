package cn.sell.comm.model.po;

import cn.sell.comm.model.Model;

/**
 * @Author Limi
 * @Description //TODO 树状结构的抽象对象
 * @Date 10:00 2019/8/13
 * @Param 
 * @return 
 **/
public interface TreePO<T extends TreePO> extends Model {

    String getParentNodeId(); //父级id

    void setParentNodeId(String pid);

    String getNodeId(); // 获取节点中保存的信息

    void setNodeId(String nodeId);

}
