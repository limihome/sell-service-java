分析讲解
上面就是我们利用pageHelper实现的一个用户的分页功能啦（selectAll方法是使用通用mapper的），
调用方法的结果在上面实现目标中已经展示出来啦，是不是还算蛮简单呢，不需要写任何的mapper类和mapper.xml就可以实现了，
不过你可能有点看不太懂了，下面我已我自己的使用前的疑问依依解答一下：


① 该分页方式是内存分页还是物理分页？
答：pagehelper分页是属于物理分页的，不是内存分页，所以不用担心数据量过大导致的性能问题。


② userMapper.selectAll()这个方法什么都没有返回，为什么直接使用page就可以得到结果呢？
答：其实你在调用PageHelper.startPage静态方法的时候，PageHelper会把每一次调用的page参数放入ThreadLocal变量中，
因为Threadlocal本身就是线程安全的，所以在多线程环境下，各个Threadlocal之间相互隔离，不同thread可以使用不同的数据源或执行不同的SQL语句，
PageHelper利用这一点通过拦截器获取到同一线程中的预编译好的SQL语句之后将SQL语句包装成具有分页功能的SQL语句，并将其再次赋值给下一步操作，
所以实际执行的SQL语句就是有了分页功能的SQL语句，执行成功后，数据结果会被写回Threadlocal的page变量中。


③ 这种PageHelper.startPage 静态方法的方式，使用的规则或者说我们要怎么具体的使用它呢？
答：使用起来很简单，你只需在你需要进行分页的 MyBatis 查询方法前调用 PageHelper.startPage 静态方法即可，紧跟在这个方法后的第一个MyBatis 查询方法会被进行分页。


④ 使用这种分页方式需要注意什么呢？
答： 使用了PageHelper.startPage方法后，一定要跟一个查询数据库的操作，否则会导致不安全的分页，所谓的不安全分页，比如说在你的方法中使用了该startPage静态方法，
但是后面却没有查询数据库操作，就会导致 PageHelper 生产了一个分页参数，但是没有被消费，这个参数就会一直保留在这个线程上。当这个线程再次被使用时，
就可能导致不该分页的方法去消费这个分页参数，这就产生了莫名其妙的分页。


课外扩展

学过了通用mapper和page-helper的使用后，我们其实可以直接写出一个相对比较通用的分页方法，下面给出代码，留给同学们课后回味和研究下它的好用之处吧O(∩_∩)O~

    public PageVO<E> selectPage(PageQO<?> pageQO) {
        Assert.notNull(pageQO, "pageQO is not null");

        Page<E> page = PageHelper.startPage(pageQO.getPageNum(), pageQO.getPageSize(), pageQO.getOrderBy());
        try {
            Object condition = pageQO.getCondition();
            if (condition == null) {
                crudMapper.selectAll();
            } else if (condition instanceof Condition) {
                crudMapper.selectByCondition(condition);
            } else if (condition instanceof Example) {
                crudMapper.selectByExample(condition);
            } else if (poType.isInstance(condition)){
                crudMapper.select((E)condition);
            } else {
                try {
                    E e = poType.newInstance();
                    BeanUtil.copyProperties(condition, e);
                    crudMapper.select(e);
                } catch (InstantiationException | IllegalAccessException e) {
                    log.error("selectPage occurs error, caused by: ", e);
                    throw new RuntimeException("poType.newInstance occurs InstantiationException or IllegalAccessException", e);
                }
            }
        } finally {
            page.close();
        }

        return PageVO.build(page);
    }