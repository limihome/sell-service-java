package cn.sell.comm.model.bo;

/**
 * @ClasssName: Node
 * @description: TODO
 * @Auctior: limi
 * @Date: 2019/8/159:54
 **/

import cn.sell.comm.model.po.TreePO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Node<T extends TreePO<TreePO>>{
    public T node;
    public List<Node> childrenNode;
}
