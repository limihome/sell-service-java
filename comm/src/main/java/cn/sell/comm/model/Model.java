package cn.sell.comm.model;

import java.io.Serializable;

/**
 * 实体类超类
 * 所有实体类都要继承这个类
 */
//public interface Model{
public interface Model extends Serializable {
}