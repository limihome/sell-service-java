package cn.sell.comm.model.qo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.hibernate.validator.constraints.Range;
import org.springframework.util.Assert;
import tk.mybatis.mapper.entity.Example;

import java.lang.reflect.Field;
import java.lang.reflect.Method;


/**
 * @author zhuamer
 * @desc 分页查询对象
 * @since 7/6/2017 2:48 PM
 */
@Data
@Builder
@ApiModel("分页查询对象")
@AllArgsConstructor

public class PageQO<T> {

    /**
     * 按创建时间倒序排序
     */
    public static final String ORDER_BY_CREATE_TIME_DESC = "create_time desc";

    @ApiModelProperty(value = "当前页")
    @Range(min = 1, max = Integer.MAX_VALUE)
    private int pageNum = 1;

    @ApiModelProperty(value = "页面长度")
    @Range(min = 1, max = Integer.MAX_VALUE)
    private int pageSize = 10;

    @ApiModelProperty(value = "排序", notes = "例：create_time desc,update_time desc")
    private String orderBy;

    /**
     * 可以是Condition、Example
     */
    private T condition;

    public PageQO(int pageNum, int pageSize) {
        super();
    	Assert.notNull(pageNum, "pageNum is not null；[当前页不能为空]");
    	Assert.notNull(pageSize, "pageSize is not null；[页面长度不能为空]");
        this.pageNum = pageNum;
        this.pageSize = pageSize;
    }

    /**
     * @Author Limi
     * @Description //TODO 获取下一页开始
     * @Date 12:08 2019/7/31
     * @Param []
     * @return int
     **/
    public int getOffset() {
        return (this.pageNum - 1) * this.pageSize;
    }

        // criteria.andEqualTo("xm", "崔颖");//条件相等
        // criteria.andGreaterThan("xb", "1");//大于
        // criteria.andLessThan("xb", "2");//小于
        // criteria.andIsNotNull("xm");// is not null
        // criteria.andCondition("xzdqh=","110104");//加各种条件都可以 = like <,可以代替全部的
        // List<String> values=new ArrayList<String>();
        // values.add("110104");
        // values.add("440304");
        // criteria.andIn("xzdqh", values);// in()函数
        // criteria.andBetween("csrq", "1956/01/08", "1966/10/21"); //时间相隔
        // Example.Criteria criteria2 = example.createCriteria();
        // criteria2.andCondition("xzdqh=","220104");
        // example.or().andCondition("xzdqh=","220104");//or
        // example.or(criteria2);//or


	public Example getExample(T emp) {
		Example example = new Example(emp.getClass());
		Example.Criteria criteria = example.createCriteria();

		Class<? extends Object> clazz = emp.getClass();
		Field[] declaredFields = clazz.getDeclaredFields();
		try {
			for (Field f : declaredFields) {
				if (!"serialVersionUID".equals(f.getName())) { // 属性如果是serialVersionUID 就跳过
//					Method getMethodName = getGetMethodName(f.getName(), methods, clazz);
					f.setAccessible(true); // 设置些属性是可以访问的
					Object val = f.get(emp);// 得到此属性的值
					if (null != val) {
						criteria.andLike("pName", "%" + f.getName() + "%");
					}
				}
			}
		} catch (IllegalAccessException | IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}

//		// 模糊查询
//		if (sysHospitalizationQuery.getpName() != "") {
//			criteria.andLike("pName", "%" + sysHospitalizationQuery.getpName() + "%");
//		}
//		// 相等查询
//		if (sysHospitalizationQuery.gethId() != "") {
//			criteria.andEqualTo("hId", sysHospitalizationQuery.gethId());
//		}
//		if (sysHospitalizationQuery.gethArea() != 0) {
//			criteria.andEqualTo("hArea", sysHospitalizationQuery.gethArea());
//			;
//		}
//		// 筛选时间--在某个时间段内
//		if (sysHospitalizationQuery.gethDate() != "") {
//			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//			Date dateBegin = null;
//			Date dateEnd = null;
//			try {
//				dateBegin = sdf.parse(sysHospitalizationQuery.gethDate());
//				dateEnd = sdf.parse(sysHospitalizationQuery.gethDate());
//			} catch (ParseException e) {
//				e.printStackTrace();
//			}
//
//			Calendar cal = Calendar.getInstance();
//			cal.setTime(dateEnd);
//			cal.add(Calendar.DATE, 1);
//			criteria.andBetween("hDate", dateBegin, new Date());
//		}
		return example;
	}

	private Method getGetMethodName(String fieldName, Method[] methods, Class<? extends Object> clazz)
			throws NoSuchMethodException, SecurityException {
		for (Method m : methods) {
			if (("get" + fieldName).toLowerCase().equals(m.getName().toLowerCase())) {
				Method method = clazz.getMethod(m.getName());
				return method;
			}
		}
		return null;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public T getCondition() {
		return condition;
	}

	public void setCondition(T condition) {
		this.condition = condition;
	}
}
