package cn.sell.comm.model.po;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Transient;

/**
 * @Author Limi
 * @Description //TODO 基础PO类,所有的实体类都需要实现这个
 * @Date 12:12 2019/7/8
 **/
@Data
public abstract class BasePO<PK> implements PojoPO<PK> {

    /**
     * @Author Limi
     * @Description // TODO
     * @Date 12:11 2019/7/8
     **/
    private static final long serialVersionUID = 1L;

    /**
     * 版本号
     */
    @Transient // 用来忽略映射字段
    @ApiModelProperty(value = "版本号")
    @Column(name = "version")
    private String version;

    /**
     * 创建人
     */
    @Transient // 用来忽略映射字段
    @ApiModelProperty(value = "创建人")
    @Column(name = "create_user")
    private String createUser;

    /**
     * 创建时间
     */
    @Transient // 用来忽略映射字段
    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private String createTime;

    /**
     * 修改人
     */
    @Transient // 用来忽略映射字段
    @ApiModelProperty(value = "修改人")
    @Column(name = "update_user")
    private String updateUser;

    /**
     * 修改时间
     */
    @Transient // 用来忽略映射字段
    @ApiModelProperty(value = "修改时间")
    @Column(name = "update_time")
    private String updateTime;
}
