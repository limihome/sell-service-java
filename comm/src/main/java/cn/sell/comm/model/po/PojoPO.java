package cn.sell.comm.model.po;

/**
 * @Author Limi
 * @Description //TODO 持久对象的抽象对象
 * @Date 10:03 2019/8/13
 * @Param 
 * @return 
 **/
public interface PojoPO<PK> extends PO<PK> {

    String getCreateUser();

    void setCreateUser(String createUser);

    String getCreateTime();

    void setCreateTime(String createTime);

    String getUpdateUser();

    void setUpdateUser(String updateUser);

    String getUpdateTime();

    void setUpdateTime(String updateTime);



    String getVersion();

    void setVersion(String version);
}
