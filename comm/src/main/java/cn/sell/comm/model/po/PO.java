package cn.sell.comm.model.po;


import cn.sell.comm.model.Model;

/**
 * 公共属性，所有表都有这个属性。
 *
 * @param <PK>
 */
public interface PO<PK> extends Model {

    PK getId();

    void setId(PK id);
}
