package cn.sell.comm.model.vo;

import cn.sell.comm.model.Model;
import cn.sell.comm.model.qo.PageQO;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.assertj.core.util.Lists;
import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @author zhuamer
 * @desc 分页VO对象
 * @since 7/6/20 17 2:48 PM
 */
@Builder
@Data
@AllArgsConstructor
public class PageVO<T> implements Model {

    private static final long serialVersionUID = -4426958360243585882L;

    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页")
    private Integer pageNum;

    /**
     * 每页的数量
     */
    @ApiModelProperty(value = "每页的数量")
    private Integer pageSize;

    /**
     * 总记录数
     */
    @ApiModelProperty(value = "总记录数")
    private Long total;

    /**
     * 总页数
     */
    @ApiModelProperty(value = "总页数")
    private Integer pages;

    /**
     * 结果集
     */
    @ApiModelProperty(value = "结果集")
    private List<T> list;

    public PageVO() {
    }

    public PageVO(@SuppressWarnings("rawtypes") PageQO pageQO) {
        this.setPageNum(pageQO.getPageNum());
        this.setPageSize(pageQO.getPageSize());
    }

    public PageVO(List<T> poList) {
        BeanUtils.copyProperties(new PageInfo<>(poList), this);
    }

    public static <T> PageVO<T> build(List<T> poList) {
        return new PageVO<>(poList);
    }

    /**
     * @param page 数据库查出来的分页数据列表
     * @desc 构建一个分页VO对象
     */
    public static <T> PageVO<T> build(Page<T> page) {
        PageVO<T> pageVO = new PageVO<>();
        BeanUtils.copyProperties(page.toPageInfo(), pageVO);
        return pageVO;
    }

    /**
     * @param page    数据库查出来的分页数据列表
     * @param voClazz 要转为的VO类
     * @desc 构建一个分页VO对象 试用场景为：从数据库取出的PO列表不做任何处理，转化为VO列表返回
     */
    public static <T, E> PageVO<T> build(Page<E> page, Class<T> voClazz) {

        PageVO<T> pageVO = new PageVO<>();
        BeanUtils.copyProperties(page, pageVO, "list");

        try {
            List<T> voList = Lists.newArrayList();
            List<E> poList = page.getResult();
            if (!CollectionUtils.isEmpty(poList)) {
                for (E e : poList) {
                    T t = voClazz.newInstance();
                    BeanUtils.copyProperties(e, t);
                    voList.add(t);
                }
            }
            pageVO.setList(voList);
        } catch (IllegalAccessException | InstantiationException e) {
            throw new RuntimeException(e);
        }

        return pageVO;
    }

    /**
     * @param poPage 数据库查出来的分页数据
     * @param voList vo数据列表
     * @desc 构建一个分页VO对象 试用场景为：将处理好的VO列表封装返回
     */
    public static <T, E> PageVO<T> build(Page<E> poPage, List<T> voList) {
        PageVO<T> page = new PageVO<>();
        BeanUtils.copyProperties(poPage, page, "list");
        page.setList(voList == null ? Lists.newArrayList() : voList);
        return page;
    }

    public static Integer getPages(Long total, Integer pageSize) {
        if (total == null || pageSize == null || total == 0 || pageSize == 0) {
            return 0;
        }
        return (int) (total % pageSize == 0 ? (total / pageSize) : (total / pageSize + 1));
    }

    public Integer getPages() {
        return getPages(this.total, this.pageSize);
    }

}
