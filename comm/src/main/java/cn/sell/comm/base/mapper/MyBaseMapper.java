package cn.sell.comm.base.mapper;


/**
 * @ClasssName: MyCrudMapper
 * @description: TODO:基础增删改查功能mapper
 * @Auctior: limi
 * @Date: 2019/7/30 17:42
 **/
public interface MyBaseMapper<T>
        extends
        InsertMapper<T>,
        DeleteMapper<T>,
        UpdateMapper<T>,
        SelectMapper<T> {
}
