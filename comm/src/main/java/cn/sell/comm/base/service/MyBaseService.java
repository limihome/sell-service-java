package cn.sell.comm.base.service;

import tk.mybatis.mapper.common.example.SelectByExampleMapper;

import java.util.List;

/**
 * 抽象类BaseService
 *
 * @author fuce
 * @ClassName: BaseService
 * @Description: TODO(Service实现这个)
 * @date 2018年6月3日
 */
public interface MyBaseService<E, PK> extends
        InsertService<E, PK>,
        UpdateService<E, PK>,
        DeleteService<PK>,
        SelectService<E, PK>,
        SelectByExampleMapper {
    public List<E> selectByEntity(E condition);
}
