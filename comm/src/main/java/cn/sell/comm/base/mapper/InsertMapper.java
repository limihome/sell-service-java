package cn.sell.comm.base.mapper;

import tk.mybatis.mapper.common.Marker;
import tk.mybatis.mapper.common.MySqlMapper;
import tk.mybatis.mapper.common.base.insert.InsertSelectiveMapper;

/**
 * @ClasssName: InsertMapper
 * @description: TODO:基础新增功能mapper
 * @Auctior: limi
 * @Date: 2019/7/30 17:35
 **/
public interface  InsertMapper<T> extends Marker,
        tk.mybatis.mapper.common.base.insert.InsertMapper<T>,
        InsertSelectiveMapper<T>,
        MySqlMapper<T>{
}
