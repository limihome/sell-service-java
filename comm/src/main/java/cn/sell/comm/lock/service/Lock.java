package cn.sell.comm.lock.service;


import cn.sell.comm.lock.model.EasyLockInfo;

/**
 * @author zhumaer
 * @desc 锁上层接口
 * @since 6/27/2018 11:29 PM
 */
public interface Lock {

    /**
     * 设置锁
     *
     * @param easyLockInfo
     * @return
     */
    Lock setLockInfo(EasyLockInfo easyLockInfo);

    /**
     * 获取锁
     *
     * @return
     */
    boolean acquire();

    /**
     * 释放锁
     */
    void release();
}
