package cn.sell.comm.lock.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhumaer
 * @desc 锁基本信息
 * @since 6/27/2018 11:29 PM
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EasyLockInfo {

    /**
     * 锁的类型
     */
    private LockType type;

    /**
     * 锁名称
     */
    private String name;

    /**
     * 最多等待时间
     */
    private long waitTime;

    /**
     * 上锁后自动释放锁时间
     */
    private long leaseTime;

}
