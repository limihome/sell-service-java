package cn.sell.comm.result.annotation;


import cn.sell.comm.result.PlatformResult;
import cn.sell.comm.result.support.Result;

import java.lang.annotation.*;

/**
 * @Author Limi
 * @Description //TODO 接口返回结果增强  会通过拦截器拦截后放入标记，在ResponseResultHandler 进行结果处理
 * @Date 18:00 2019/7/11
 **/
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ResponseResult {

    Class<? extends Result> value() default PlatformResult.class;

}