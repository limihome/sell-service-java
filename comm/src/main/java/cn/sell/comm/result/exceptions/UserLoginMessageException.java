package cn.sell.comm.result.exceptions;

import cn.sell.comm.result.enums.ResultCode;

/**
 * @ClasssName: UserLoginMessageException
 * @description: TODO
 * @Auctior: limi
 * @Date: 2019/8/1618:06
 **/
public class UserLoginMessageException  extends BusinessException {

    private static final long serialVersionUID = 3721036867889297081L;

    public UserLoginMessageException() {
        super();
    }

    public UserLoginMessageException(Object data) {
        super();
        super.data = data;
    }

    public UserLoginMessageException(ResultCode resultCode) {
        super(resultCode);
    }

    public UserLoginMessageException(ResultCode resultCode, Object data) {
        super(resultCode, data);
    }

    public UserLoginMessageException(String msg) {
        super(msg);
    }

    public UserLoginMessageException(String formatMsg, Object... objects) {
        super(formatMsg, objects);
    }

}
