package cn.sell.comm.result.exceptions;


import cn.sell.comm.result.enums.ResultCode;

/**
 * @Author Limi
 * @Description //TODO 数据没有找到异常
 * @Date 18:01 2019/7/11
 **/
public class UserNotLoginException extends BusinessException {

    private static final long serialVersionUID = 3721036867889297081L;

    public UserNotLoginException() {
        super();
    }

    public UserNotLoginException(Object data) {
        super();
        super.data = data;
    }

    public UserNotLoginException(ResultCode resultCode) {
        super(resultCode);
    }

    public UserNotLoginException(ResultCode resultCode, Object data) {
        super(resultCode, data);
    }

    public UserNotLoginException(String msg) {
        super(msg);
    }

    public UserNotLoginException(String formatMsg, Object... objects) {
        super(formatMsg, objects);
    }

}
