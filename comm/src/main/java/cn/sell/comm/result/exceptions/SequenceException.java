package cn.sell.comm.result.exceptions;

import cn.sell.comm.result.enums.ResultCode;

public class SequenceException extends BusinessException {

    private static final long serialVersionUID = 3721036867889297081L;

    public SequenceException() {
        super();
    }

    public SequenceException(Object data) {
        super();
        super.data = data;
    }

    public SequenceException(ResultCode resultCode) {
        super(resultCode);
    }

    public SequenceException(ResultCode resultCode, Object data) {
        super(resultCode, data);
    }

    public SequenceException(String msg) {
        super(msg);
    }

    public SequenceException(String formatMsg, Object... objects) {
        super(formatMsg, objects);
    }

}