//package cn.sell.comm.file;
//
//import com.aliyun.oss.ClientException;
//import com.aliyun.oss.OSSClient;
//import com.aliyun.oss.common.auth.InvalidCredentialsException;
//import com.aliyun.oss.model.OSSObject;
//import com.aliyun.oss.model.ObjectMetadata;
//import lombok.extern.log4j.Log4j2;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Component;
//
//import javax.imageio.stream.FileImageOutputStream;
//import java.io.*;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//@Log4j2
//@Component
//public class UploadOSS {
//
//    //	private static String ClaimImgUploadPath;
//    @Value("${HUAGUI_FILE_URL}")
//    private static String properURL;
//
//    @Value("${OSS_BUCKET_NAME}")
//    private static String OSS_BUCKET_NAME;
//
//    @Value("${oss.host.endpoint}")
//    private static String OSS_ENDPOINT;
//
//    @Value("${oss.accessKeyId}")
//    private static String ACCESS_ID;
//
//    @Value("${oss.accessKeySecret}")
//    private static String ACCESS_KEY;
//
//    @Value("${oss.bucketName}")
//    private static String DownloadLink;
//
//    /**
//     * 上传文件
//     *
//     * @param client     OSS_ENDPOINT节点
//     * @param client     ACCESS_ID对象
//     * @param client     ACCESS_KEY key
//     * @param bucketName Bucket名
//     * @param Objectkey  上传到OSS起的名constract_no
//     * @param filename   本地文件名
//     * @throws OSSException
//     * @throws ClientException
//     * @throws FileNotFoundException
//     */
//
//    /**
//     *
//     * @param prtNo
//     * @param oss_endpoint
//     * @param access_id
//     * @param access_key
//     * @param oss_bucket_name
//     * @return
//     */
//    public static List<Map<String, String>> uploadFile(
//            String prtNo,
//            String oss_endpoint,
//            String access_id,
//            String access_key,
//            String oss_bucket_name) {
//        try {
//            log.info("OSS_ENDPOINT:{}", oss_endpoint);
//            log.info("ACCESS_ID:{}", access_id);
//            log.info("ACCESS_KEY:{}", access_key);
//            OSSClient client = new OSSClient(oss_endpoint, access_id, access_key);
////			File file = new File(properURL+File.separator+filename);//转换后tif文件存放地址
//            File[] files = findAllFiles(prtNo);
//            if (null != files && files.length > 0) {
//                List<Map<String, String>> list = new ArrayList<Map<String, String>>();
//                for (File file : files) {
//                    ObjectMetadata objectMeta = new ObjectMetadata();
//                    Map<String, String> map = new HashMap<String, String>();
//                    objectMeta.setContentLength(file.length());
//                    InputStream input = new FileInputStream(file);
//                    log.info("oss_bucket_name:{}", oss_bucket_name);
//                    client.putObject(oss_bucket_name, file.getName(), input, objectMeta);
//                    log.info("upload OSS success: file.getName:{},DownloadLink:{}", file.getName(), DownloadLink);
//                    map.put(file.getName(), DownloadLink + file.getName());
//                    list.add(map);
//                }
//                return list;
//            }
//        } catch (InvalidCredentialsException e) {
//            log.info("登录失败！：" + e.getMessage());
//            e.printStackTrace();
//            return null;
//        } catch (ClientException e) {
//            log.info("连接失败！：" + e.getMessage());
//            e.printStackTrace();
//            return null;
//        } catch (Exception e) {
//            log.info("发生错误，因为：" + e.getMessage());
//            e.printStackTrace();
//            return null;
//        }
//        return null;
//    }
//
//    public static File[] findAllFiles(String prtNo) throws Exception {
//        File f = new File(prtNo);
//        if (f.isDirectory()) {
//            File[] files = f.listFiles();
//            return files;
//        }
//        return null;
//    }
//
//    /**
//     * @return void 返回类型
//     * @Title: listObjects
//     * @Description: TODO(获取服务器上文件)
//     * @author XXX
//     */
//    public static void listObjects(String filename, String filepath) {
//        String bucketName = OSS_BUCKET_NAME;
//        String endpoint = OSS_ENDPOINT;
//
//        // 初始化一个OSSClient
//        OSSClient client = new OSSClient(endpoint, ACCESS_ID, ACCESS_KEY);
//
//        // 获取Object，返回结果为OSSObject对象
//        OSSObject object = client.getObject(bucketName, filename);
//
//        // 获取Object的输入流
//        InputStream objectContent = object.getObjectContent();
//        // 处理Object
//        ByteArrayOutputStream outSteam = new ByteArrayOutputStream();
//        byte[] buffer = new byte[1024];
//        int len = -1;
//        String path = filepath + filename;
//        try {
//            while ((len = objectContent.read(buffer)) != -1) {
//                outSteam.write(buffer, 0, len);
//            }
//            byte[] imgByteArr = outSteam.toByteArray();
//
//            if (imgByteArr.length < 3 || path.equals(""))
//                return;
//            try {
//                FileImageOutputStream imageOutput = new FileImageOutputStream(new File(path));
//                imageOutput.write(imgByteArr, 0, imgByteArr.length);
//                imageOutput.close();
//                System.out.println("Make Picture success,Please find image in " + path);
//            } catch (Exception ex) {
//                System.out.println("Exception: " + ex);
//                ex.printStackTrace();
//            }
//
//        } catch (IOException e1) {
//            e1.printStackTrace();
//        }
//    }
//}
