package cn.sell.comm.util;

import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonValueProcessor;

import java.sql.Timestamp;

public class TimestampJsonValueProcessor implements JsonValueProcessor {

    public Object processArrayValue(Object value, JsonConfig jsonConfig) {
        return process(value);
    }

    public Object processObjectValue(String key, Object value, JsonConfig jsonConfig) {
        return process(value);
    }

    private Object process(Object value) {
    	return (value==null)?null:((Timestamp)value).getTime();
    }
}
