/**
 * Project Name:springboot-home
 * File Name:RequestContextHolderUtil.java
 * Package Name:cn.home.common.tools
 * Date:2019年3月28日下午6:48:33
 * Copyright (c) 2019,byx9577@qq.com All Rights Reserved.
 */

package cn.sell.comm.util;

import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @desc 应用级对象获取工具类
 *
 * @author zhumaer
 * @since 9/18/2017 3:00 PM
 */
public class RequestContextHolderUtil {

    public static HttpServletRequest getRequest() {
        return getRequestAttributes().getRequest();
    }

    public static HttpServletResponse getResponse() {
        return getRequestAttributes().getResponse();
    }

    public static HttpSession getSession() {
        return getRequest().getSession();
    }

    public static ServletRequestAttributes getRequestAttributes() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes());
    }

    public static ServletContext getServletContext() {
        return ContextLoader.getCurrentWebApplicationContext().getServletContext();
    }

}

