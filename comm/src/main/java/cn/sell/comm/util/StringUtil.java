package cn.sell.comm.util;

public class StringUtil {
	
	public static Boolean isEmpty(String param){
		if(param == null || param.isEmpty() || param.trim().isEmpty()) return true;
		else return false;
	}
	public static Boolean notEmpty(String param){
		return isEmpty(param)?false:true;
	}
	
	public static boolean isNumeric(String str){
		
		if(isEmpty(str)) return false;
		 try{
			 Double.parseDouble(str);
		 }catch(Exception e){
			 return false;
		 }
		return true;
	}

	/**
	 *  格式化字符串(替换符自己指定)
	 */
	public static String formatIfArgs(String format, String replaceOperator, Object... args) {
		if (isEmpty(format) || isEmpty(replaceOperator)) {
			return format;
		}

		format = replace(format, replaceOperator, "%s");
		return formatIfArgs(format, args);
	}

	/**
	 * 替换字符串
	 */
	public static String replace(String inString, String oldPattern, String newPattern) {
		if (notEmpty(inString) && notEmpty(oldPattern) && newPattern != null) {
			int index = inString.indexOf(oldPattern);
			if (index == -1) {
				return inString;
			} else {
				int capacity = inString.length();
				if (newPattern.length() > oldPattern.length()) {
					capacity += 16;
				}

				StringBuilder sb = new StringBuilder(capacity);
				int pos = 0;

				for(int patLen = oldPattern.length(); index >= 0; index = inString.indexOf(oldPattern, pos)) {
					sb.append(inString.substring(pos, index));
					sb.append(newPattern);
					pos = index + patLen;
				}

				sb.append(inString.substring(pos));
				return sb.toString();
			}
		} else {
			return inString;
		}
	}

	/**
	 *  格式化字符串（替换符为%s）
	 */
	public static String formatIfArgs(String format, Object... args) {
		if (isEmpty(format)) {
			return format;
		}

		return (args == null || args.length == 0)  ? String.format(format.replaceAll("%([^n])", "%%$1")) : String.format(format, args);
	}

	public static void main(String[] args){
		
		System.out.println("res: "+isEmpty(""));
	}

}
