package cn.sell.comm.util;

import org.apache.commons.codec.digest.DigestUtils;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

public class MD5Util {

	/**利用MD5进行加密
     * @param str  待加密的字符串
     * @return  加密后的字符串
     * @throws NoSuchAlgorithmException  没有这种产生消息摘要的算法
     * @throws UnsupportedEncodingException  
     */
//    public static String encrypt(String str) throws NoSuchAlgorithmException, UnsupportedEncodingException{
//        //确定计算方法
//        MessageDigest md5=MessageDigest.getInstance("MD5");
//        BASE64Encoder base64en = new BASE64Encoder();
//        //加密后的字符串
//        String newstr=base64en.encode(md5.digest(str.getBytes("utf-8")));
//        return newstr;
//    }
    
    
    public static String md5Hex(String str) throws UnsupportedEncodingException{
    	return DigestUtils.md5Hex(str.getBytes("UTF-8"));
    }
    
    public static void main(String[] args) throws Exception{
//    	System.out.println("aa:"+md5Hex("123456{agentCode = 8000574;pageParams ={pageNum = 1;pageSize = 10;pages = 0;total = 0;};status = 0;}"));
//    	System.out.println("aa:"+md5Hex("123456{agentCode = 8000574;pageParams ={pageNum = 1;pageSize = 10;pages = 0;total = 0;};status = 0;}"));
//    	System.out.println("aa:"+md5Hex("this is hkt feign token"));

    	System.out.println("aa:"+md5Hex("123456{\"requestUerId\":\"58271\",\"productCode\":\"HT005\",\"requestType\":\"1\",\"requestService\":\"huaTai.underwrite\",\"requestObject\":{\"productCode\":\"HT005\",\"orderInfo\":{\"customerId\":\"58271\",\"productChannel\":\"01\",\"productCode\":\"HT005\",\"num\":\"100\"},\"riskList\":[{\"riskCode\":\"HT005\",\"riskName\":\"企业安心雇主责任险\",\"prem\":\"22750\",\"mult\":\"100\",\"payIntv\":\"0\",\"payYears\":\"\",\"years\":\"1Y\",\"isMainRisk\":\"1\",\"mainRiskCode\":\"HT005\",\"getStartDate\":\"2019-04-18\",\"getEndDate\":\"2020-04-17\",\"extraInfo\":{\"planMult1\":50,\"planPrem1\":170,\"planMult2\":50,\"planPrem2\":285,\"planMult3\":0,\"planPrem3\":400,\"planMult4\":0,\"planPrem4\":515,\"planMult5\":0,\"planPrem5\":625}}],\"applyInfo\":{\"name\":\"华康\",\"addressNo\":\"华康地址\",\"addressFull\":\"华康地址\",\"idType\":\"18\",\"idNo\":\"123456\"},\"insuredList\":[{\"relationToAppnt\":\"22\",\"name\":\"华康\",\"addressNo\":\"华康地址\",\"addressFull\":\"华康地址\",\"idType\":\"18\",\"idNo\":\"123456\",\"occupationType\":\"4\",\"occupationCode\":\"A1111\",\"occupationName\":\"稻谷种植\"}],\"bnfList\":[{\"relationToInsured\":\"00\"}],\"impartList\":[],\"paymentInfo\":{\"payAmount\":\"22750\"},\"extraInfo\":{\"appntNum\":100,\"isFull\":1,\"tip2_1\":1,\"tip2_3\":1,\"tip2_4\":1,\"bill\":1,\"tip2_2_1\":1,\"tip2_2_2\":2,\"tip2_2_3\":0,\"tip3\":4,\"occName\":\"保险\",\"channelName\":\"华康保险代理有限公司\",\"socialCode\":\"123456\"}}}"));



//    	org.apache.commons.codec.digest.DigestUtils.md5Hex(String data)

    }
}
