/**
 * Project Name:springboot-home
 * File Name:SpringContextHolder.java
 * Package Name:cn.home.common.tools
 * Date:2019年3月28日下午5:37:06
 * Copyright (c) 2019,byx9577@qq.com All Rights Reserved.
 * <p>
 * <p>
 * Project Name:springboot-home
 * File Name:SpringContextHolder.java
 * Package Name:cn.home.common.tools
 * Date:2019年3月28日下午5:37:06
 * Copyright (c) 2019, chenzhou1025@126.com All Rights Reserved.
 */
/**
 * Project Name:springboot-home 
 * File Name:SpringContextHolder.java 
 * Package Name:cn.home.common.tools 
 * Date:2019年3月28日下午5:37:06 
 * Copyright (c) 2019, chenzhou1025@126.com All Rights Reserved. 
 *
 */

package cn.sell.comm.util;
/**
 * ClassName: SpringContextHolder <br/> 
 * Function: TODO 简单描述作用：. <br/> 
 * Reason: TODO ADD REASON(可选). <br/> 
 * date: 2019年3月28日 下午5:37:06 <br/> 
 *
 * @author limi
 * @version
 * @since JDK 1.8 
 */

import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * 以静态变量保存Spring ApplicationContext, 可在任何代码任何地方任何时候取出ApplicaitonContext.
 *
 */
@Service
@Lazy(false)
public class SpringContextHolder implements ApplicationContextAware, DisposableBean {

    private static ApplicationContext applicationContext = null;


    /**
     * 取得存储在静态变量中的ApplicationContext.
     */
    public static ApplicationContext getApplicationContext() {
        assertContextInjected();
        return applicationContext;
    }

    /**
     * 从静态变量applicationContext中取得Bean, 自动转型为所赋值对象的类型.
     */
    @SuppressWarnings("unchecked")
    public static <T> T getBean(String name) {
        assertContextInjected();
        return (T) applicationContext.getBean(name);
    }

    /**
     * 从静态变量applicationContext中取得Bean, 自动转型为所赋值对象的类型.
     */
    public static <T> T getBean(Class<T> requiredType) {
        assertContextInjected();
        return applicationContext.getBean(requiredType);
    }

    /**
     * 清除SpringContextHolder中的ApplicationContext为Null.
     */
    public static void clearHolder() {
        applicationContext = null;
    }

    /**
     * 实现ApplicationContextAware接口, 注入Context到静态变量中.
     */
    @Override
    public void setApplicationContext(ApplicationContext appContext) {
        applicationContext = appContext;
    }

    /**
     * 实现DisposableBean接口, 在Context关闭时清理静态变量.
     */
    @Override
    public void destroy() throws Exception {
        SpringContextHolder.clearHolder();
    }

    /**
     * 检查ApplicationContext不为空.
     */
    private static void assertContextInjected() {
        Validate.validState(applicationContext != null, "applicaitonContext属性未注入, 请在applicationContext.xml中定义SpringContextHolder.");
    }
}
