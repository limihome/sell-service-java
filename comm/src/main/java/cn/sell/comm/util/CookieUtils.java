/**
 * Project Name:springboot-home
 * File Name:CookieUtils.java
 * Package Name:cn.home.common.util
 * Date:2019年3月28日下午5:33:20
 * Copyright (c) 2019,byx9577@qq.com All Rights Reserved.
 * <p>
 * <p>
 * Project Name:springboot-home
 * File Name:CookieUtils.java
 * Package Name:cn.home.common.util
 * Date:2019年3月28日下午5:33:20
 * Copyright (c) 2019, chenzhou1025@126.com All Rights Reserved.
 * <p>
 * Project Name:springboot-home
 * File Name:CookieUtils.java
 * Package Name:cn.home.common.util
 * Date:2019年3月28日下午5:33:20
 * Copyright (c) 2019, chenzhou1025@126.com All Rights Reserved.
 * <p>
 * Project Name:springboot-home
 * File Name:CookieUtils.java
 * Package Name:cn.home.common.util
 * Date:2019年3月28日下午5:33:20
 * Copyright (c) 2019, chenzhou1025@126.com All Rights Reserved.
 * <p>
 * Project Name:springboot-home
 * File Name:CookieUtils.java
 * Package Name:cn.home.common.util
 * Date:2019年3月28日下午5:33:20
 * Copyright (c) 2019, chenzhou1025@126.com All Rights Reserved.
 */
/**
 * Project Name:springboot-home 
 * File Name:CookieUtils.java 
 * Package Name:cn.home.common.util 
 * Date:2019年3月28日下午5:33:20 
 * Copyright (c) 2019, chenzhou1025@126.com All Rights Reserved. 
 *
 */

package cn.sell.comm.util;

/**
 * ClassName: CookieUtils <br/> 
 * Function: TODO 简单描述作用：Cookie工具类. <br/> 
 * Reason: TODO ADD REASON(可选). <br/> 
 * date: 2019年3月28日 下午5:33:20 <br/> 
 *
 * @author limi
 * @version
 * @since JDK 1.8 
 */

import org.apache.commons.lang.StringUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;


/**
 * Cookie工具类
 *
 */
public class CookieUtils {

//	protected static final String ROOT_PATH = "/td";

    protected static final int COOK_DATE_DAY = 60 * 60 * 48;

    protected static final String DEFAULT_ENCODER = "UTF-8";

    /**
     *
     * setCookie:设置 Cookie（生成时间为1天） <br/>
     *
     * @author limi
     * @param response
     * @param name     参数名称
     * @param value    参数值
     * @since JDK 1.8
     */
    public static void setCookie(HttpServletResponse response, String name, String value) {
        setCookie(response, name, value, COOK_DATE_DAY);
    }

    /**
     * 设置 Cookie
     *
     * @param name   名称
     * @param value  值
     * @param maxAge 生存时间（单位秒）
     */
    public static void setCookie(HttpServletResponse response, String name, String value, int maxAge) {
        Cookie cookie = new Cookie(name, null);
        if (StringUtils.isNotBlank(SpringContextHolder.getApplicationContext().getApplicationName())) {
            cookie.setPath(SpringContextHolder.getApplicationContext().getApplicationName());
        } else {
            cookie.setPath("/");
        }
        cookie.setMaxAge(maxAge);
        try {
            cookie.setValue(URLEncoder.encode(value, DEFAULT_ENCODER));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        response.addCookie(cookie);
    }

    /**
     * 获得指定Cookie的值
     *
     * @param name 名称
     * @return 值
     */
    public static String getCookie(HttpServletRequest request, String name) {
        return getCookie(request, null, name, false);
    }

    /**
     * 获得指定Cookie的值，并删除。
     *
     * @param name 名称
     * @return 值
     */
    public static String getCookie(HttpServletRequest request, HttpServletResponse response, String name) {
        return getCookie(request, response, name, true);
    }

    /**
     * 获得指定Cookie的值
     *
     * @param request  请求对象
     * @param response 响应对象
     * @param name     名字
     * @param isRemove 是否移除
     * @return 值
     */
    public static String getCookie(HttpServletRequest request, HttpServletResponse response, String name,
                                   boolean isRemove) {
        String value = null;
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(name)) {
                    try {
                        value = URLDecoder.decode(cookie.getValue(), DEFAULT_ENCODER);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    if (isRemove) {
                        cookie.setMaxAge(0);
                        response.addCookie(cookie);
                    }
                }
            }
        }
        return value;
    }
}
