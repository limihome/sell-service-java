package cn.sell.comm.util;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;

import java.sql.Timestamp;
import java.util.*;

/**
 * JSON转换工具类
 *
 * @author wujian
 * @date 2017-05-10
 */
public class JsonUtil {
  private static JsonConfig jsonConfig = new JsonConfig();
  public static final String Default_DATE_PATTERN = "yyyy-MM-dd HH:mm:ss";
	static{
    	jsonConfig.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
    	jsonConfig.registerJsonValueProcessor(Date.class, new DateJsonValueProcessor(Default_DATE_PATTERN));
    	jsonConfig.registerJsonValueProcessor(Timestamp.class, new TimestampJsonValueProcessor());
	}
    /**
     * 对象转换成JSON字符串
     *
     * @param obj
     *            需要转换的对象
     * @return 对象的string字符
     */
    public static String toJson(Object obj) {
    	if(obj instanceof List || obj instanceof Set){
        	JSONArray json = JSONArray.fromObject(obj, jsonConfig);
        	return json.toString();
    	}else{
    		JSONObject jSONObject = JSONObject.fromObject(obj,jsonConfig);
    		return jSONObject.toString();
    	}
    }

    /**
     * 对象转换成JSON字符串
     *
     * @param obj
     *            需要转换的对象
     * @return 对象的string字符
     */
    public static String toJson(Object obj,JsonConfig config) {
    	if(obj instanceof List || obj instanceof Set){
    		JSONArray json = JSONArray.fromObject(obj, config);
    		return json.toString();
    	}else{
    		JSONObject jSONObject = JSONObject.fromObject(obj,config);
    		return jSONObject.toString();
    	}
    }

    /**
     * JSON字符串转换成对象
     *
     * @param jsonString
     *            需要转换的字符串
     * @param type
     *            需要转换的对象类型
     * @return 对象
     */
    @SuppressWarnings("unchecked")
    public static <T> T fromJson(String jsonString, Class<T> type) {
        JSONObject jsonObject = JSONObject.fromObject(jsonString);
        return (T) JSONObject.toBean(jsonObject, type);
    }

    /**
     * JSON字符串转换成对象
     *
     * @param obj
     *            需要转换的JsonObject
     * @param type
     *            需要转换的对象类型
     * @return 对象
     */
    @SuppressWarnings("unchecked")
    public static <T> T fromJsonObject(JSONObject obj, Class<T> type) {
    	return (T) JSONObject.toBean(obj, type);
    }

    /**
     * 将JSONArray对象转换成list集合
     *
     * @param jsonArr
     * @return
     */
    @SuppressWarnings("unchecked")
	public static  <T> List<T> jsonToList(JSONArray jsonArr, Class<T> type) {
        List<T> list = new ArrayList<T>();
        for (Object obj : jsonArr) {
        	JSONObject tmp = (JSONObject) obj;
        	list.add((T)JSONObject.toBean(tmp, type));
        }
        return list;
    }
    /**
     * 将JSONArray对象转换成list集合
     *
     * @param jsonArr
     * @return
     */
    public static List<Object> jsonToList(JSONArray jsonArr) {
    	List<Object> list = new ArrayList<Object>();
    	for (Object obj : jsonArr) {
    		if (obj instanceof JSONArray) {
    			list.add(jsonToList((JSONArray) obj));
    		} else if (obj instanceof JSONObject) {
    			list.add(jsonToMap((JSONObject) obj));
    		} else {
    			list.add(obj);
    		}
    	}
    	return list;
    }

    /**
     * 将json字符串转换成map对象
     *
     * @param json
     * @return
     */
    public static Map<String, Object> jsonToMap(String json) {
        JSONObject obj = JSONObject.fromObject(json);
        return jsonToMap(obj);
    }

    /**
     * 将JSONObject转换成map对象
     *
     * @param json
     * @return
     */
    public static Map<String, Object> jsonToMap(JSONObject obj) {
        Set<?> set = obj.keySet();
        Map<String, Object> map = new HashMap<String, Object>(set.size());
        for (Object key : obj.keySet()) {
            Object value = obj.get(key);
            if (value instanceof JSONArray) {
                map.put(key.toString(), jsonToList((JSONArray) value));
            } else if (value instanceof JSONObject) {
                map.put(key.toString(), jsonToMap((JSONObject) value));
            } else {
                map.put(key.toString(), obj.get(key));
            }

        }
        return map;
    }

    public static void main(String[] args) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("time", new Date());
        map.put("name", "yy");
        map.put("age", 20);
        map.put("timestamp", new Timestamp(System.currentTimeMillis()));
        System.out.println(toJson(map));
    }
}
