package cn.sell.comm.util;

/**
 * @ClasssName: Sequence
 * @description: TODO:主键生成，并不是我想要的。
 * https://www.cnblogs.com/coderzl/archive/2017/09/07/7489886.html
 * @Auctior: limi
 * @Date: 2019/8/3016:15
 **/

import org.springframework.stereotype.Component;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Sequence {
    private static final long ONE_STEP = 10;
    private static final Lock LOCK = new ReentrantLock();
    private static long lastTime = System.currentTimeMillis();
    private static short lastCount = 0;
    private static int count = 0;

    @SuppressWarnings("finally")
    public static String nextId() {
        LOCK.lock();
        try {
            if (lastCount == ONE_STEP) {
                boolean done = false;
                while (!done) {
                    long now = System.currentTimeMillis();
                    if (now == lastTime) {
                        try {
                            Thread.currentThread();
                            Thread.sleep(1);
                        } catch (InterruptedException e) {
                        }
                        continue;
                    } else {
                        lastTime = now;
                        lastCount = 0;
                        done = true;
                    }
                }
            }
            count = lastCount++;
        } finally {
            LOCK.unlock();
            return lastTime + "" + String.format("%03d", count);
        }
    }

    //获得batchNo批次号
    @SuppressWarnings({"finally"})
    public static String getBatchNo() {
        LOCK.lock();
        try {
            if (lastCount == ONE_STEP) {
                boolean done = false;
                while (!done) {
                    long now = System.currentTimeMillis();
                    if (now == lastTime) {
                        try {
                            Thread.currentThread();
                            Thread.sleep(1);
                        } catch (InterruptedException e) {
                        }
                        continue;
                    } else {
                        lastTime = now;
                        lastCount = 0;
                        done = true;
                    }
                }
            }
            count = lastCount++;
        } finally {
            LOCK.unlock();
            String str = lastTime + "" + String.format("%03d", count);
            String batchNo = "C" + str.substring(1);
            return batchNo;
        }
    }

    public static void main(String[] args) {
        //测试
        System.out.println(nextId());
        System.out.println(getBatchNo());

    }
}