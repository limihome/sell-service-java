package cn.sell.comm.enums.base;

import com.fasterxml.jackson.annotation.JsonValue;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;

public interface DisplayedEnum {

    String DEFAULT_CODE_NAME = "code";
    String DEFAULT_NAME_NAME = "name";
    String DEFAULT_VALUE_NAME = "value";

    default String getValue() {
        return this.getValueBase(DEFAULT_VALUE_NAME);
    }

    @JsonValue
    default String getName() {
        return this.getValueBase(DEFAULT_NAME_NAME);
    }

    default String getValueBase(String fieldStr){
        Field field = ReflectionUtils.findField(this.getClass(), fieldStr);
        if (field == null)
            return null;
        try {
            field.setAccessible(true);
            return field.get(this).toString();
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    static <T extends Enum<T>> T valueOfEnum(Class<T> enumClass, String value) {
        if (value == null)
            throw new IllegalArgumentException("DisplayedEnum value should not be null");
        if (enumClass.isAssignableFrom(DisplayedEnum.class))
            throw new IllegalArgumentException("illegal DisplayedEnum type");
        T[] enums = enumClass.getEnumConstants();
        for (T t : enums) {
            DisplayedEnum displayedEnum = (DisplayedEnum) t;
            if (displayedEnum.getValue().equals(value))
                return (T) displayedEnum;
        }
        throw new IllegalArgumentException("cannot parse integer: " + value + " to " + enumClass.getName());
    }
}



