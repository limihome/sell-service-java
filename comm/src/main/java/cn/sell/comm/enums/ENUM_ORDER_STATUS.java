package cn.sell.comm.enums;

public enum ENUM_ORDER_STATUS {
    PAYMENT("payment", "待付款", 1),
    SHIPMENT("shipment", "待发货", 2),
    RECEIVING("receiving", "待收货", 3),
    COMMENT("comment", "待评论", 4),
    COMPLETE("complete", "已完成", 5),
    ;

    ENUM_ORDER_STATUS(String type, String name, int code) {
    }
}
