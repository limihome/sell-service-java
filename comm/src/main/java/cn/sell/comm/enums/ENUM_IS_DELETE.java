package cn.sell.comm.enums;

import cn.sell.comm.enums.base.DisplayedEnum;

public enum ENUM_IS_DELETE implements DisplayedEnum {
    NORMAL("0","正常","0"),
    DELETE("1","删除","1"),
    ;
    private String code;
    private String name;
    private String value;

    ENUM_IS_DELETE(String code, String name, String value) {
        this.code = code;
        this.name = name;
        this.value = value;
    }
    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    public String getName() {
        return name;
    }
}
